(function() {
    'use strict';

    angular
        .module('codpubliapp', [
            'ngStorage',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ui.bootstrap',
            'ui.bootstrap.datetimepicker',
            'ui.router',
            'infinite-scroll',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar'
        ])
        .run(run);

    run.$inject = ['stateHandler', 'translationHandler','$rootScope'];

    function run(stateHandler, translationHandler,$rootScope ,$templateCache, $timeout) {
        stateHandler.initialize();
        translationHandler.initialize();
        $rootScope.$on("$viewContentLoaded", function(event, toState, toParams, fromState, fromParams) {
            console.log("Cambio de estados")

            var urlScripts = "content/resourcesjs/codelogs.js";

            $.getScript(urlScripts).done(function(){
                fullScan();
            });
        });
    }
})();
