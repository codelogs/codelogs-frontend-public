
function _classCallCheck(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
} ! function (t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
} ("undefined" != typeof window ? window : this, function (t, e) {
    function n(t) {
        var e = t.length,
            n = rt.type(t);
        return "function" === n || rt.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
    }

    function i(t, e, n) {
        if (rt.isFunction(e)) return rt.grep(t, function (t, i) {
            return !!e.call(t, i, t) !== n
        });
        if (e.nodeType) return rt.grep(t, function (t) {
            return t === e !== n
        });
        if ("string" == typeof e) {
            if (ht.test(e)) return rt.filter(e, t, n);
            e = rt.filter(e, t)
        }
        return rt.grep(t, function (t) {
            return rt.inArray(t, e) >= 0 !== n
        })
    }

    function r(t, e) {
        do t = t[e]; while (t && 1 !== t.nodeType);
        return t
    }

    function o(t) {
        var e = wt[t] = {};
        return rt.each(t.match(_t) || [], function (t, n) {
            e[n] = !0
        }), e
    }

    function a() {
        pt.addEventListener ? (pt.removeEventListener("DOMContentLoaded", s, !1), t.removeEventListener("load", s, !1)) : (pt.detachEvent("onreadystatechange", s), t.detachEvent("onload", s))
    }

    function s() {
        (pt.addEventListener || "load" === event.type || "complete" === pt.readyState) && (a(), rt.ready())
    }

    function l(t, e, n) {
        if (void 0 === n && 1 === t.nodeType) {
            var i = "data-" + e.replace(St, "-$1").toLowerCase();
            if (n = t.getAttribute(i), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : Ct.test(n) ? rt.parseJSON(n) : n
                } catch (r) { }
                rt.data(t, e, n)
            } else n = void 0
        }
        return n
    }

    function u(t) {
        var e;
        for (e in t)
            if (("data" !== e || !rt.isEmptyObject(t[e])) && "toJSON" !== e) return !1;
        return !0
    }

    function c(t, e, n, i) {
        if (rt.acceptData(t)) {
            var r, o, a = rt.expando,
                s = t.nodeType,
                l = s ? rt.cache : t,
                u = s ? t[a] : t[a] && a;
            if (u && l[u] && (i || l[u].data) || void 0 !== n || "string" != typeof e) return u || (u = s ? t[a] = G.pop() || rt.guid++ : a), l[u] || (l[u] = s ? {} : {
                toJSON: rt.noop
            }), ("object" == typeof e || "function" == typeof e) && (i ? l[u] = rt.extend(l[u], e) : l[u].data = rt.extend(l[u].data, e)), o = l[u], i || (o.data || (o.data = {}), o = o.data), void 0 !== n && (o[rt.camelCase(e)] = n), "string" == typeof e ? (r = o[e], null == r && (r = o[rt.camelCase(e)])) : r = o, r
        }
    }

    function d(t, e, n) {
        if (rt.acceptData(t)) {
            var i, r, o = t.nodeType,
                a = o ? rt.cache : t,
                s = o ? t[rt.expando] : rt.expando;
            if (a[s]) {
                if (e && (i = n ? a[s] : a[s].data)) {
                    rt.isArray(e) ? e = e.concat(rt.map(e, rt.camelCase)) : e in i ? e = [e] : (e = rt.camelCase(e), e = e in i ? [e] : e.split(" ")), r = e.length;
                    for (; r--;) delete i[e[r]];
                    if (n ? !u(i) : !rt.isEmptyObject(i)) return
                } (n || (delete a[s].data, u(a[s]))) && (o ? rt.cleanData([t], !0) : nt.deleteExpando || a != a.window ? delete a[s] : a[s] = null)
            }
        }
    }

    function h() {
        return !0
    }

    function f() {
        return !1
    }

    function p() {
        try {
            return pt.activeElement
        } catch (t) { }
    }

    function m(t) {
        var e = Ft.split("|"),
            n = t.createDocumentFragment();
        if (n.createElement)
            for (; e.length;) n.createElement(e.pop());
        return n
    }

    function g(t, e) {
        var n, i, r = 0,
            o = typeof t.getElementsByTagName !== kt ? t.getElementsByTagName(e || "*") : typeof t.querySelectorAll !== kt ? t.querySelectorAll(e || "*") : void 0;
        if (!o)
            for (o = [], n = t.childNodes || t; null != (i = n[r]); r++) !e || rt.nodeName(i, e) ? o.push(i) : rt.merge(o, g(i, e));
        return void 0 === e || e && rt.nodeName(t, e) ? rt.merge([t], o) : o
    }

    function v(t) {
        Mt.test(t.type) && (t.defaultChecked = t.checked)
    }

    function y(t, e) {
        return rt.nodeName(t, "table") && rt.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }

    function _(t) {
        return t.type = (null !== rt.find.attr(t, "type")) + "/" + t.type, t
    }

    function w(t) {
        var e = Vt.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function b(t, e) {
        for (var n, i = 0; null != (n = t[i]); i++) rt._data(n, "globalEval", !e || rt._data(e[i], "globalEval"))
    }

    function x(t, e) {
        if (1 === e.nodeType && rt.hasData(t)) {
            var n, i, r, o = rt._data(t),
                a = rt._data(e, o),
                s = o.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s)
                    for (i = 0, r = s[n].length; r > i; i++) rt.event.add(e, n, s[n][i])
            }
            a.data && (a.data = rt.extend({}, a.data))
        }
    }

    function k(t, e) {
        var n, i, r;
        if (1 === e.nodeType) {
            if (n = e.nodeName.toLowerCase(), !nt.noCloneEvent && e[rt.expando]) {
                r = rt._data(e);
                for (i in r.events) rt.removeEvent(e, i, r.handle);
                e.removeAttribute(rt.expando)
            }
            "script" === n && e.text !== t.text ? (_(e).text = t.text, w(e)) : "object" === n ? (e.parentNode && (e.outerHTML = t.outerHTML), nt.html5Clone && t.innerHTML && !rt.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === n && Mt.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === n ? e.defaultSelected = e.selected = t.defaultSelected : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
        }
    }

    function C(e, n) {
        var i, r = rt(n.createElement(e)).appendTo(n.body),
            o = t.getDefaultComputedStyle && (i = t.getDefaultComputedStyle(r[0])) ? i.display : rt.css(r[0], "display");
        return r.detach(), o
    }

    function S(t) {
        var e = pt,
            n = Zt[t];
        return n || (n = C(t, e), "none" !== n && n || (Kt = (Kt || rt("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = (Kt[0].contentWindow || Kt[0].contentDocument).document, e.write(), e.close(), n = C(t, e), Kt.detach()), Zt[t] = n), n
    }

    function T(t, e) {
        return {
            get: function () {
                var n = t();
                if (null != n) return n ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }

    function D(t, e) {
        if (e in t) return e;
        for (var n = e.charAt(0).toUpperCase() + e.slice(1), i = e, r = he.length; r--;)
            if (e = he[r] + n, e in t) return e;
        return i
    }

    function $(t, e) {
        for (var n, i, r, o = [], a = 0, s = t.length; s > a; a++) i = t[a], i.style && (o[a] = rt._data(i, "olddisplay"), n = i.style.display, e ? (o[a] || "none" !== n || (i.style.display = ""), "" === i.style.display && $t(i) && (o[a] = rt._data(i, "olddisplay", S(i.nodeName)))) : (r = $t(i), (n && "none" !== n || !r) && rt._data(i, "olddisplay", r ? n : rt.css(i, "display"))));
        for (a = 0; s > a; a++) i = t[a], i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? o[a] || "" : "none"));
        return t
    }

    function E(t, e, n) {
        var i = le.exec(e);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : e
    }

    function M(t, e, n, i, r) {
        for (var o = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, a = 0; 4 > o; o += 2) "margin" === n && (a += rt.css(t, n + Dt[o], !0, r)), i ? ("content" === n && (a -= rt.css(t, "padding" + Dt[o], !0, r)), "margin" !== n && (a -= rt.css(t, "border" + Dt[o] + "Width", !0, r))) : (a += rt.css(t, "padding" + Dt[o], !0, r), "padding" !== n && (a += rt.css(t, "border" + Dt[o] + "Width", !0, r)));
        return a
    }

    function A(t, e, n) {
        var i = !0,
            r = "width" === e ? t.offsetWidth : t.offsetHeight,
            o = te(t),
            a = nt.boxSizing && "border-box" === rt.css(t, "boxSizing", !1, o);
        if (0 >= r || null == r) {
            if (r = ee(t, e, o), (0 > r || null == r) && (r = t.style[e]), ie.test(r)) return r;
            i = a && (nt.boxSizingReliable() || r === t.style[e]), r = parseFloat(r) || 0
        }
        return r + M(t, e, n || (a ? "border" : "content"), i, o) + "px"
    }

    function O(t, e, n, i, r) {
        return new O.prototype.init(t, e, n, i, r)
    }

    function H() {
        return setTimeout(function () {
            fe = void 0
        }), fe = rt.now()
    }

    function N(t, e) {
        var n, i = {
            height: t
        },
            r = 0;
        for (e = e ? 1 : 0; 4 > r; r += 2 - e) n = Dt[r], i["margin" + n] = i["padding" + n] = t;
        return e && (i.opacity = i.width = t), i
    }

    function P(t, e, n) {
        for (var i, r = (_e[e] || []).concat(_e["*"]), o = 0, a = r.length; a > o; o++)
            if (i = r[o].call(n, e, t)) return i
    }

    function F(t, e, n) {
        var i, r, o, a, s, l, u, c, d = this,
            h = {},
            f = t.style,
            p = t.nodeType && $t(t),
            m = rt._data(t, "fxshow");
        n.queue || (s = rt._queueHooks(t, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function () {
            s.unqueued || l()
        }), s.unqueued++ , d.always(function () {
            d.always(function () {
                s.unqueued-- , rt.queue(t, "fx").length || s.empty.fire()
            })
        })), 1 === t.nodeType && ("height" in e || "width" in e) && (n.overflow = [f.overflow, f.overflowX, f.overflowY], u = rt.css(t, "display"), c = "none" === u ? rt._data(t, "olddisplay") || S(t.nodeName) : u, "inline" === c && "none" === rt.css(t, "float") && (nt.inlineBlockNeedsLayout && "inline" !== S(t.nodeName) ? f.zoom = 1 : f.display = "inline-block")), n.overflow && (f.overflow = "hidden", nt.shrinkWrapBlocks() || d.always(function () {
            f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
        }));
        for (i in e)
            if (r = e[i], me.exec(r)) {
                if (delete e[i], o = o || "toggle" === r, r === (p ? "hide" : "show")) {
                    if ("show" !== r || !m || void 0 === m[i]) continue;
                    p = !0
                }
                h[i] = m && m[i] || rt.style(t, i)
            } else u = void 0;
        if (rt.isEmptyObject(h)) "inline" === ("none" === u ? S(t.nodeName) : u) && (f.display = u);
        else {
            m ? "hidden" in m && (p = m.hidden) : m = rt._data(t, "fxshow", {}), o && (m.hidden = !p), p ? rt(t).show() : d.done(function () {
                rt(t).hide()
            }), d.done(function () {
                var e;
                rt._removeData(t, "fxshow");
                for (e in h) rt.style(t, e, h[e])
            });
            for (i in h) a = P(p ? m[i] : 0, i, d), i in m || (m[i] = a.start, p && (a.end = a.start, a.start = "width" === i || "height" === i ? 1 : 0))
        }
    }

    function j(t, e) {
        var n, i, r, o, a;
        for (n in t)
            if (i = rt.camelCase(n), r = e[i], o = t[n], rt.isArray(o) && (r = o[1], o = t[n] = o[0]), n !== i && (t[i] = o, delete t[n]), a = rt.cssHooks[i], a && "expand" in a) {
                o = a.expand(o), delete t[i];
                for (n in o) n in t || (t[n] = o[n], e[n] = r)
            } else e[i] = r
    }

    function L(t, e, n) {
        var i, r, o = 0,
            a = ye.length,
            s = rt.Deferred().always(function () {
                delete l.elem
            }),
            l = function () {
                if (r) return !1;
                for (var e = fe || H(), n = Math.max(0, u.startTime + u.duration - e), i = n / u.duration || 0, o = 1 - i, a = 0, l = u.tweens.length; l > a; a++) u.tweens[a].run(o);
                return s.notifyWith(t, [u, o, n]), 1 > o && l ? n : (s.resolveWith(t, [u]), !1)
            },
            u = s.promise({
                elem: t,
                props: rt.extend({}, e),
                opts: rt.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: e,
                originalOptions: n,
                startTime: fe || H(),
                duration: n.duration,
                tweens: [],
                createTween: function (e, n) {
                    var i = rt.Tween(t, u.opts, e, n, u.opts.specialEasing[e] || u.opts.easing);
                    return u.tweens.push(i), i
                },
                stop: function (e) {
                    var n = 0,
                        i = e ? u.tweens.length : 0;
                    if (r) return this;
                    for (r = !0; i > n; n++) u.tweens[n].run(1);
                    return e ? s.resolveWith(t, [u, e]) : s.rejectWith(t, [u, e]), this
                }
            }),
            c = u.props;
        for (j(c, u.opts.specialEasing); a > o; o++)
            if (i = ye[o].call(u, t, c, u.opts)) return i;
        return rt.map(c, P, u), rt.isFunction(u.opts.start) && u.opts.start.call(t, u), rt.fx.timer(rt.extend(l, {
            elem: t,
            anim: u,
            queue: u.opts.queue
        })), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
    }

    function I(t) {
        return function (e, n) {
            "string" != typeof e && (n = e, e = "*");
            var i, r = 0,
                o = e.toLowerCase().match(_t) || [];
            if (rt.isFunction(n))
                for (; i = o[r++];) "+" === i.charAt(0) ? (i = i.slice(1) || "*", (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
        }
    }

    function Y(t, e, n, i) {
        function r(s) {
            var l;
            return o[s] = !0, rt.each(t[s] || [], function (t, s) {
                var u = s(e, n, i);
                return "string" != typeof u || a || o[u] ? a ? !(l = u) : void 0 : (e.dataTypes.unshift(u), r(u), !1)
            }), l
        }
        var o = {},
            a = t === ze;
        return r(e.dataTypes[0]) || !o["*"] && r("*")
    }

    function R(t, e) {
        var n, i, r = rt.ajaxSettings.flatOptions || {};
        for (i in e) void 0 !== e[i] && ((r[i] ? t : n || (n = {}))[i] = e[i]);
        return n && rt.extend(!0, t, n), t
    }

    function W(t, e, n) {
        for (var i, r, o, a, s = t.contents, l = t.dataTypes;
            "*" === l[0];) l.shift(), void 0 === r && (r = t.mimeType || e.getResponseHeader("Content-Type"));
        if (r)
            for (a in s)
                if (s[a] && s[a].test(r)) {
                    l.unshift(a);
                    break
                }
        if (l[0] in n) o = l[0];
        else {
            for (a in n) {
                if (!l[0] || t.converters[a + " " + l[0]]) {
                    o = a;
                    break
                }
                i || (i = a)
            }
            o = o || i
        }
        return o ? (o !== l[0] && l.unshift(o), n[o]) : void 0
    }

    function z(t, e, n, i) {
        var r, o, a, s, l, u = {},
            c = t.dataTypes.slice();
        if (c[1])
            for (a in t.converters) u[a.toLowerCase()] = t.converters[a];
        for (o = c.shift(); o;)
            if (t.responseFields[o] && (n[t.responseFields[o]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = o, o = c.shift())
                if ("*" === o) o = l;
                else if ("*" !== l && l !== o) {
                    if (a = u[l + " " + o] || u["* " + o], !a)
                        for (r in u)
                            if (s = r.split(" "), s[1] === o && (a = u[l + " " + s[0]] || u["* " + s[0]])) {
                                a === !0 ? a = u[r] : u[r] !== !0 && (o = s[0], c.unshift(s[1]));
                                break
                            }
                    if (a !== !0)
                        if (a && t["throws"]) e = a(e);
                        else try {
                            e = a(e)
                        } catch (d) {
                            return {
                                state: "parsererror",
                                error: a ? d : "No conversion from " + l + " to " + o
                            }
                        }
                }
        return {
            state: "success",
            data: e
        }
    }

    function q(t, e, n, i) {
        var r;
        if (rt.isArray(e)) rt.each(e, function (e, r) {
            n || Ve.test(t) ? i(t, r) : q(t + "[" + ("object" == typeof r ? e : "") + "]", r, n, i)
        });
        else if (n || "object" !== rt.type(e)) i(t, e);
        else
            for (r in e) q(t + "[" + r + "]", e[r], n, i)
    }

    function B() {
        try {
            return new t.XMLHttpRequest
        } catch (e) { }
    }

    function U() {
        try {
            return new t.ActiveXObject("Microsoft.XMLHTTP")
        } catch (e) { }
    }

    function V(t) {
        return rt.isWindow(t) ? t : 9 === t.nodeType ? t.defaultView || t.parentWindow : !1
    }
    var G = [],
        X = G.slice,
        Q = G.concat,
        J = G.push,
        K = G.indexOf,
        Z = {},
        tt = Z.toString,
        et = Z.hasOwnProperty,
        nt = {},
        it = "1.11.1",
        rt = function (t, e) {
            return new rt.fn.init(t, e)
        },
        ot = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        at = /^-ms-/,
        st = /-([\da-z])/gi,
        lt = function (t, e) {
            return e.toUpperCase()
        };
    rt.fn = rt.prototype = {
        jquery: it,
        constructor: rt,
        selector: "",
        length: 0,
        toArray: function () {
            return X.call(this)
        },
        get: function (t) {
            return null != t ? 0 > t ? this[t + this.length] : this[t] : X.call(this)
        },
        pushStack: function (t) {
            var e = rt.merge(this.constructor(), t);
            return e.prevObject = this, e.context = this.context, e
        },
        each: function (t, e) {
            return rt.each(this, t, e)
        },
        map: function (t) {
            return this.pushStack(rt.map(this, function (e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function () {
            return this.pushStack(X.apply(this, arguments))
        },
        first: function () {
            return this.eq(0)
        },
        last: function () {
            return this.eq(-1)
        },
        eq: function (t) {
            var e = this.length,
                n = +t + (0 > t ? e : 0);
            return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
        },
        end: function () {
            return this.prevObject || this.constructor(null)
        },
        push: J,
        sort: G.sort,
        splice: G.splice
    }, rt.extend = rt.fn.extend = function () {
        var t, e, n, i, r, o, a = arguments[0] || {},
            s = 1,
            l = arguments.length,
            u = !1;
        for ("boolean" == typeof a && (u = a, a = arguments[s] || {}, s++), "object" == typeof a || rt.isFunction(a) || (a = {}), s === l && (a = this, s--); l > s; s++)
            if (null != (r = arguments[s]))
                for (i in r) t = a[i], n = r[i], a !== n && (u && n && (rt.isPlainObject(n) || (e = rt.isArray(n))) ? (e ? (e = !1, o = t && rt.isArray(t) ? t : []) : o = t && rt.isPlainObject(t) ? t : {}, a[i] = rt.extend(u, o, n)) : void 0 !== n && (a[i] = n));
        return a
    }, rt.extend({
        expando: "jQuery" + (it + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function (t) {
            throw new Error(t)
        },
        noop: function () { },
        isFunction: function (t) {
            return "function" === rt.type(t)
        },
        isArray: Array.isArray || function (t) {
            return "array" === rt.type(t)
        },
        isWindow: function (t) {
            return null != t && t == t.window
        },
        isNumeric: function (t) {
            return !rt.isArray(t) && t - parseFloat(t) >= 0
        },
        isEmptyObject: function (t) {
            var e;
            for (e in t) return !1;
            return !0
        },
        isPlainObject: function (t) {
            var e;
            if (!t || "object" !== rt.type(t) || t.nodeType || rt.isWindow(t)) return !1;
            try {
                if (t.constructor && !et.call(t, "constructor") && !et.call(t.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            if (nt.ownLast)
                for (e in t) return et.call(t, e);
            for (e in t);
            return void 0 === e || et.call(t, e)
        },
        type: function (t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? Z[tt.call(t)] || "object" : typeof t
        },
        globalEval: function (e) {
            e && rt.trim(e) && (t.execScript || function (e) {
                t.eval.call(t, e)
            })(e)
        },
        camelCase: function (t) {
            return t.replace(at, "ms-").replace(st, lt)
        },
        nodeName: function (t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        },
        each: function (t, e, i) {
            var r, o = 0,
                a = t.length,
                s = n(t);
            if (i) {
                if (s)
                    for (; a > o && (r = e.apply(t[o], i), r !== !1); o++);
                else
                    for (o in t)
                        if (r = e.apply(t[o], i), r === !1) break
            } else if (s)
                for (; a > o && (r = e.call(t[o], o, t[o]), r !== !1); o++);
            else
                for (o in t)
                    if (r = e.call(t[o], o, t[o]), r === !1) break; return t
        },
        trim: function (t) {
            return null == t ? "" : (t + "").replace(ot, "")
        },
        makeArray: function (t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? rt.merge(i, "string" == typeof t ? [t] : t) : J.call(i, t)), i
        },
        inArray: function (t, e, n) {
            var i;
            if (e) {
                if (K) return K.call(e, t, n);
                for (i = e.length, n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++)
                    if (n in e && e[n] === t) return n
            }
            return -1
        },
        merge: function (t, e) {
            for (var n = +e.length, i = 0, r = t.length; n > i;) t[r++] = e[i++];
            if (n !== n)
                for (; void 0 !== e[i];) t[r++] = e[i++];
            return t.length = r, t
        },
        grep: function (t, e, n) {
            for (var i, r = [], o = 0, a = t.length, s = !n; a > o; o++) i = !e(t[o], o), i !== s && r.push(t[o]);
            return r
        },
        map: function (t, e, i) {
            var r, o = 0,
                a = t.length,
                s = n(t),
                l = [];
            if (s)
                for (; a > o; o++) r = e(t[o], o, i), null != r && l.push(r);
            else
                for (o in t) r = e(t[o], o, i), null != r && l.push(r);
            return Q.apply([], l)
        },
        guid: 1,
        proxy: function (t, e) {
            var n, i, r;
            return "string" == typeof e && (r = t[e], e = t, t = r), rt.isFunction(t) ? (n = X.call(arguments, 2), i = function () {
                return t.apply(e || this, n.concat(X.call(arguments)))
            }, i.guid = t.guid = t.guid || rt.guid++ , i) : void 0
        },
        now: function () {
            return +new Date
        },
        support: nt
    }), rt.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
        Z["[object " + e + "]"] = e.toLowerCase()
    });
    var ut = function (t) {
        function e(t, e, n, i) {
            var r, o, a, s, l, u, d, f, p, m;
            if ((e ? e.ownerDocument || e : Y) !== O && A(e), e = e || O, n = n || [], !t || "string" != typeof t) return n;
            if (1 !== (s = e.nodeType) && 9 !== s) return [];
            if (N && !i) {
                if (r = yt.exec(t))
                    if (a = r[1]) {
                        if (9 === s) {
                            if (o = e.getElementById(a), !o || !o.parentNode) return n;
                            if (o.id === a) return n.push(o), n
                        } else if (e.ownerDocument && (o = e.ownerDocument.getElementById(a)) && L(e, o) && o.id === a) return n.push(o), n
                    } else {
                        if (r[2]) return Z.apply(n, e.getElementsByTagName(t)), n;
                        if ((a = r[3]) && b.getElementsByClassName && e.getElementsByClassName) return Z.apply(n, e.getElementsByClassName(a)), n
                    }
                if (b.qsa && (!P || !P.test(t))) {
                    if (f = d = I, p = e, m = 9 === s && t, 1 === s && "object" !== e.nodeName.toLowerCase()) {
                        for (u = S(t), (d = e.getAttribute("id")) ? f = d.replace(wt, "\\$&") : e.setAttribute("id", f), f = "[id='" + f + "'] ", l = u.length; l--;) u[l] = f + h(u[l]);
                        p = _t.test(t) && c(e.parentNode) || e, m = u.join(",")
                    }
                    if (m) try {
                        return Z.apply(n, p.querySelectorAll(m)), n
                    } catch (g) { } finally {
                            d || e.removeAttribute("id")
                        }
                }
            }
            return D(t.replace(lt, "$1"), e, n, i)
        }

        function n() {
            function t(n, i) {
                return e.push(n + " ") > x.cacheLength && delete t[e.shift()], t[n + " "] = i
            }
            var e = [];
            return t
        }

        function i(t) {
            return t[I] = !0, t
        }

        function r(t) {
            var e = O.createElement("div");
            try {
                return !!t(e)
            } catch (n) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function o(t, e) {
            for (var n = t.split("|"), i = t.length; i--;) x.attrHandle[n[i]] = e
        }

        function a(t, e) {
            var n = e && t,
                i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || G) - (~t.sourceIndex || G);
            if (i) return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === e) return -1;
            return t ? 1 : -1
        }

        function s(t) {
            return function (e) {
                var n = e.nodeName.toLowerCase();
                return "input" === n && e.type === t
            }
        }

        function l(t) {
            return function (e) {
                var n = e.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && e.type === t
            }
        }

        function u(t) {
            return i(function (e) {
                return e = +e, i(function (n, i) {
                    for (var r, o = t([], n.length, e), a = o.length; a--;) n[r = o[a]] && (n[r] = !(i[r] = n[r]))
                })
            })
        }

        function c(t) {
            return t && typeof t.getElementsByTagName !== V && t
        }

        function d() { }

        function h(t) {
            for (var e = 0, n = t.length, i = ""; n > e; e++) i += t[e].value;
            return i
        }

        function f(t, e, n) {
            var i = e.dir,
                r = n && "parentNode" === i,
                o = W++;
            return e.first ? function (e, n, o) {
                for (; e = e[i];)
                    if (1 === e.nodeType || r) return t(e, n, o)
            } : function (e, n, a) {
                var s, l, u = [R, o];
                if (a) {
                    for (; e = e[i];)
                        if ((1 === e.nodeType || r) && t(e, n, a)) return !0
                } else
                    for (; e = e[i];)
                        if (1 === e.nodeType || r) {
                            if (l = e[I] || (e[I] = {}), (s = l[i]) && s[0] === R && s[1] === o) return u[2] = s[2];
                            if (l[i] = u, u[2] = t(e, n, a)) return !0
                        }
            }
        }

        function p(t) {
            return t.length > 1 ? function (e, n, i) {
                for (var r = t.length; r--;)
                    if (!t[r](e, n, i)) return !1;
                return !0
            } : t[0]
        }

        function m(t, n, i) {
            for (var r = 0, o = n.length; o > r; r++) e(t, n[r], i);
            return i
        }

        function g(t, e, n, i, r) {
            for (var o, a = [], s = 0, l = t.length, u = null != e; l > s; s++)(o = t[s]) && (!n || n(o, i, r)) && (a.push(o), u && e.push(s));
            return a
        }

        function v(t, e, n, r, o, a) {
            return r && !r[I] && (r = v(r)), o && !o[I] && (o = v(o, a)), i(function (i, a, s, l) {
                var u, c, d, h = [],
                    f = [],
                    p = a.length,
                    v = i || m(e || "*", s.nodeType ? [s] : s, []),
                    y = !t || !i && e ? v : g(v, h, t, s, l),
                    _ = n ? o || (i ? t : p || r) ? [] : a : y;
                if (n && n(y, _, s, l), r)
                    for (u = g(_, f), r(u, [], s, l), c = u.length; c--;)(d = u[c]) && (_[f[c]] = !(y[f[c]] = d));
                if (i) {
                    if (o || t) {
                        if (o) {
                            for (u = [], c = _.length; c--;)(d = _[c]) && u.push(y[c] = d);
                            o(null, _ = [], u, l)
                        }
                        for (c = _.length; c--;)(d = _[c]) && (u = o ? et.call(i, d) : h[c]) > -1 && (i[u] = !(a[u] = d))
                    }
                } else _ = g(_ === a ? _.splice(p, _.length) : _), o ? o(null, a, _, l) : Z.apply(a, _)
            })
        }

        function y(t) {
            for (var e, n, i, r = t.length, o = x.relative[t[0].type], a = o || x.relative[" "], s = o ? 1 : 0, l = f(function (t) {
                return t === e
            }, a, !0), u = f(function (t) {
                return et.call(e, t) > -1
            }, a, !0), c = [function (t, n, i) {
                return !o && (i || n !== $) || ((e = n).nodeType ? l(t, n, i) : u(t, n, i))
            }]; r > s; s++)
                if (n = x.relative[t[s].type]) c = [f(p(c), n)];
                else {
                    if (n = x.filter[t[s].type].apply(null, t[s].matches), n[I]) {
                        for (i = ++s; r > i && !x.relative[t[i].type]; i++);
                        return v(s > 1 && p(c), s > 1 && h(t.slice(0, s - 1).concat({
                            value: " " === t[s - 2].type ? "*" : ""
                        })).replace(lt, "$1"), n, i > s && y(t.slice(s, i)), r > i && y(t = t.slice(i)), r > i && h(t))
                    }
                    c.push(n)
                }
            return p(c)
        }

        function _(t, n) {
            var r = n.length > 0,
                o = t.length > 0,
                a = function (i, a, s, l, u) {
                    var c, d, h, f = 0,
                        p = "0",
                        m = i && [],
                        v = [],
                        y = $,
                        _ = i || o && x.find.TAG("*", u),
                        w = R += null == y ? 1 : Math.random() || .1,
                        b = _.length;
                    for (u && ($ = a !== O && a); p !== b && null != (c = _[p]); p++) {
                        if (o && c) {
                            for (d = 0; h = t[d++];)
                                if (h(c, a, s)) {
                                    l.push(c);
                                    break
                                }
                            u && (R = w)
                        }
                        r && ((c = !h && c) && f-- , i && m.push(c))
                    }
                    if (f += p, r && p !== f) {
                        for (d = 0; h = n[d++];) h(m, v, a, s);
                        if (i) {
                            if (f > 0)
                                for (; p--;) m[p] || v[p] || (v[p] = J.call(l));
                            v = g(v)
                        }
                        Z.apply(l, v), u && !i && v.length > 0 && f + n.length > 1 && e.uniqueSort(l)
                    }
                    return u && (R = w, $ = y), m
                };
            return r ? i(a) : a
        }
        var w, b, x, k, C, S, T, D, $, E, M, A, O, H, N, P, F, j, L, I = "sizzle" + -new Date,
            Y = t.document,
            R = 0,
            W = 0,
            z = n(),
            q = n(),
            B = n(),
            U = function (t, e) {
                return t === e && (M = !0), 0
            },
            V = "undefined",
            G = 1 << 31,
            X = {}.hasOwnProperty,
            Q = [],
            J = Q.pop,
            K = Q.push,
            Z = Q.push,
            tt = Q.slice,
            et = Q.indexOf || function (t) {
                for (var e = 0, n = this.length; n > e; e++)
                    if (this[e] === t) return e;
                return -1
            },
            nt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            it = "[\\x20\\t\\r\\n\\f]",
            rt = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ot = rt.replace("w", "w#"),
            at = "\\[" + it + "*(" + rt + ")(?:" + it + "*([*^$|!~]?=)" + it + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ot + "))|)" + it + "*\\]",
            st = ":(" + rt + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + at + ")*)|.*)\\)|)",
            lt = new RegExp("^" + it + "+|((?:^|[^\\\\])(?:\\\\.)*)" + it + "+$", "g"),
            ut = new RegExp("^" + it + "*," + it + "*"),
            ct = new RegExp("^" + it + "*([>+~]|" + it + ")" + it + "*"),
            dt = new RegExp("=" + it + "*([^\\]'\"]*?)" + it + "*\\]", "g"),
            ht = new RegExp(st),
            ft = new RegExp("^" + ot + "$"),
            pt = {
                ID: new RegExp("^#(" + rt + ")"),
                CLASS: new RegExp("^\\.(" + rt + ")"),
                TAG: new RegExp("^(" + rt.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + at),
                PSEUDO: new RegExp("^" + st),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + it + "*(even|odd|(([+-]|)(\\d*)n|)" + it + "*(?:([+-]|)" + it + "*(\\d+)|))" + it + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + nt + ")$", "i"),
                needsContext: new RegExp("^" + it + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + it + "*((?:-\\d)?\\d*)" + it + "*\\)|)(?=[^-]|$)", "i")
            },
            mt = /^(?:input|select|textarea|button)$/i,
            gt = /^h\d$/i,
            vt = /^[^{]+\{\s*\[native \w/,
            yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            _t = /[+~]/,
            wt = /'|\\/g,
            bt = new RegExp("\\\\([\\da-f]{1,6}" + it + "?|(" + it + ")|.)", "ig"),
            xt = function (t, e, n) {
                var i = "0x" + e - 65536;
                return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            };
        try {
            Z.apply(Q = tt.call(Y.childNodes), Y.childNodes), Q[Y.childNodes.length].nodeType
        } catch (kt) {
            Z = {
                apply: Q.length ? function (t, e) {
                    K.apply(t, tt.call(e))
                } : function (t, e) {
                    for (var n = t.length, i = 0; t[n++] = e[i++];);
                    t.length = n - 1
                }
            }
        }
        b = e.support = {}, C = e.isXML = function (t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return e ? "HTML" !== e.nodeName : !1
        }, A = e.setDocument = function (t) {
            var e, n = t ? t.ownerDocument || t : Y,
                i = n.defaultView;
            return n !== O && 9 === n.nodeType && n.documentElement ? (O = n, H = n.documentElement, N = !C(n), i && i !== i.top && (i.addEventListener ? i.addEventListener("unload", function () {
                A()
            }, !1) : i.attachEvent && i.attachEvent("onunload", function () {
                A()
            })), b.attributes = r(function (t) {
                return t.className = "i", !t.getAttribute("className")
            }), b.getElementsByTagName = r(function (t) {
                return t.appendChild(n.createComment("")), !t.getElementsByTagName("*").length
            }), b.getElementsByClassName = vt.test(n.getElementsByClassName) && r(function (t) {
                return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 2 === t.getElementsByClassName("i").length
            }), b.getById = r(function (t) {
                return H.appendChild(t).id = I, !n.getElementsByName || !n.getElementsByName(I).length
            }), b.getById ? (x.find.ID = function (t, e) {
                if (typeof e.getElementById !== V && N) {
                    var n = e.getElementById(t);
                    return n && n.parentNode ? [n] : []
                }
            }, x.filter.ID = function (t) {
                var e = t.replace(bt, xt);
                return function (t) {
                    return t.getAttribute("id") === e
                }
            }) : (delete x.find.ID, x.filter.ID = function (t) {
                var e = t.replace(bt, xt);
                return function (t) {
                    var n = typeof t.getAttributeNode !== V && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }), x.find.TAG = b.getElementsByTagName ? function (t, e) {
                return typeof e.getElementsByTagName !== V ? e.getElementsByTagName(t) : void 0
            } : function (t, e) {
                var n, i = [],
                    r = 0,
                    o = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = o[r++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return o
            }, x.find.CLASS = b.getElementsByClassName && function (t, e) {
                return typeof e.getElementsByClassName !== V && N ? e.getElementsByClassName(t) : void 0
            }, F = [], P = [], (b.qsa = vt.test(n.querySelectorAll)) && (r(function (t) {
                t.innerHTML = "<select msallowclip=''><option selected=''></option></select>", t.querySelectorAll("[msallowclip^='']").length && P.push("[*^$]=" + it + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || P.push("\\[" + it + "*(?:value|" + nt + ")"), t.querySelectorAll(":checked").length || P.push(":checked")
            }), r(function (t) {
                var e = n.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && P.push("name" + it + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || P.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), P.push(",.*:")
            })), (b.matchesSelector = vt.test(j = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && r(function (t) {
                b.disconnectedMatch = j.call(t, "div"), j.call(t, "[s!='']:x"), F.push("!=", st)
            }), P = P.length && new RegExp(P.join("|")), F = F.length && new RegExp(F.join("|")), e = vt.test(H.compareDocumentPosition), L = e || vt.test(H.contains) ? function (t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t,
                    i = e && e.parentNode;
                return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
            } : function (t, e) {
                if (e)
                    for (; e = e.parentNode;)
                        if (e === t) return !0;
                return !1
            }, U = e ? function (t, e) {
                if (t === e) return M = !0, 0;
                var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return i ? i : (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & i || !b.sortDetached && e.compareDocumentPosition(t) === i ? t === n || t.ownerDocument === Y && L(Y, t) ? -1 : e === n || e.ownerDocument === Y && L(Y, e) ? 1 : E ? et.call(E, t) - et.call(E, e) : 0 : 4 & i ? -1 : 1)
            } : function (t, e) {
                if (t === e) return M = !0, 0;
                var i, r = 0,
                    o = t.parentNode,
                    s = e.parentNode,
                    l = [t],
                    u = [e];
                if (!o || !s) return t === n ? -1 : e === n ? 1 : o ? -1 : s ? 1 : E ? et.call(E, t) - et.call(E, e) : 0;
                if (o === s) return a(t, e);
                for (i = t; i = i.parentNode;) l.unshift(i);
                for (i = e; i = i.parentNode;) u.unshift(i);
                for (; l[r] === u[r];) r++;
                return r ? a(l[r], u[r]) : l[r] === Y ? -1 : u[r] === Y ? 1 : 0
            }, n) : O
        }, e.matches = function (t, n) {
            return e(t, null, null, n)
        }, e.matchesSelector = function (t, n) {
            if ((t.ownerDocument || t) !== O && A(t), n = n.replace(dt, "='$1']"), b.matchesSelector && N && (!F || !F.test(n)) && (!P || !P.test(n))) try {
                var i = j.call(t, n);
                if (i || b.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
            } catch (r) { }
            return e(n, O, null, [t]).length > 0
        }, e.contains = function (t, e) {
            return (t.ownerDocument || t) !== O && A(t), L(t, e)
        }, e.attr = function (t, e) {
            (t.ownerDocument || t) !== O && A(t);
            var n = x.attrHandle[e.toLowerCase()],
                i = n && X.call(x.attrHandle, e.toLowerCase()) ? n(t, e, !N) : void 0;
            return void 0 !== i ? i : b.attributes || !N ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }, e.error = function (t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function (t) {
            var e, n = [],
                i = 0,
                r = 0;
            if (M = !b.detectDuplicates, E = !b.sortStable && t.slice(0), t.sort(U), M) {
                for (; e = t[r++];) e === t[r] && (i = n.push(r));
                for (; i--;) t.splice(n[i], 1)
            }
            return E = null, t
        }, k = e.getText = function (t) {
            var e, n = "",
                i = 0,
                r = t.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) n += k(t)
                } else if (3 === r || 4 === r) return t.nodeValue
            } else
                for (; e = t[i++];) n += k(e);
            return n
        }, x = e.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: pt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function (t) {
                    return t[1] = t[1].replace(bt, xt), t[3] = (t[3] || t[4] || t[5] || "").replace(bt, xt), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                },
                CHILD: function (t) {
                    return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                },
                PSEUDO: function (t) {
                    var e, n = !t[6] && t[2];
                    return pt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && ht.test(n) && (e = S(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3))
                }
            },
            filter: {
                TAG: function (t) {
                    var e = t.replace(bt, xt).toLowerCase();
                    return "*" === t ? function () {
                        return !0
                    } : function (t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function (t) {
                    var e = z[t + " "];
                    return e || (e = new RegExp("(^|" + it + ")" + t + "(" + it + "|$)")) && z(t, function (t) {
                        return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== V && t.getAttribute("class") || "")
                    })
                },
                ATTR: function (t, n, i) {
                    return function (r) {
                        var o = e.attr(r, t);
                        return null == o ? "!=" === n : n ? (o += "", "=" === n ? o === i : "!=" === n ? o !== i : "^=" === n ? i && 0 === o.indexOf(i) : "*=" === n ? i && o.indexOf(i) > -1 : "$=" === n ? i && o.slice(-i.length) === i : "~=" === n ? (" " + o + " ").indexOf(i) > -1 : "|=" === n ? o === i || o.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                },
                CHILD: function (t, e, n, i, r) {
                    var o = "nth" !== t.slice(0, 3),
                        a = "last" !== t.slice(-4),
                        s = "of-type" === e;
                    return 1 === i && 0 === r ? function (t) {
                        return !!t.parentNode
                    } : function (e, n, l) {
                        var u, c, d, h, f, p, m = o !== a ? "nextSibling" : "previousSibling",
                            g = e.parentNode,
                            v = s && e.nodeName.toLowerCase(),
                            y = !l && !s;
                        if (g) {
                            if (o) {
                                for (; m;) {
                                    for (d = e; d = d[m];)
                                        if (s ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                    p = m = "only" === t && !p && "nextSibling"
                                }
                                return !0
                            }
                            if (p = [a ? g.firstChild : g.lastChild], a && y) {
                                for (c = g[I] || (g[I] = {}), u = c[t] || [], f = u[0] === R && u[1], h = u[0] === R && u[2], d = f && g.childNodes[f]; d = ++f && d && d[m] || (h = f = 0) || p.pop();)
                                    if (1 === d.nodeType && ++h && d === e) {
                                        c[t] = [R, f, h];
                                        break
                                    }
                            } else if (y && (u = (e[I] || (e[I] = {}))[t]) && u[0] === R) h = u[1];
                            else
                                for (;
                                    (d = ++f && d && d[m] || (h = f = 0) || p.pop()) && ((s ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++h || (y && ((d[I] || (d[I] = {}))[t] = [R, h]), d !== e)););
                            return h -= r, h === i || h % i === 0 && h / i >= 0
                        }
                    }
                },
                PSEUDO: function (t, n) {
                    var r, o = x.pseudos[t] || x.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return o[I] ? o(n) : o.length > 1 ? (r = [t, t, "", n], x.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function (t, e) {
                        for (var i, r = o(t, n), a = r.length; a--;) i = et.call(t, r[a]), t[i] = !(e[i] = r[a])
                    }) : function (t) {
                        return o(t, 0, r)
                    }) : o
                }
            },
            pseudos: {
                not: i(function (t) {
                    var e = [],
                        n = [],
                        r = T(t.replace(lt, "$1"));
                    return r[I] ? i(function (t, e, n, i) {
                        for (var o, a = r(t, null, i, []), s = t.length; s--;)(o = a[s]) && (t[s] = !(e[s] = o))
                    }) : function (t, i, o) {
                        return e[0] = t, r(e, null, o, n), !n.pop()
                    }
                }),
                has: i(function (t) {
                    return function (n) {
                        return e(t, n).length > 0
                    }
                }),
                contains: i(function (t) {
                    return function (e) {
                        return (e.textContent || e.innerText || k(e)).indexOf(t) > -1
                    }
                }),
                lang: i(function (t) {
                    return ft.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(bt, xt).toLowerCase(),
                        function (e) {
                            var n;
                            do
                                if (n = N ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return n = n.toLowerCase(), n === t || 0 === n.indexOf(t + "-");
                            while ((e = e.parentNode) && 1 === e.nodeType);
                            return !1
                        }
                }),
                target: function (e) {
                    var n = t.location && t.location.hash;
                    return n && n.slice(1) === e.id
                },
                root: function (t) {
                    return t === H
                },
                focus: function (t) {
                    return t === O.activeElement && (!O.hasFocus || O.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: function (t) {
                    return t.disabled === !1
                },
                disabled: function (t) {
                    return t.disabled === !0
                },
                checked: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function (t) {
                    return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
                },
                empty: function (t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6) return !1;
                    return !0
                },
                parent: function (t) {
                    return !x.pseudos.empty(t)
                },
                header: function (t) {
                    return gt.test(t.nodeName)
                },
                input: function (t) {
                    return mt.test(t.nodeName)
                },
                button: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function (t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: u(function () {
                    return [0]
                }),
                last: u(function (t, e) {
                    return [e - 1]
                }),
                eq: u(function (t, e, n) {
                    return [0 > n ? n + e : n]
                }),
                even: u(function (t, e) {
                    for (var n = 0; e > n; n += 2) t.push(n);
                    return t
                }),
                odd: u(function (t, e) {
                    for (var n = 1; e > n; n += 2) t.push(n);
                    return t
                }),
                lt: u(function (t, e, n) {
                    for (var i = 0 > n ? n + e : n; --i >= 0;) t.push(i);
                    return t
                }),
                gt: u(function (t, e, n) {
                    for (var i = 0 > n ? n + e : n; ++i < e;) t.push(i);
                    return t
                })
            }
        }, x.pseudos.nth = x.pseudos.eq;
        for (w in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) x.pseudos[w] = s(w);
        for (w in {
            submit: !0,
            reset: !0
        }) x.pseudos[w] = l(w);
        return d.prototype = x.filters = x.pseudos, x.setFilters = new d, S = e.tokenize = function (t, n) {
            var i, r, o, a, s, l, u, c = q[t + " "];
            if (c) return n ? 0 : c.slice(0);
            for (s = t, l = [], u = x.preFilter; s;) {
                (!i || (r = ut.exec(s))) && (r && (s = s.slice(r[0].length) || s), l.push(o = [])), i = !1, (r = ct.exec(s)) && (i = r.shift(), o.push({
                    value: i,
                    type: r[0].replace(lt, " ")
                }), s = s.slice(i.length));
                for (a in x.filter) !(r = pt[a].exec(s)) || u[a] && !(r = u[a](r)) || (i = r.shift(), o.push({
                    value: i,
                    type: a,
                    matches: r
                }), s = s.slice(i.length));
                if (!i) break
            }
            return n ? s.length : s ? e.error(t) : q(t, l).slice(0)
        }, T = e.compile = function (t, e) {
            var n, i = [],
                r = [],
                o = B[t + " "];
            if (!o) {
                for (e || (e = S(t)), n = e.length; n--;) o = y(e[n]), o[I] ? i.push(o) : r.push(o);
                o = B(t, _(r, i)), o.selector = t
            }
            return o
        }, D = e.select = function (t, e, n, i) {
            var r, o, a, s, l, u = "function" == typeof t && t,
                d = !i && S(t = u.selector || t);
            if (n = n || [], 1 === d.length) {
                if (o = d[0] = d[0].slice(0), o.length > 2 && "ID" === (a = o[0]).type && b.getById && 9 === e.nodeType && N && x.relative[o[1].type]) {
                    if (e = (x.find.ID(a.matches[0].replace(bt, xt), e) || [])[0], !e) return n;
                    u && (e = e.parentNode), t = t.slice(o.shift().value.length)
                }
                for (r = pt.needsContext.test(t) ? 0 : o.length; r-- && (a = o[r], !x.relative[s = a.type]);)
                    if ((l = x.find[s]) && (i = l(a.matches[0].replace(bt, xt), _t.test(o[0].type) && c(e.parentNode) || e))) {
                        if (o.splice(r, 1), t = i.length && h(o), !t) return Z.apply(n, i), n;
                        break
                    }
            }
            return (u || T(t, d))(i, e, !N, n, _t.test(t) && c(e.parentNode) || e), n
        }, b.sortStable = I.split("").sort(U).join("") === I, b.detectDuplicates = !!M, A(), b.sortDetached = r(function (t) {
            return 1 & t.compareDocumentPosition(O.createElement("div"))
        }), r(function (t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || o("type|href|height|width", function (t, e, n) {
            return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), b.attributes && r(function (t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || o("value", function (t, e, n) {
            return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
        }), r(function (t) {
            return null == t.getAttribute("disabled")
        }) || o(nt, function (t, e, n) {
            var i;
            return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }), e
    } (t);
    rt.find = ut, rt.expr = ut.selectors, rt.expr[":"] = rt.expr.pseudos, rt.unique = ut.uniqueSort, rt.text = ut.getText, rt.isXMLDoc = ut.isXML, rt.contains = ut.contains;
    var ct = rt.expr.match.needsContext,
        dt = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        ht = /^.[^:#\[\.,]*$/;
    rt.filter = function (t, e, n) {
        var i = e[0];
        return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? rt.find.matchesSelector(i, t) ? [i] : [] : rt.find.matches(t, rt.grep(e, function (t) {
            return 1 === t.nodeType
        }))
    }, rt.fn.extend({
        find: function (t) {
            var e, n = [],
                i = this,
                r = i.length;
            if ("string" != typeof t) return this.pushStack(rt(t).filter(function () {
                for (e = 0; r > e; e++)
                    if (rt.contains(i[e], this)) return !0
            }));
            for (e = 0; r > e; e++) rt.find(t, i[e], n);
            return n = this.pushStack(r > 1 ? rt.unique(n) : n), n.selector = this.selector ? this.selector + " " + t : t, n
        },
        filter: function (t) {
            return this.pushStack(i(this, t || [], !1))
        },
        not: function (t) {
            return this.pushStack(i(this, t || [], !0))
        },
        is: function (t) {
            return !!i(this, "string" == typeof t && ct.test(t) ? rt(t) : t || [], !1).length
        }
    });
    var ft, pt = t.document,
        mt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        gt = rt.fn.init = function (t, e) {
            var n, i;
            if (!t) return this;
            if ("string" == typeof t) {
                if (n = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && t.length >= 3 ? [null, t, null] : mt.exec(t), !n || !n[1] && e) return !e || e.jquery ? (e || ft).find(t) : this.constructor(e).find(t);
                if (n[1]) {
                    if (e = e instanceof rt ? e[0] : e, rt.merge(this, rt.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : pt, !0)), dt.test(n[1]) && rt.isPlainObject(e))
                        for (n in e) rt.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                    return this
                }
                if (i = pt.getElementById(n[2]), i && i.parentNode) {
                    if (i.id !== n[2]) return ft.find(t);
                    this.length = 1, this[0] = i
                }
                return this.context = pt, this.selector = t, this
            }
            return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : rt.isFunction(t) ? "undefined" != typeof ft.ready ? ft.ready(t) : t(rt) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), rt.makeArray(t, this))
        };
    gt.prototype = rt.fn, ft = rt(pt);
    var vt = /^(?:parents|prev(?:Until|All))/,
        yt = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    rt.extend({
        dir: function (t, e, n) {
            for (var i = [], r = t[e]; r && 9 !== r.nodeType && (void 0 === n || 1 !== r.nodeType || !rt(r).is(n));) 1 === r.nodeType && i.push(r), r = r[e];
            return i
        },
        sibling: function (t, e) {
            for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
            return n
        }
    }), rt.fn.extend({
        has: function (t) {
            var e, n = rt(t, this),
                i = n.length;
            return this.filter(function () {
                for (e = 0; i > e; e++)
                    if (rt.contains(this, n[e])) return !0
            })
        },
        closest: function (t, e) {
            for (var n, i = 0, r = this.length, o = [], a = ct.test(t) || "string" != typeof t ? rt(t, e || this.context) : 0; r > i; i++)
                for (n = this[i]; n && n !== e; n = n.parentNode)
                    if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && rt.find.matchesSelector(n, t))) {
                        o.push(n);
                        break
                    }
            return this.pushStack(o.length > 1 ? rt.unique(o) : o)
        },
        index: function (t) {
            return t ? "string" == typeof t ? rt.inArray(this[0], rt(t)) : rt.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function (t, e) {
            return this.pushStack(rt.unique(rt.merge(this.get(), rt(t, e))))
        },
        addBack: function (t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }), rt.each({
        parent: function (t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function (t) {
            return rt.dir(t, "parentNode")
        },
        parentsUntil: function (t, e, n) {
            return rt.dir(t, "parentNode", n)
        },
        next: function (t) {
            return r(t, "nextSibling")
        },
        prev: function (t) {
            return r(t, "previousSibling")
        },
        nextAll: function (t) {
            return rt.dir(t, "nextSibling")
        },
        prevAll: function (t) {
            return rt.dir(t, "previousSibling")
        },
        nextUntil: function (t, e, n) {
            return rt.dir(t, "nextSibling", n)
        },
        prevUntil: function (t, e, n) {
            return rt.dir(t, "previousSibling", n)
        },
        siblings: function (t) {
            return rt.sibling((t.parentNode || {}).firstChild, t)
        },
        children: function (t) {
            return rt.sibling(t.firstChild)
        },
        contents: function (t) {
            return rt.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : rt.merge([], t.childNodes)
        }
    }, function (t, e) {
        rt.fn[t] = function (n, i) {
            var r = rt.map(this, e, n);
            return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (r = rt.filter(i, r)), this.length > 1 && (yt[t] || (r = rt.unique(r)), vt.test(t) && (r = r.reverse())), this.pushStack(r)
        }
    });
    var _t = /\S+/g,
        wt = {};
    rt.Callbacks = function (t) {
        t = "string" == typeof t ? wt[t] || o(t) : rt.extend({}, t);
        var e, n, i, r, a, s, l = [],
            u = !t.once && [],
            c = function (o) {
                for (n = t.memory && o, i = !0, a = s || 0, s = 0, r = l.length, e = !0; l && r > a; a++)
                    if (l[a].apply(o[0], o[1]) === !1 && t.stopOnFalse) {
                        n = !1;
                        break
                    }
                e = !1, l && (u ? u.length && c(u.shift()) : n ? l = [] : d.disable())
            },
            d = {
                add: function () {
                    if (l) {
                        var i = l.length;
                        ! function o(e) {
                            rt.each(e, function (e, n) {
                                var i = rt.type(n);
                                "function" === i ? t.unique && d.has(n) || l.push(n) : n && n.length && "string" !== i && o(n)
                            })
                        } (arguments), e ? r = l.length : n && (s = i, c(n))
                    }
                    return this
                },
                remove: function () {
                    return l && rt.each(arguments, function (t, n) {
                        for (var i;
                            (i = rt.inArray(n, l, i)) > -1;) l.splice(i, 1), e && (r >= i && r-- , a >= i && a--)
                    }), this
                },
                has: function (t) {
                    return t ? rt.inArray(t, l) > -1 : !(!l || !l.length)
                },
                empty: function () {
                    return l = [], r = 0, this
                },
                disable: function () {
                    return l = u = n = void 0, this
                },
                disabled: function () {
                    return !l
                },
                lock: function () {
                    return u = void 0, n || d.disable(), this
                },
                locked: function () {
                    return !u
                },
                fireWith: function (t, n) {
                    return !l || i && !u || (n = n || [], n = [t, n.slice ? n.slice() : n], e ? u.push(n) : c(n)), this
                },
                fire: function () {
                    return d.fireWith(this, arguments), this
                },
                fired: function () {
                    return !!i
                }
            };
        return d
    }, rt.extend({
        Deferred: function (t) {
            var e = [
                ["resolve", "done", rt.Callbacks("once memory"), "resolved"],
                ["reject", "fail", rt.Callbacks("once memory"), "rejected"],
                ["notify", "progress", rt.Callbacks("memory")]
            ],
                n = "pending",
                i = {
                    state: function () {
                        return n
                    },
                    always: function () {
                        return r.done(arguments).fail(arguments), this
                    },
                    then: function () {
                        var t = arguments;
                        return rt.Deferred(function (n) {
                            rt.each(e, function (e, o) {
                                var a = rt.isFunction(t[e]) && t[e];
                                r[o[1]](function () {
                                    var t = a && a.apply(this, arguments);
                                    t && rt.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === i ? n.promise() : this, a ? [t] : arguments)
                                })
                            }), t = null
                        }).promise()
                    },
                    promise: function (t) {
                        return null != t ? rt.extend(t, i) : i
                    }
                },
                r = {};
            return i.pipe = i.then, rt.each(e, function (t, o) {
                var a = o[2],
                    s = o[3];
                i[o[1]] = a.add, s && a.add(function () {
                    n = s
                }, e[1 ^ t][2].disable, e[2][2].lock), r[o[0]] = function () {
                    return r[o[0] + "With"](this === r ? i : this, arguments), this
                }, r[o[0] + "With"] = a.fireWith
            }), i.promise(r), t && t.call(r, r), r
        },
        when: function (t) {
            var e, n, i, r = 0,
                o = X.call(arguments),
                a = o.length,
                s = 1 !== a || t && rt.isFunction(t.promise) ? a : 0,
                l = 1 === s ? t : rt.Deferred(),
                u = function (t, n, i) {
                    return function (r) {
                        n[t] = this, i[t] = arguments.length > 1 ? X.call(arguments) : r, i === e ? l.notifyWith(n, i) : --s || l.resolveWith(n, i)
                    }
                };
            if (a > 1)
                for (e = new Array(a), n = new Array(a), i = new Array(a); a > r; r++) o[r] && rt.isFunction(o[r].promise) ? o[r].promise().done(u(r, i, o)).fail(l.reject).progress(u(r, n, e)) : --s;
            return s || l.resolveWith(i, o), l.promise()
        }
    });
    var bt;
    rt.fn.ready = function (t) {
        return rt.ready.promise().done(t), this
    }, rt.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function (t) {
            t ? rt.readyWait++ : rt.ready(!0)
        },
        ready: function (t) {
            if (t === !0 ? !--rt.readyWait : !rt.isReady) {
                if (!pt.body) return setTimeout(rt.ready);
                rt.isReady = !0, t !== !0 && --rt.readyWait > 0 || (bt.resolveWith(pt, [rt]), rt.fn.triggerHandler && (rt(pt).triggerHandler("ready"), rt(pt).off("ready")))
            }
        }
    }), rt.ready.promise = function (e) {
        if (!bt)
            if (bt = rt.Deferred(), "complete" === pt.readyState) setTimeout(rt.ready);
            else if (pt.addEventListener) pt.addEventListener("DOMContentLoaded", s, !1), t.addEventListener("load", s, !1);
            else {
                pt.attachEvent("onreadystatechange", s), t.attachEvent("onload", s);
                var n = !1;
                try {
                    n = null == t.frameElement && pt.documentElement
                } catch (i) { }
                n && n.doScroll && ! function r() {
                    if (!rt.isReady) {
                        try {
                            n.doScroll("left")
                        } catch (t) {
                            return setTimeout(r, 50)
                        }
                        a(), rt.ready()
                    }
                } ()
            }
        return bt.promise(e)
    };
    var xt, kt = "undefined";
    for (xt in rt(nt)) break;
    nt.ownLast = "0" !== xt, nt.inlineBlockNeedsLayout = !1, rt(function () {
        var t, e, n, i;
        n = pt.getElementsByTagName("body")[0], n && n.style && (e = pt.createElement("div"), i = pt.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(e), typeof e.style.zoom !== kt && (e.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", nt.inlineBlockNeedsLayout = t = 3 === e.offsetWidth, t && (n.style.zoom = 1)), n.removeChild(i))
    }),
        function () {
            var t = pt.createElement("div");
            if (null == nt.deleteExpando) {
                nt.deleteExpando = !0;
                try {
                    delete t.test
                } catch (e) {
                    nt.deleteExpando = !1
                }
            }
            t = null
        } (), rt.acceptData = function (t) {
            var e = rt.noData[(t.nodeName + " ").toLowerCase()],
                n = +t.nodeType || 1;
            return 1 !== n && 9 !== n ? !1 : !e || e !== !0 && t.getAttribute("classid") === e
        };
    var Ct = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        St = /([A-Z])/g;
    rt.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function (t) {
            return t = t.nodeType ? rt.cache[t[rt.expando]] : t[rt.expando], !!t && !u(t)
        },
        data: function (t, e, n) {
            return c(t, e, n)
        },
        removeData: function (t, e) {
            return d(t, e)
        },
        _data: function (t, e, n) {
            return c(t, e, n, !0)
        },
        _removeData: function (t, e) {
            return d(t, e, !0)
        }
    }), rt.fn.extend({
        data: function (t, e) {
            var n, i, r, o = this[0],
                a = o && o.attributes;
            if (void 0 === t) {
                if (this.length && (r = rt.data(o), 1 === o.nodeType && !rt._data(o, "parsedAttrs"))) {
                    for (n = a.length; n--;) a[n] && (i = a[n].name, 0 === i.indexOf("data-") && (i = rt.camelCase(i.slice(5)), l(o, i, r[i])));
                    rt._data(o, "parsedAttrs", !0)
                }
                return r
            }
            return "object" == typeof t ? this.each(function () {
                rt.data(this, t)
            }) : arguments.length > 1 ? this.each(function () {
                rt.data(this, t, e)
            }) : o ? l(o, t, rt.data(o, t)) : void 0
        },
        removeData: function (t) {
            return this.each(function () {
                rt.removeData(this, t)
            })
        }
    }), rt.extend({
        queue: function (t, e, n) {
            var i;
            return t ? (e = (e || "fx") + "queue", i = rt._data(t, e), n && (!i || rt.isArray(n) ? i = rt._data(t, e, rt.makeArray(n)) : i.push(n)), i || []) : void 0
        },
        dequeue: function (t, e) {
            e = e || "fx";
            var n = rt.queue(t, e),
                i = n.length,
                r = n.shift(),
                o = rt._queueHooks(t, e),
                a = function () {
                    rt.dequeue(t, e)
                };
            "inprogress" === r && (r = n.shift(), i--), r && ("fx" === e && n.unshift("inprogress"), delete o.stop, r.call(t, a, o)), !i && o && o.empty.fire()
        },
        _queueHooks: function (t, e) {
            var n = e + "queueHooks";
            return rt._data(t, n) || rt._data(t, n, {
                empty: rt.Callbacks("once memory").add(function () {
                    rt._removeData(t, e + "queue"), rt._removeData(t, n)
                })
            })
        }
    }), rt.fn.extend({
        queue: function (t, e) {
            var n = 2;
            return "string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? rt.queue(this[0], t) : void 0 === e ? this : this.each(function () {
                var n = rt.queue(this, t, e);
                rt._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && rt.dequeue(this, t)
            })
        },
        dequeue: function (t) {
            return this.each(function () {
                rt.dequeue(this, t)
            })
        },
        clearQueue: function (t) {
            return this.queue(t || "fx", [])
        },
        promise: function (t, e) {
            var n, i = 1,
                r = rt.Deferred(),
                o = this,
                a = this.length,
                s = function () {
                    --i || r.resolveWith(o, [o])
                };
            for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; a--;) n = rt._data(o[a], t + "queueHooks"), n && n.empty && (i++ , n.empty.add(s));
            return s(), r.promise(e)
        }
    });
    var Tt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Dt = ["Top", "Right", "Bottom", "Left"],
        $t = function (t, e) {
            return t = e || t, "none" === rt.css(t, "display") || !rt.contains(t.ownerDocument, t)
        },
        Et = rt.access = function (t, e, n, i, r, o, a) {
            var s = 0,
                l = t.length,
                u = null == n;
            if ("object" === rt.type(n)) {
                r = !0;
                for (s in n) rt.access(t, e, s, n[s], !0, o, a)
            } else if (void 0 !== i && (r = !0, rt.isFunction(i) || (a = !0), u && (a ? (e.call(t, i), e = null) : (u = e, e = function (t, e, n) {
                return u.call(rt(t), n)
            })), e))
                for (; l > s; s++) e(t[s], n, a ? i : i.call(t[s], s, e(t[s], n)));
            return r ? t : u ? e.call(t) : l ? e(t[0], n) : o
        },
        Mt = /^(?:checkbox|radio)$/i;
    ! function () {
        var t = pt.createElement("input"),
            e = pt.createElement("div"),
            n = pt.createDocumentFragment();
        if (e.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", nt.leadingWhitespace = 3 === e.firstChild.nodeType, nt.tbody = !e.getElementsByTagName("tbody").length, nt.htmlSerialize = !!e.getElementsByTagName("link").length, nt.html5Clone = "<:nav></:nav>" !== pt.createElement("nav").cloneNode(!0).outerHTML, t.type = "checkbox", t.checked = !0, n.appendChild(t), nt.appendChecked = t.checked, e.innerHTML = "<textarea>x</textarea>", nt.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue, n.appendChild(e), e.innerHTML = "<input type='radio' checked='checked' name='t'/>", nt.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, nt.noCloneEvent = !0, e.attachEvent && (e.attachEvent("onclick", function () {
            nt.noCloneEvent = !1
        }), e.cloneNode(!0).click()), null == nt.deleteExpando) {
            nt.deleteExpando = !0;
            try {
                delete e.test
            } catch (i) {
                nt.deleteExpando = !1
            }
        }
    } (),
        function () {
            var e, n, i = pt.createElement("div");
            for (e in {
                submit: !0,
                change: !0,
                focusin: !0
            }) n = "on" + e, (nt[e + "Bubbles"] = n in t) || (i.setAttribute(n, "t"), nt[e + "Bubbles"] = i.attributes[n].expando === !1);
            i = null
        } ();
    var At = /^(?:input|select|textarea)$/i,
        Ot = /^key/,
        Ht = /^(?:mouse|pointer|contextmenu)|click/,
        Nt = /^(?:focusinfocus|focusoutblur)$/,
        Pt = /^([^.]*)(?:\.(.+)|)$/;
    rt.event = {
        global: {},
        add: function (t, e, n, i, r) {
            var o, a, s, l, u, c, d, h, f, p, m, g = rt._data(t);
            if (g) {
                for (n.handler && (l = n, n = l.handler, r = l.selector), n.guid || (n.guid = rt.guid++), (a = g.events) || (a = g.events = {}), (c = g.handle) || (c = g.handle = function (t) {
                    return typeof rt === kt || t && rt.event.triggered === t.type ? void 0 : rt.event.dispatch.apply(c.elem, arguments)
                }, c.elem = t), e = (e || "").match(_t) || [""], s = e.length; s--;) o = Pt.exec(e[s]) || [], f = m = o[1], p = (o[2] || "").split(".").sort(), f && (u = rt.event.special[f] || {}, f = (r ? u.delegateType : u.bindType) || f, u = rt.event.special[f] || {}, d = rt.extend({
                    type: f,
                    origType: m,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: r,
                    needsContext: r && rt.expr.match.needsContext.test(r),
                    namespace: p.join(".")
                }, l), (h = a[f]) || (h = a[f] = [], h.delegateCount = 0, u.setup && u.setup.call(t, i, p, c) !== !1 || (t.addEventListener ? t.addEventListener(f, c, !1) : t.attachEvent && t.attachEvent("on" + f, c))), u.add && (u.add.call(t, d), d.handler.guid || (d.handler.guid = n.guid)), r ? h.splice(h.delegateCount++, 0, d) : h.push(d), rt.event.global[f] = !0);
                t = null
            }
        },
        remove: function (t, e, n, i, r) {
            var o, a, s, l, u, c, d, h, f, p, m, g = rt.hasData(t) && rt._data(t);
            if (g && (c = g.events)) {
                for (e = (e || "").match(_t) || [""], u = e.length; u--;)
                    if (s = Pt.exec(e[u]) || [], f = m = s[1], p = (s[2] || "").split(".").sort(), f) {
                        for (d = rt.event.special[f] || {}, f = (i ? d.delegateType : d.bindType) || f, h = c[f] || [], s = s[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = o = h.length; o--;) a = h[o], !r && m !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || i && i !== a.selector && ("**" !== i || !a.selector) || (h.splice(o, 1), a.selector && h.delegateCount-- , d.remove && d.remove.call(t, a));
                        l && !h.length && (d.teardown && d.teardown.call(t, p, g.handle) !== !1 || rt.removeEvent(t, f, g.handle), delete c[f])
                    } else
                        for (f in c) rt.event.remove(t, f + e[u], n, i, !0);
                rt.isEmptyObject(c) && (delete g.handle, rt._removeData(t, "events"))
            }
        },
        trigger: function (e, n, i, r) {
            var o, a, s, l, u, c, d, h = [i || pt],
                f = et.call(e, "type") ? e.type : e,
                p = et.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = c = i = i || pt, 3 !== i.nodeType && 8 !== i.nodeType && !Nt.test(f + rt.event.triggered) && (f.indexOf(".") >= 0 && (p = f.split("."), f = p.shift(), p.sort()), a = f.indexOf(":") < 0 && "on" + f, e = e[rt.expando] ? e : new rt.Event(f, "object" == typeof e && e), e.isTrigger = r ? 2 : 3, e.namespace = p.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = i), n = null == n ? [e] : rt.makeArray(n, [e]), u = rt.event.special[f] || {}, r || !u.trigger || u.trigger.apply(i, n) !== !1)) {
                if (!r && !u.noBubble && !rt.isWindow(i)) {
                    for (l = u.delegateType || f, Nt.test(l + f) || (s = s.parentNode); s; s = s.parentNode) h.push(s), c = s;
                    c === (i.ownerDocument || pt) && h.push(c.defaultView || c.parentWindow || t)
                }
                for (d = 0;
                    (s = h[d++]) && !e.isPropagationStopped();) e.type = d > 1 ? l : u.bindType || f, o = (rt._data(s, "events") || {})[e.type] && rt._data(s, "handle"), o && o.apply(s, n), o = a && s[a], o && o.apply && rt.acceptData(s) && (e.result = o.apply(s, n), e.result === !1 && e.preventDefault());
                if (e.type = f, !r && !e.isDefaultPrevented() && (!u._default || u._default.apply(h.pop(), n) === !1) && rt.acceptData(i) && a && i[f] && !rt.isWindow(i)) {
                    c = i[a], c && (i[a] = null), rt.event.triggered = f;
                    try {
                        i[f]()
                    } catch (m) { }
                    rt.event.triggered = void 0, c && (i[a] = c)
                }
                return e.result
            }
        },
        dispatch: function (t) {
            t = rt.event.fix(t);
            var e, n, i, r, o, a = [],
                s = X.call(arguments),
                l = (rt._data(this, "events") || {})[t.type] || [],
                u = rt.event.special[t.type] || {};
            if (s[0] = t, t.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, t) !== !1) {
                for (a = rt.event.handlers.call(this, t, l), e = 0;
                    (r = a[e++]) && !t.isPropagationStopped();)
                    for (t.currentTarget = r.elem, o = 0;
                        (i = r.handlers[o++]) && !t.isImmediatePropagationStopped();)(!t.namespace_re || t.namespace_re.test(i.namespace)) && (t.handleObj = i, t.data = i.data, n = ((rt.event.special[i.origType] || {}).handle || i.handler).apply(r.elem, s), void 0 !== n && (t.result = n) === !1 && (t.preventDefault(), t.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, t), t.result
            }
        },
        handlers: function (t, e) {
            var n, i, r, o, a = [],
                s = e.delegateCount,
                l = t.target;
            if (s && l.nodeType && (!t.button || "click" !== t.type))
                for (; l != this; l = l.parentNode || this)
                    if (1 === l.nodeType && (l.disabled !== !0 || "click" !== t.type)) {
                        for (r = [], o = 0; s > o; o++) i = e[o], n = i.selector + " ", void 0 === r[n] && (r[n] = i.needsContext ? rt(n, this).index(l) >= 0 : rt.find(n, this, null, [l]).length), r[n] && r.push(i);
                        r.length && a.push({
                            elem: l,
                            handlers: r
                        })
                    }
            return s < e.length && a.push({
                elem: this,
                handlers: e.slice(s)
            }), a
        },
        fix: function (t) {
            if (t[rt.expando]) return t;
            var e, n, i, r = t.type,
                o = t,
                a = this.fixHooks[r];
            for (a || (this.fixHooks[r] = a = Ht.test(r) ? this.mouseHooks : Ot.test(r) ? this.keyHooks : {}), i = a.props ? this.props.concat(a.props) : this.props, t = new rt.Event(o), e = i.length; e--;) n = i[e], t[n] = o[n];
            return t.target || (t.target = o.srcElement || pt), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, a.filter ? a.filter(t, o) : t
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function (t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (t, e) {
                var n, i, r, o = e.button,
                    a = e.fromElement;
                return null == t.pageX && null != e.clientX && (i = t.target.ownerDocument || pt, r = i.documentElement, n = i.body, t.pageX = e.clientX + (r && r.scrollLeft || n && n.scrollLeft || 0) - (r && r.clientLeft || n && n.clientLeft || 0), t.pageY = e.clientY + (r && r.scrollTop || n && n.scrollTop || 0) - (r && r.clientTop || n && n.clientTop || 0)), !t.relatedTarget && a && (t.relatedTarget = a === t.target ? e.toElement : a), t.which || void 0 === o || (t.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), t
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function () {
                    if (this !== p() && this.focus) try {
                        return this.focus(), !1
                    } catch (t) { }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function () {
                    return this === p() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function () {
                    return rt.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                },
                _default: function (t) {
                    return rt.nodeName(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function (t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function (t, e, n, i) {
            var r = rt.extend(new rt.Event, n, {
                type: t,
                isSimulated: !0,
                originalEvent: {}
            });
            i ? rt.event.trigger(r, null, e) : rt.event.dispatch.call(e, r), r.isDefaultPrevented() && n.preventDefault()
        }
    }, rt.removeEvent = pt.removeEventListener ? function (t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n, !1)
    } : function (t, e, n) {
        var i = "on" + e;
        t.detachEvent && (typeof t[i] === kt && (t[i] = null), t.detachEvent(i, n))
    }, rt.Event = function (t, e) {
        return this instanceof rt.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && t.returnValue === !1 ? h : f) : this.type = t, e && rt.extend(this, e), this.timeStamp = t && t.timeStamp || rt.now(), void (this[rt.expando] = !0)) : new rt.Event(t, e)
    }, rt.Event.prototype = {
        isDefaultPrevented: f,
        isPropagationStopped: f,
        isImmediatePropagationStopped: f,
        preventDefault: function () {
            var t = this.originalEvent;
            this.isDefaultPrevented = h, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
        },
        stopPropagation: function () {
            var t = this.originalEvent;
            this.isPropagationStopped = h, t && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
        },
        stopImmediatePropagation: function () {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = h, t && t.stopImmediatePropagation && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, rt.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (t, e) {
        rt.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function (t) {
                var n, i = this,
                    r = t.relatedTarget,
                    o = t.handleObj;
                return (!r || r !== i && !rt.contains(i, r)) && (t.type = o.origType, n = o.handler.apply(this, arguments), t.type = e), n
            }
        }
    }), nt.submitBubbles || (rt.event.special.submit = {
        setup: function () {
            return rt.nodeName(this, "form") ? !1 : void rt.event.add(this, "click._submit keypress._submit", function (t) {
                var e = t.target,
                    n = rt.nodeName(e, "input") || rt.nodeName(e, "button") ? e.form : void 0;
                n && !rt._data(n, "submitBubbles") && (rt.event.add(n, "submit._submit", function (t) {
                    t._submit_bubble = !0
                }), rt._data(n, "submitBubbles", !0))
            })
        },
        postDispatch: function (t) {
            t._submit_bubble && (delete t._submit_bubble, this.parentNode && !t.isTrigger && rt.event.simulate("submit", this.parentNode, t, !0))
        },
        teardown: function () {
            return rt.nodeName(this, "form") ? !1 : void rt.event.remove(this, "._submit")
        }
    }), nt.changeBubbles || (rt.event.special.change = {
        setup: function () {
            return At.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (rt.event.add(this, "propertychange._change", function (t) {
                "checked" === t.originalEvent.propertyName && (this._just_changed = !0)
            }), rt.event.add(this, "click._change", function (t) {
                this._just_changed && !t.isTrigger && (this._just_changed = !1), rt.event.simulate("change", this, t, !0)
            })), !1) : void rt.event.add(this, "beforeactivate._change", function (t) {
                var e = t.target;
                At.test(e.nodeName) && !rt._data(e, "changeBubbles") && (rt.event.add(e, "change._change", function (t) {
                    !this.parentNode || t.isSimulated || t.isTrigger || rt.event.simulate("change", this.parentNode, t, !0)
                }), rt._data(e, "changeBubbles", !0))
            })
        },
        handle: function (t) {
            var e = t.target;
            return this !== e || t.isSimulated || t.isTrigger || "radio" !== e.type && "checkbox" !== e.type ? t.handleObj.handler.apply(this, arguments) : void 0
        },
        teardown: function () {
            return rt.event.remove(this, "._change"), !At.test(this.nodeName)
        }
    }), nt.focusinBubbles || rt.each({
        focus: "focusin",
        blur: "focusout"
    }, function (t, e) {
        var n = function (t) {
            rt.event.simulate(e, t.target, rt.event.fix(t), !0)
        };
        rt.event.special[e] = {
            setup: function () {
                var i = this.ownerDocument || this,
                    r = rt._data(i, e);
                r || i.addEventListener(t, n, !0), rt._data(i, e, (r || 0) + 1)
            },
            teardown: function () {
                var i = this.ownerDocument || this,
                    r = rt._data(i, e) - 1;
                r ? rt._data(i, e, r) : (i.removeEventListener(t, n, !0), rt._removeData(i, e))
            }
        }
    }), rt.fn.extend({
        on: function (t, e, n, i, r) {
            var o, a;
            if ("object" == typeof t) {
                "string" != typeof e && (n = n || e, e = void 0);
                for (o in t) this.on(o, e, n, t[o], r);
                return this
            }
            if (null == n && null == i ? (i = e, n = e = void 0) : null == i && ("string" == typeof e ? (i = n, n = void 0) : (i = n, n = e, e = void 0)), i === !1) i = f;
            else if (!i) return this;
            return 1 === r && (a = i, i = function (t) {
                return rt().off(t), a.apply(this, arguments)
            }, i.guid = a.guid || (a.guid = rt.guid++)), this.each(function () {
                rt.event.add(this, t, i, n, e)
            })
        },
        one: function (t, e, n, i) {
            return this.on(t, e, n, i, 1)
        },
        off: function (t, e, n) {
            var i, r;
            if (t && t.preventDefault && t.handleObj) return i = t.handleObj, rt(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof t) {
                for (r in t) this.off(r, e, t[r]);
                return this
            }
            return (e === !1 || "function" == typeof e) && (n = e, e = void 0), n === !1 && (n = f), this.each(function () {
                rt.event.remove(this, t, n, e)
            })
        },
        trigger: function (t, e) {
            return this.each(function () {
                rt.event.trigger(t, e, this)
            })
        },
        triggerHandler: function (t, e) {
            var n = this[0];
            return n ? rt.event.trigger(t, e, n, !0) : void 0
        }
    });
    var Ft = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        jt = / jQuery\d+="(?:null|\d+)"/g,
        Lt = new RegExp("<(?:" + Ft + ")[\\s/>]", "i"),
        It = /^\s+/,
        Yt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Rt = /<([\w:]+)/,
        Wt = /<tbody/i,
        zt = /<|&#?\w+;/,
        qt = /<(?:script|style|link)/i,
        Bt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ut = /^$|\/(?:java|ecma)script/i,
        Vt = /^true\/(.*)/,
        Gt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Xt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: nt.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        Qt = m(pt),
        Jt = Qt.appendChild(pt.createElement("div"));
    Xt.optgroup = Xt.option, Xt.tbody = Xt.tfoot = Xt.colgroup = Xt.caption = Xt.thead, Xt.th = Xt.td, rt.extend({
        clone: function (t, e, n) {
            var i, r, o, a, s, l = rt.contains(t.ownerDocument, t);
            if (nt.html5Clone || rt.isXMLDoc(t) || !Lt.test("<" + t.nodeName + ">") ? o = t.cloneNode(!0) : (Jt.innerHTML = t.outerHTML, Jt.removeChild(o = Jt.firstChild)), !(nt.noCloneEvent && nt.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || rt.isXMLDoc(t)))
                for (i = g(o), s = g(t), a = 0; null != (r = s[a]); ++a) i[a] && k(r, i[a]);
            if (e)
                if (n)
                    for (s = s || g(t), i = i || g(o), a = 0; null != (r = s[a]); a++) x(r, i[a]);
                else x(t, o);
            return i = g(o, "script"), i.length > 0 && b(i, !l && g(t, "script")), i = s = r = null, o
        },
        buildFragment: function (t, e, n, i) {
            for (var r, o, a, s, l, u, c, d = t.length, h = m(e), f = [], p = 0; d > p; p++)
                if (o = t[p], o || 0 === o)
                    if ("object" === rt.type(o)) rt.merge(f, o.nodeType ? [o] : o);
                    else if (zt.test(o)) {
                        for (s = s || h.appendChild(e.createElement("div")), l = (Rt.exec(o) || ["", ""])[1].toLowerCase(), c = Xt[l] || Xt._default, s.innerHTML = c[1] + o.replace(Yt, "<$1></$2>") + c[2], r = c[0]; r--;) s = s.lastChild;
                        if (!nt.leadingWhitespace && It.test(o) && f.push(e.createTextNode(It.exec(o)[0])), !nt.tbody)
                            for (o = "table" !== l || Wt.test(o) ? "<table>" !== c[1] || Wt.test(o) ? 0 : s : s.firstChild, r = o && o.childNodes.length; r--;) rt.nodeName(u = o.childNodes[r], "tbody") && !u.childNodes.length && o.removeChild(u);
                        for (rt.merge(f, s.childNodes), s.textContent = ""; s.firstChild;) s.removeChild(s.firstChild);
                        s = h.lastChild
                    } else f.push(e.createTextNode(o));
            for (s && h.removeChild(s), nt.appendChecked || rt.grep(g(f, "input"), v), p = 0; o = f[p++];)
                if ((!i || -1 === rt.inArray(o, i)) && (a = rt.contains(o.ownerDocument, o), s = g(h.appendChild(o), "script"), a && b(s), n))
                    for (r = 0; o = s[r++];) Ut.test(o.type || "") && n.push(o);
            return s = null, h
        },
        cleanData: function (t, e) {
            for (var n, i, r, o, a = 0, s = rt.expando, l = rt.cache, u = nt.deleteExpando, c = rt.event.special; null != (n = t[a]); a++)
                if ((e || rt.acceptData(n)) && (r = n[s], o = r && l[r])) {
                    if (o.events)
                        for (i in o.events) c[i] ? rt.event.remove(n, i) : rt.removeEvent(n, i, o.handle);
                    l[r] && (delete l[r], u ? delete n[s] : typeof n.removeAttribute !== kt ? n.removeAttribute(s) : n[s] = null, G.push(r))
                }
        }
    }), rt.fn.extend({
        text: function (t) {
            return Et(this, function (t) {
                return void 0 === t ? rt.text(this) : this.empty().append((this[0] && this[0].ownerDocument || pt).createTextNode(t))
            }, null, t, arguments.length)
        },
        append: function () {
            return this.domManip(arguments, function (t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = y(this, t);
                    e.appendChild(t)
                }
            })
        },
        prepend: function () {
            return this.domManip(arguments, function (t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = y(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function () {
            return this.domManip(arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function () {
            return this.domManip(arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        remove: function (t, e) {
            for (var n, i = t ? rt.filter(t, this) : this, r = 0; null != (n = i[r]); r++) e || 1 !== n.nodeType || rt.cleanData(g(n)), n.parentNode && (e && rt.contains(n.ownerDocument, n) && b(g(n, "script")), n.parentNode.removeChild(n));
            return this
        },
        empty: function () {
            for (var t, e = 0; null != (t = this[e]); e++) {
                for (1 === t.nodeType && rt.cleanData(g(t, !1)); t.firstChild;) t.removeChild(t.firstChild);
                t.options && rt.nodeName(t, "select") && (t.options.length = 0)
            }
            return this
        },
        clone: function (t, e) {
            return t = null == t ? !1 : t, e = null == e ? t : e, this.map(function () {
                return rt.clone(this, t, e)
            })
        },
        html: function (t) {
            return Et(this, function (t) {
                var e = this[0] || {},
                    n = 0,
                    i = this.length;
                if (void 0 === t) return 1 === e.nodeType ? e.innerHTML.replace(jt, "") : void 0;
                if ("string" == typeof t && !qt.test(t) && (nt.htmlSerialize || !Lt.test(t)) && (nt.leadingWhitespace || !It.test(t)) && !Xt[(Rt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = t.replace(Yt, "<$1></$2>");
                    try {
                        for (; i > n; n++) e = this[n] || {}, 1 === e.nodeType && (rt.cleanData(g(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (r) { }
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function () {
            var t = arguments[0];
            return this.domManip(arguments, function (e) {
                t = this.parentNode, rt.cleanData(g(this)), t && t.replaceChild(e, this)
            }), t && (t.length || t.nodeType) ? this : this.remove()
        },
        detach: function (t) {
            return this.remove(t, !0)
        },
        domManip: function (t, e) {
            t = Q.apply([], t);
            var n, i, r, o, a, s, l = 0,
                u = this.length,
                c = this,
                d = u - 1,
                h = t[0],
                f = rt.isFunction(h);
            if (f || u > 1 && "string" == typeof h && !nt.checkClone && Bt.test(h)) return this.each(function (n) {
                var i = c.eq(n);
                f && (t[0] = h.call(this, n, i.html())), i.domManip(t, e)
            });
            if (u && (s = rt.buildFragment(t, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) {
                for (o = rt.map(g(s, "script"), _), r = o.length; u > l; l++) i = s, l !== d && (i = rt.clone(i, !0, !0), r && rt.merge(o, g(i, "script"))), e.call(this[l], i, l);
                if (r)
                    for (a = o[o.length - 1].ownerDocument, rt.map(o, w), l = 0; r > l; l++) i = o[l], Ut.test(i.type || "") && !rt._data(i, "globalEval") && rt.contains(a, i) && (i.src ? rt._evalUrl && rt._evalUrl(i.src) : rt.globalEval((i.text || i.textContent || i.innerHTML || "").replace(Gt, "")));
                s = n = null
            }
            return this
        }
    }), rt.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (t, e) {
        rt.fn[t] = function (t) {
            for (var n, i = 0, r = [], o = rt(t), a = o.length - 1; a >= i; i++) n = i === a ? this : this.clone(!0), rt(o[i])[e](n), J.apply(r, n.get());
            return this.pushStack(r)
        }
    });
    var Kt, Zt = {};
    ! function () {
        var t;
        nt.shrinkWrapBlocks = function () {
            if (null != t) return t;
            t = !1;
            var e, n, i;
            return n = pt.getElementsByTagName("body")[0], n && n.style ? (e = pt.createElement("div"), i = pt.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(e), typeof e.style.zoom !== kt && (e.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", e.appendChild(pt.createElement("div")).style.width = "5px", t = 3 !== e.offsetWidth), n.removeChild(i), t) : void 0
        }
    } ();
    var te, ee, ne = /^margin/,
        ie = new RegExp("^(" + Tt + ")(?!px)[a-z%]+$", "i"),
        re = /^(top|right|bottom|left)$/;
    t.getComputedStyle ? (te = function (t) {
        return t.ownerDocument.defaultView.getComputedStyle(t, null)
    }, ee = function (t, e, n) {
        var i, r, o, a, s = t.style;
        return n = n || te(t), a = n ? n.getPropertyValue(e) || n[e] : void 0, n && ("" !== a || rt.contains(t.ownerDocument, t) || (a = rt.style(t, e)), ie.test(a) && ne.test(e) && (i = s.width, r = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = i, s.minWidth = r, s.maxWidth = o)), void 0 === a ? a : a + ""
    }) : pt.documentElement.currentStyle && (te = function (t) {
        return t.currentStyle
    }, ee = function (t, e, n) {
        var i, r, o, a, s = t.style;
        return n = n || te(t), a = n ? n[e] : void 0, null == a && s && s[e] && (a = s[e]), ie.test(a) && !re.test(e) && (i = s.left, r = t.runtimeStyle, o = r && r.left, o && (r.left = t.currentStyle.left), s.left = "fontSize" === e ? "1em" : a, a = s.pixelLeft + "px", s.left = i, o && (r.left = o)), void 0 === a ? a : a + "" || "auto"
    }),
        function () {
            function e() {
                var e, n, i, r;
                n = pt.getElementsByTagName("body")[0],
                    n && n.style && (e = pt.createElement("div"), i = pt.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(e), e.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", o = a = !1, l = !0, t.getComputedStyle && (o = "1%" !== (t.getComputedStyle(e, null) || {}).top, a = "4px" === (t.getComputedStyle(e, null) || {
                        width: "4px"
                    }).width, r = e.appendChild(pt.createElement("div")), r.style.cssText = e.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", r.style.marginRight = r.style.width = "0", e.style.width = "1px", l = !parseFloat((t.getComputedStyle(r, null) || {}).marginRight)), e.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", r = e.getElementsByTagName("td"), r[0].style.cssText = "margin:0;border:0;padding:0;display:none", s = 0 === r[0].offsetHeight, s && (r[0].style.display = "", r[1].style.display = "none", s = 0 === r[0].offsetHeight), n.removeChild(i))
            }
            var n, i, r, o, a, s, l;
            n = pt.createElement("div"), n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", r = n.getElementsByTagName("a")[0], i = r && r.style, i && (i.cssText = "float:left;opacity:.5", nt.opacity = "0.5" === i.opacity, nt.cssFloat = !!i.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", nt.clearCloneStyle = "content-box" === n.style.backgroundClip, nt.boxSizing = "" === i.boxSizing || "" === i.MozBoxSizing || "" === i.WebkitBoxSizing, rt.extend(nt, {
                reliableHiddenOffsets: function () {
                    return null == s && e(), s
                },
                boxSizingReliable: function () {
                    return null == a && e(), a
                },
                pixelPosition: function () {
                    return null == o && e(), o
                },
                reliableMarginRight: function () {
                    return null == l && e(), l
                }
            }))
        } (), rt.swap = function (t, e, n, i) {
            var r, o, a = {};
            for (o in e) a[o] = t.style[o], t.style[o] = e[o];
            r = n.apply(t, i || []);
            for (o in e) t.style[o] = a[o];
            return r
        };
    var oe = /alpha\([^)]*\)/i,
        ae = /opacity\s*=\s*([^)]*)/,
        se = /^(none|table(?!-c[ea]).+)/,
        le = new RegExp("^(" + Tt + ")(.*)$", "i"),
        ue = new RegExp("^([+-])=(" + Tt + ")", "i"),
        ce = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        de = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        he = ["Webkit", "O", "Moz", "ms"];
    rt.extend({
        cssHooks: {
            opacity: {
                get: function (t, e) {
                    if (e) {
                        var n = ee(t, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": nt.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function (t, e, n, i) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var r, o, a, s = rt.camelCase(e),
                    l = t.style;
                if (e = rt.cssProps[s] || (rt.cssProps[s] = D(l, s)), a = rt.cssHooks[e] || rt.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (r = a.get(t, !1, i)) ? r : l[e];
                if (o = typeof n, "string" === o && (r = ue.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(rt.css(t, e)), o = "number"), null != n && n === n && ("number" !== o || rt.cssNumber[s] || (n += "px"), nt.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"), !(a && "set" in a && void 0 === (n = a.set(t, n, i))))) try {
                    l[e] = n
                } catch (u) { }
            }
        },
        css: function (t, e, n, i) {
            var r, o, a, s = rt.camelCase(e);
            return e = rt.cssProps[s] || (rt.cssProps[s] = D(t.style, s)), a = rt.cssHooks[e] || rt.cssHooks[s], a && "get" in a && (o = a.get(t, !0, n)), void 0 === o && (o = ee(t, e, i)), "normal" === o && e in de && (o = de[e]), "" === n || n ? (r = parseFloat(o), n === !0 || rt.isNumeric(r) ? r || 0 : o) : o
        }
    }), rt.each(["height", "width"], function (t, e) {
        rt.cssHooks[e] = {
            get: function (t, n, i) {
                return n ? se.test(rt.css(t, "display")) && 0 === t.offsetWidth ? rt.swap(t, ce, function () {
                    return A(t, e, i)
                }) : A(t, e, i) : void 0
            },
            set: function (t, n, i) {
                var r = i && te(t);
                return E(t, n, i ? M(t, e, i, nt.boxSizing && "border-box" === rt.css(t, "boxSizing", !1, r), r) : 0)
            }
        }
    }), nt.opacity || (rt.cssHooks.opacity = {
        get: function (t, e) {
            return ae.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
        },
        set: function (t, e) {
            var n = t.style,
                i = t.currentStyle,
                r = rt.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
                o = i && i.filter || n.filter || "";
            n.zoom = 1, (e >= 1 || "" === e) && "" === rt.trim(o.replace(oe, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === e || i && !i.filter) || (n.filter = oe.test(o) ? o.replace(oe, r) : o + " " + r)
        }
    }), rt.cssHooks.marginRight = T(nt.reliableMarginRight, function (t, e) {
        return e ? rt.swap(t, {
            display: "inline-block"
        }, ee, [t, "marginRight"]) : void 0
    }), rt.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function (t, e) {
        rt.cssHooks[t + e] = {
            expand: function (n) {
                for (var i = 0, r = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) r[t + Dt[i] + e] = o[i] || o[i - 2] || o[0];
                return r
            }
        }, ne.test(t) || (rt.cssHooks[t + e].set = E)
    }), rt.fn.extend({
        css: function (t, e) {
            return Et(this, function (t, e, n) {
                var i, r, o = {},
                    a = 0;
                if (rt.isArray(e)) {
                    for (i = te(t), r = e.length; r > a; a++) o[e[a]] = rt.css(t, e[a], !1, i);
                    return o
                }
                return void 0 !== n ? rt.style(t, e, n) : rt.css(t, e)
            }, t, e, arguments.length > 1)
        },
        show: function () {
            return $(this, !0)
        },
        hide: function () {
            return $(this)
        },
        toggle: function (t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
                $t(this) ? rt(this).show() : rt(this).hide()
            })
        }
    }), rt.Tween = O, O.prototype = {
        constructor: O,
        init: function (t, e, n, i, r, o) {
            this.elem = t, this.prop = n, this.easing = r || "swing", this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = o || (rt.cssNumber[n] ? "" : "px")
        },
        cur: function () {
            var t = O.propHooks[this.prop];
            return t && t.get ? t.get(this) : O.propHooks._default.get(this)
        },
        run: function (t) {
            var e, n = O.propHooks[this.prop];
            return this.options.duration ? this.pos = e = rt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : O.propHooks._default.set(this), this
        }
    }, O.prototype.init.prototype = O.prototype, O.propHooks = {
        _default: {
            get: function (t) {
                var e;
                return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = rt.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0) : t.elem[t.prop]
            },
            set: function (t) {
                rt.fx.step[t.prop] ? rt.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[rt.cssProps[t.prop]] || rt.cssHooks[t.prop]) ? rt.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
            }
        }
    }, O.propHooks.scrollTop = O.propHooks.scrollLeft = {
        set: function (t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, rt.easing = {
        linear: function (t) {
            return t
        },
        swing: function (t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }
    }, rt.fx = O.prototype.init, rt.fx.step = {};
    var fe, pe, me = /^(?:toggle|show|hide)$/,
        ge = new RegExp("^(?:([+-])=|)(" + Tt + ")([a-z%]*)$", "i"),
        ve = /queueHooks$/,
        ye = [F],
        _e = {
            "*": [function (t, e) {
                var n = this.createTween(t, e),
                    i = n.cur(),
                    r = ge.exec(e),
                    o = r && r[3] || (rt.cssNumber[t] ? "" : "px"),
                    a = (rt.cssNumber[t] || "px" !== o && +i) && ge.exec(rt.css(n.elem, t)),
                    s = 1,
                    l = 20;
                if (a && a[3] !== o) {
                    o = o || a[3], r = r || [], a = +i || 1;
                    do s = s || ".5", a /= s, rt.style(n.elem, t, a + o); while (s !== (s = n.cur() / i) && 1 !== s && --l)
                }
                return r && (a = n.start = +a || +i || 0, n.unit = o, n.end = r[1] ? a + (r[1] + 1) * r[2] : +r[2]), n
            }]
        };
    rt.Animation = rt.extend(L, {
        tweener: function (t, e) {
            rt.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
            for (var n, i = 0, r = t.length; r > i; i++) n = t[i], _e[n] = _e[n] || [], _e[n].unshift(e)
        },
        prefilter: function (t, e) {
            e ? ye.unshift(t) : ye.push(t)
        }
    }), rt.speed = function (t, e, n) {
        var i = t && "object" == typeof t ? rt.extend({}, t) : {
            complete: n || !n && e || rt.isFunction(t) && t,
            duration: t,
            easing: n && e || e && !rt.isFunction(e) && e
        };
        return i.duration = rt.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in rt.fx.speeds ? rt.fx.speeds[i.duration] : rt.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function () {
            rt.isFunction(i.old) && i.old.call(this), i.queue && rt.dequeue(this, i.queue)
        }, i
    }, rt.fn.extend({
        fadeTo: function (t, e, n, i) {
            return this.filter($t).css("opacity", 0).show().end().animate({
                opacity: e
            }, t, n, i)
        },
        animate: function (t, e, n, i) {
            var r = rt.isEmptyObject(t),
                o = rt.speed(e, n, i),
                a = function () {
                    var e = L(this, rt.extend({}, t), o);
                    (r || rt._data(this, "finish")) && e.stop(!0)
                };
            return a.finish = a, r || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
        },
        stop: function (t, e, n) {
            var i = function (t) {
                var e = t.stop;
                delete t.stop, e(n)
            };
            return "string" != typeof t && (n = e, e = t, t = void 0), e && t !== !1 && this.queue(t || "fx", []), this.each(function () {
                var e = !0,
                    r = null != t && t + "queueHooks",
                    o = rt.timers,
                    a = rt._data(this);
                if (r) a[r] && a[r].stop && i(a[r]);
                else
                    for (r in a) a[r] && a[r].stop && ve.test(r) && i(a[r]);
                for (r = o.length; r--;) o[r].elem !== this || null != t && o[r].queue !== t || (o[r].anim.stop(n), e = !1, o.splice(r, 1));
                (e || !n) && rt.dequeue(this, t)
            })
        },
        finish: function (t) {
            return t !== !1 && (t = t || "fx"), this.each(function () {
                var e, n = rt._data(this),
                    i = n[t + "queue"],
                    r = n[t + "queueHooks"],
                    o = rt.timers,
                    a = i ? i.length : 0;
                for (n.finish = !0, rt.queue(this, t, []), r && r.stop && r.stop.call(this, !0), e = o.length; e--;) o[e].elem === this && o[e].queue === t && (o[e].anim.stop(!0), o.splice(e, 1));
                for (e = 0; a > e; e++) i[e] && i[e].finish && i[e].finish.call(this);
                delete n.finish
            })
        }
    }), rt.each(["toggle", "show", "hide"], function (t, e) {
        var n = rt.fn[e];
        rt.fn[e] = function (t, i, r) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(N(e, !0), t, i, r)
        }
    }), rt.each({
        slideDown: N("show"),
        slideUp: N("hide"),
        slideToggle: N("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function (t, e) {
        rt.fn[t] = function (t, n, i) {
            return this.animate(e, t, n, i)
        }
    }), rt.timers = [], rt.fx.tick = function () {
        var t, e = rt.timers,
            n = 0;
        for (fe = rt.now(); n < e.length; n++) t = e[n], t() || e[n] !== t || e.splice(n--, 1);
        e.length || rt.fx.stop(), fe = void 0
    }, rt.fx.timer = function (t) {
        rt.timers.push(t), t() ? rt.fx.start() : rt.timers.pop()
    }, rt.fx.interval = 13, rt.fx.start = function () {
        pe || (pe = setInterval(rt.fx.tick, rt.fx.interval))
    }, rt.fx.stop = function () {
        clearInterval(pe), pe = null
    }, rt.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, rt.fn.delay = function (t, e) {
        return t = rt.fx ? rt.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function (e, n) {
            var i = setTimeout(e, t);
            n.stop = function () {
                clearTimeout(i)
            }
        })
    },
        function () {
            var t, e, n, i, r;
            e = pt.createElement("div"), e.setAttribute("className", "t"), e.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = e.getElementsByTagName("a")[0], n = pt.createElement("select"), r = n.appendChild(pt.createElement("option")), t = e.getElementsByTagName("input")[0], i.style.cssText = "top:1px", nt.getSetAttribute = "t" !== e.className, nt.style = /top/.test(i.getAttribute("style")), nt.hrefNormalized = "/a" === i.getAttribute("href"), nt.checkOn = !!t.value, nt.optSelected = r.selected, nt.enctype = !!pt.createElement("form").enctype, n.disabled = !0, nt.optDisabled = !r.disabled, t = pt.createElement("input"), t.setAttribute("value", ""), nt.input = "" === t.getAttribute("value"), t.value = "t", t.setAttribute("type", "radio"), nt.radioValue = "t" === t.value
        } ();
    var we = /\r/g;
    rt.fn.extend({
        val: function (t) {
            var e, n, i, r = this[0]; {
                if (arguments.length) return i = rt.isFunction(t), this.each(function (n) {
                    var r;
                    1 === this.nodeType && (r = i ? t.call(this, n, rt(this).val()) : t, null == r ? r = "" : "number" == typeof r ? r += "" : rt.isArray(r) && (r = rt.map(r, function (t) {
                        return null == t ? "" : t + ""
                    })), e = rt.valHooks[this.type] || rt.valHooks[this.nodeName.toLowerCase()], e && "set" in e && void 0 !== e.set(this, r, "value") || (this.value = r))
                });
                if (r) return e = rt.valHooks[r.type] || rt.valHooks[r.nodeName.toLowerCase()], e && "get" in e && void 0 !== (n = e.get(r, "value")) ? n : (n = r.value, "string" == typeof n ? n.replace(we, "") : null == n ? "" : n)
            }
        }
    }), rt.extend({
        valHooks: {
            option: {
                get: function (t) {
                    var e = rt.find.attr(t, "value");
                    return null != e ? e : rt.trim(rt.text(t))
                }
            },
            select: {
                get: function (t) {
                    for (var e, n, i = t.options, r = t.selectedIndex, o = "select-one" === t.type || 0 > r, a = o ? null : [], s = o ? r + 1 : i.length, l = 0 > r ? s : o ? r : 0; s > l; l++)
                        if (n = i[l], (n.selected || l === r) && (nt.optDisabled ? !n.disabled : null === n.getAttribute("disabled")) && (!n.parentNode.disabled || !rt.nodeName(n.parentNode, "optgroup"))) {
                            if (e = rt(n).val(), o) return e;
                            a.push(e)
                        }
                    return a
                },
                set: function (t, e) {
                    for (var n, i, r = t.options, o = rt.makeArray(e), a = r.length; a--;)
                        if (i = r[a], rt.inArray(rt.valHooks.option.get(i), o) >= 0) try {
                            i.selected = n = !0
                        } catch (s) {
                            i.scrollHeight
                        } else i.selected = !1;
                    return n || (t.selectedIndex = -1), r
                }
            }
        }
    }), rt.each(["radio", "checkbox"], function () {
        rt.valHooks[this] = {
            set: function (t, e) {
                return rt.isArray(e) ? t.checked = rt.inArray(rt(t).val(), e) >= 0 : void 0
            }
        }, nt.checkOn || (rt.valHooks[this].get = function (t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    });
    var be, xe, ke = rt.expr.attrHandle,
        Ce = /^(?:checked|selected)$/i,
        Se = nt.getSetAttribute,
        Te = nt.input;
    rt.fn.extend({
        attr: function (t, e) {
            return Et(this, rt.attr, t, e, arguments.length > 1)
        },
        removeAttr: function (t) {
            return this.each(function () {
                rt.removeAttr(this, t)
            })
        }
    }), rt.extend({
        attr: function (t, e, n) {
            var i, r, o = t.nodeType;
            if (t && 3 !== o && 8 !== o && 2 !== o) return typeof t.getAttribute === kt ? rt.prop(t, e, n) : (1 === o && rt.isXMLDoc(t) || (e = e.toLowerCase(), i = rt.attrHooks[e] || (rt.expr.match.bool.test(e) ? xe : be)), void 0 === n ? i && "get" in i && null !== (r = i.get(t, e)) ? r : (r = rt.find.attr(t, e), null == r ? void 0 : r) : null !== n ? i && "set" in i && void 0 !== (r = i.set(t, n, e)) ? r : (t.setAttribute(e, n + ""), n) : void rt.removeAttr(t, e))
        },
        removeAttr: function (t, e) {
            var n, i, r = 0,
                o = e && e.match(_t);
            if (o && 1 === t.nodeType)
                for (; n = o[r++];) i = rt.propFix[n] || n, rt.expr.match.bool.test(n) ? Te && Se || !Ce.test(n) ? t[i] = !1 : t[rt.camelCase("default-" + n)] = t[i] = !1 : rt.attr(t, n, ""), t.removeAttribute(Se ? n : i)
        },
        attrHooks: {
            type: {
                set: function (t, e) {
                    if (!nt.radioValue && "radio" === e && rt.nodeName(t, "input")) {
                        var n = t.value;
                        return t.setAttribute("type", e), n && (t.value = n), e
                    }
                }
            }
        }
    }), xe = {
        set: function (t, e, n) {
            return e === !1 ? rt.removeAttr(t, n) : Te && Se || !Ce.test(n) ? t.setAttribute(!Se && rt.propFix[n] || n, n) : t[rt.camelCase("default-" + n)] = t[n] = !0, n
        }
    }, rt.each(rt.expr.match.bool.source.match(/\w+/g), function (t, e) {
        var n = ke[e] || rt.find.attr;
        ke[e] = Te && Se || !Ce.test(e) ? function (t, e, i) {
            var r, o;
            return i || (o = ke[e], ke[e] = r, r = null != n(t, e, i) ? e.toLowerCase() : null, ke[e] = o), r
        } : function (t, e, n) {
            return n ? void 0 : t[rt.camelCase("default-" + e)] ? e.toLowerCase() : null
        }
    }), Te && Se || (rt.attrHooks.value = {
        set: function (t, e, n) {
            return rt.nodeName(t, "input") ? void (t.defaultValue = e) : be && be.set(t, e, n)
        }
    }), Se || (be = {
        set: function (t, e, n) {
            var i = t.getAttributeNode(n);
            return i || t.setAttributeNode(i = t.ownerDocument.createAttribute(n)), i.value = e += "", "value" === n || e === t.getAttribute(n) ? e : void 0
        }
    }, ke.id = ke.name = ke.coords = function (t, e, n) {
        var i;
        return n ? void 0 : (i = t.getAttributeNode(e)) && "" !== i.value ? i.value : null
    }, rt.valHooks.button = {
        get: function (t, e) {
            var n = t.getAttributeNode(e);
            return n && n.specified ? n.value : void 0
        },
        set: be.set
    }, rt.attrHooks.contenteditable = {
        set: function (t, e, n) {
            be.set(t, "" === e ? !1 : e, n)
        }
    }, rt.each(["width", "height"], function (t, e) {
        rt.attrHooks[e] = {
            set: function (t, n) {
                return "" === n ? (t.setAttribute(e, "auto"), n) : void 0
            }
        }
    })), nt.style || (rt.attrHooks.style = {
        get: function (t) {
            return t.style.cssText || void 0
        },
        set: function (t, e) {
            return t.style.cssText = e + ""
        }
    });
    var De = /^(?:input|select|textarea|button|object)$/i,
        $e = /^(?:a|area)$/i;
    rt.fn.extend({
        prop: function (t, e) {
            return Et(this, rt.prop, t, e, arguments.length > 1)
        },
        removeProp: function (t) {
            return t = rt.propFix[t] || t, this.each(function () {
                try {
                    this[t] = void 0, delete this[t]
                } catch (e) { }
            })
        }
    }), rt.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function (t, e, n) {
            var i, r, o, a = t.nodeType;
            if (t && 3 !== a && 8 !== a && 2 !== a) return o = 1 !== a || !rt.isXMLDoc(t), o && (e = rt.propFix[e] || e, r = rt.propHooks[e]), void 0 !== n ? r && "set" in r && void 0 !== (i = r.set(t, n, e)) ? i : t[e] = n : r && "get" in r && null !== (i = r.get(t, e)) ? i : t[e]
        },
        propHooks: {
            tabIndex: {
                get: function (t) {
                    var e = rt.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : De.test(t.nodeName) || $e.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        }
    }), nt.hrefNormalized || rt.each(["href", "src"], function (t, e) {
        rt.propHooks[e] = {
            get: function (t) {
                return t.getAttribute(e, 4)
            }
        }
    }), nt.optSelected || (rt.propHooks.selected = {
        get: function (t) {
            var e = t.parentNode;
            return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
        }
    }), rt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        rt.propFix[this.toLowerCase()] = this
    }), nt.enctype || (rt.propFix.enctype = "encoding");
    var Ee = /[\t\r\n\f]/g;
    rt.fn.extend({
        addClass: function (t) {
            var e, n, i, r, o, a, s = 0,
                l = this.length,
                u = "string" == typeof t && t;
            if (rt.isFunction(t)) return this.each(function (e) {
                rt(this).addClass(t.call(this, e, this.className))
            });
            if (u)
                for (e = (t || "").match(_t) || []; l > s; s++)
                    if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ee, " ") : " ")) {
                        for (o = 0; r = e[o++];) i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        a = rt.trim(i), n.className !== a && (n.className = a)
                    }
            return this
        },
        removeClass: function (t) {
            var e, n, i, r, o, a, s = 0,
                l = this.length,
                u = 0 === arguments.length || "string" == typeof t && t;
            if (rt.isFunction(t)) return this.each(function (e) {
                rt(this).removeClass(t.call(this, e, this.className))
            });
            if (u)
                for (e = (t || "").match(_t) || []; l > s; s++)
                    if (n = this[s], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Ee, " ") : "")) {
                        for (o = 0; r = e[o++];)
                            for (; i.indexOf(" " + r + " ") >= 0;) i = i.replace(" " + r + " ", " ");
                        a = t ? rt.trim(i) : "", n.className !== a && (n.className = a)
                    }
            return this
        },
        toggleClass: function (t, e) {
            var n = typeof t;
            return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : rt.isFunction(t) ? this.each(function (n) {
                rt(this).toggleClass(t.call(this, n, this.className, e), e)
            }) : this.each(function () {
                if ("string" === n)
                    for (var e, i = 0, r = rt(this), o = t.match(_t) || []; e = o[i++];) r.hasClass(e) ? r.removeClass(e) : r.addClass(e);
                else (n === kt || "boolean" === n) && (this.className && rt._data(this, "__className__", this.className), this.className = this.className || t === !1 ? "" : rt._data(this, "__className__") || "")
            })
        },
        hasClass: function (t) {
            for (var e = " " + t + " ", n = 0, i = this.length; i > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Ee, " ").indexOf(e) >= 0) return !0;
            return !1
        }
    }), rt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, e) {
        rt.fn[e] = function (t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }), rt.fn.extend({
        hover: function (t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        },
        bind: function (t, e, n) {
            return this.on(t, null, e, n)
        },
        unbind: function (t, e) {
            return this.off(t, null, e)
        },
        delegate: function (t, e, n, i) {
            return this.on(e, t, n, i)
        },
        undelegate: function (t, e, n) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
        }
    });
    var Me = rt.now(),
        Ae = /\?/,
        Oe = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    rt.parseJSON = function (e) {
        if (t.JSON && t.JSON.parse) return t.JSON.parse(e + "");
        var n, i = null,
            r = rt.trim(e + "");
        return r && !rt.trim(r.replace(Oe, function (t, e, r, o) {
            return n && e && (i = 0), 0 === i ? t : (n = r || e, i += !o - !r, "")
        })) ? Function("return " + r)() : rt.error("Invalid JSON: " + e)
    }, rt.parseXML = function (e) {
        var n, i;
        if (!e || "string" != typeof e) return null;
        try {
            t.DOMParser ? (i = new DOMParser, n = i.parseFromString(e, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(e))
        } catch (r) {
            n = void 0
        }
        return n && n.documentElement && !n.getElementsByTagName("parsererror").length || rt.error("Invalid XML: " + e), n
    };
    var He, Ne, Pe = /#.*$/,
        Fe = /([?&])_=[^&]*/,
        je = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Le = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Ie = /^(?:GET|HEAD)$/,
        Ye = /^\/\//,
        Re = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        We = {},
        ze = {},
        qe = "*/".concat("*");
    try {
        Ne = location.href
    } catch (Be) {
        Ne = pt.createElement("a"), Ne.href = "", Ne = Ne.href
    }
    He = Re.exec(Ne.toLowerCase()) || [], rt.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Ne,
            type: "GET",
            isLocal: Le.test(He[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": qe,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": rt.parseJSON,
                "text xml": rt.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function (t, e) {
            return e ? R(R(t, rt.ajaxSettings), e) : R(rt.ajaxSettings, t)
        },
        ajaxPrefilter: I(We),
        ajaxTransport: I(ze),
        ajax: function (t, e) {
            function n(t, e, n, i) {
                var r, c, v, y, w, x = e;
                2 !== _ && (_ = 2, s && clearTimeout(s), u = void 0, a = i || "", b.readyState = t > 0 ? 4 : 0, r = t >= 200 && 300 > t || 304 === t, n && (y = W(d, b, n)), y = z(d, y, b, r), r ? (d.ifModified && (w = b.getResponseHeader("Last-Modified"), w && (rt.lastModified[o] = w), w = b.getResponseHeader("etag"), w && (rt.etag[o] = w)), 204 === t || "HEAD" === d.type ? x = "nocontent" : 304 === t ? x = "notmodified" : (x = y.state, c = y.data, v = y.error, r = !v)) : (v = x, (t || !x) && (x = "error", 0 > t && (t = 0))), b.status = t, b.statusText = (e || x) + "", r ? p.resolveWith(h, [c, x, b]) : p.rejectWith(h, [b, x, v]), b.statusCode(g), g = void 0, l && f.trigger(r ? "ajaxSuccess" : "ajaxError", [b, d, r ? c : v]), m.fireWith(h, [b, x]), l && (f.trigger("ajaxComplete", [b, d]), --rt.active || rt.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (e = t, t = void 0), e = e || {};
            var i, r, o, a, s, l, u, c, d = rt.ajaxSetup({}, e),
                h = d.context || d,
                f = d.context && (h.nodeType || h.jquery) ? rt(h) : rt.event,
                p = rt.Deferred(),
                m = rt.Callbacks("once memory"),
                g = d.statusCode || {},
                v = {},
                y = {},
                _ = 0,
                w = "canceled",
                b = {
                    readyState: 0,
                    getResponseHeader: function (t) {
                        var e;
                        if (2 === _) {
                            if (!c)
                                for (c = {}; e = je.exec(a);) c[e[1].toLowerCase()] = e[2];
                            e = c[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    },
                    getAllResponseHeaders: function () {
                        return 2 === _ ? a : null
                    },
                    setRequestHeader: function (t, e) {
                        var n = t.toLowerCase();
                        return _ || (t = y[n] = y[n] || t, v[t] = e), this
                    },
                    overrideMimeType: function (t) {
                        return _ || (d.mimeType = t), this
                    },
                    statusCode: function (t) {
                        var e;
                        if (t)
                            if (2 > _)
                                for (e in t) g[e] = [g[e], t[e]];
                            else b.always(t[b.status]);
                        return this
                    },
                    abort: function (t) {
                        var e = t || w;
                        return u && u.abort(e), n(0, e), this
                    }
                };
            if (p.promise(b).complete = m.add, b.success = b.done, b.error = b.fail, d.url = ((t || d.url || Ne) + "").replace(Pe, "").replace(Ye, He[1] + "//"), d.type = e.method || e.type || d.method || d.type, d.dataTypes = rt.trim(d.dataType || "*").toLowerCase().match(_t) || [""], null == d.crossDomain && (i = Re.exec(d.url.toLowerCase()), d.crossDomain = !(!i || i[1] === He[1] && i[2] === He[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (He[3] || ("http:" === He[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = rt.param(d.data, d.traditional)), Y(We, d, e, b), 2 === _) return b;
            l = d.global, l && 0 === rt.active++ && rt.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Ie.test(d.type), o = d.url, d.hasContent || (d.data && (o = d.url += (Ae.test(o) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Fe.test(o) ? o.replace(Fe, "$1_=" + Me++) : o + (Ae.test(o) ? "&" : "?") + "_=" + Me++)), d.ifModified && (rt.lastModified[o] && b.setRequestHeader("If-Modified-Since", rt.lastModified[o]), rt.etag[o] && b.setRequestHeader("If-None-Match", rt.etag[o])), (d.data && d.hasContent && d.contentType !== !1 || e.contentType) && b.setRequestHeader("Content-Type", d.contentType), b.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + qe + "; q=0.01" : "") : d.accepts["*"]);
            for (r in d.headers) b.setRequestHeader(r, d.headers[r]);
            if (d.beforeSend && (d.beforeSend.call(h, b, d) === !1 || 2 === _)) return b.abort();
            w = "abort";
            for (r in {
                success: 1,
                error: 1,
                complete: 1
            }) b[r](d[r]);
            if (u = Y(ze, d, e, b)) {
                b.readyState = 1, l && f.trigger("ajaxSend", [b, d]), d.async && d.timeout > 0 && (s = setTimeout(function () {
                    b.abort("timeout")
                }, d.timeout));
                try {
                    _ = 1, u.send(v, n)
                } catch (x) {
                    if (!(2 > _)) throw x;
                    n(-1, x)
                }
            } else n(-1, "No Transport");
            return b
        },
        getJSON: function (t, e, n) {
            return rt.get(t, e, n, "json")
        },
        getScript: function (t, e) {
            return rt.get(t, void 0, e, "script")
        }
    }), rt.each(["get", "post"], function (t, e) {
        rt[e] = function (t, n, i, r) {
            return rt.isFunction(n) && (r = r || i, i = n, n = void 0), rt.ajax({
                url: t,
                type: e,
                dataType: r,
                data: n,
                success: i
            })
        }
    }), rt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
        rt.fn[e] = function (t) {
            return this.on(e, t)
        }
    }), rt._evalUrl = function (t) {
        return rt.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }, rt.fn.extend({
        wrapAll: function (t) {
            if (rt.isFunction(t)) return this.each(function (e) {
                rt(this).wrapAll(t.call(this, e))
            });
            if (this[0]) {
                var e = rt(t, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
                    for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;) t = t.firstChild;
                    return t
                }).append(this)
            }
            return this
        },
        wrapInner: function (t) {
            return rt.isFunction(t) ? this.each(function (e) {
                rt(this).wrapInner(t.call(this, e))
            }) : this.each(function () {
                var e = rt(this),
                    n = e.contents();
                n.length ? n.wrapAll(t) : e.append(t)
            })
        },
        wrap: function (t) {
            var e = rt.isFunction(t);
            return this.each(function (n) {
                rt(this).wrapAll(e ? t.call(this, n) : t)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                rt.nodeName(this, "body") || rt(this).replaceWith(this.childNodes)
            }).end()
        }
    }), rt.expr.filters.hidden = function (t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0 || !nt.reliableHiddenOffsets() && "none" === (t.style && t.style.display || rt.css(t, "display"))
    }, rt.expr.filters.visible = function (t) {
        return !rt.expr.filters.hidden(t)
    };
    var Ue = /%20/g,
        Ve = /\[\]$/,
        Ge = /\r?\n/g,
        Xe = /^(?:submit|button|image|reset|file)$/i,
        Qe = /^(?:input|select|textarea|keygen)/i;
    rt.param = function (t, e) {
        var n, i = [],
            r = function (t, e) {
                e = rt.isFunction(e) ? e() : null == e ? "" : e, i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
            };
        if (void 0 === e && (e = rt.ajaxSettings && rt.ajaxSettings.traditional), rt.isArray(t) || t.jquery && !rt.isPlainObject(t)) rt.each(t, function () {
            r(this.name, this.value)
        });
        else
            for (n in t) q(n, t[n], e, r);
        return i.join("&").replace(Ue, "+")
    }, rt.fn.extend({
        serialize: function () {
            return rt.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map(function () {
                var t = rt.prop(this, "elements");
                return t ? rt.makeArray(t) : this
            }).filter(function () {
                var t = this.type;
                return this.name && !rt(this).is(":disabled") && Qe.test(this.nodeName) && !Xe.test(t) && (this.checked || !Mt.test(t))
            }).map(function (t, e) {
                var n = rt(this).val();
                return null == n ? null : rt.isArray(n) ? rt.map(n, function (t) {
                    return {
                        name: e.name,
                        value: t.replace(Ge, "\r\n")
                    }
                }) : {
                        name: e.name,
                        value: n.replace(Ge, "\r\n")
                    }
            }).get()
        }
    }), rt.ajaxSettings.xhr = void 0 !== t.ActiveXObject ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && B() || U()
    } : B;
    var Je = 0,
        Ke = {},
        Ze = rt.ajaxSettings.xhr();
    t.ActiveXObject && rt(t).on("unload", function () {
        for (var t in Ke) Ke[t](void 0, !0)
    }), nt.cors = !!Ze && "withCredentials" in Ze, Ze = nt.ajax = !!Ze, Ze && rt.ajaxTransport(function (t) {
        if (!t.crossDomain || nt.cors) {
            var e;
            return {
                send: function (n, i) {
                    var r, o = t.xhr(),
                        a = ++Je;
                    if (o.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                        for (r in t.xhrFields) o[r] = t.xhrFields[r];
                    t.mimeType && o.overrideMimeType && o.overrideMimeType(t.mimeType), t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (r in n) void 0 !== n[r] && o.setRequestHeader(r, n[r] + "");
                    o.send(t.hasContent && t.data || null), e = function (n, r) {
                        var s, l, u;
                        if (e && (r || 4 === o.readyState))
                            if (delete Ke[a], e = void 0, o.onreadystatechange = rt.noop, r) 4 !== o.readyState && o.abort();
                            else {
                                u = {}, s = o.status, "string" == typeof o.responseText && (u.text = o.responseText);
                                try {
                                    l = o.statusText
                                } catch (c) {
                                    l = ""
                                }
                                s || !t.isLocal || t.crossDomain ? 1223 === s && (s = 204) : s = u.text ? 200 : 404
                            }
                        u && i(s, l, u, o.getAllResponseHeaders())
                    }, t.async ? 4 === o.readyState ? setTimeout(e) : o.onreadystatechange = Ke[a] = e : e()
                },
                abort: function () {
                    e && e(void 0, !0)
                }
            }
        }
    }), rt.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function (t) {
                return rt.globalEval(t), t
            }
        }
    }), rt.ajaxPrefilter("script", function (t) {
        void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
    }), rt.ajaxTransport("script", function (t) {
        if (t.crossDomain) {
            var e, n = pt.head || rt("head")[0] || pt.documentElement;
            return {
                send: function (i, r) {
                    e = pt.createElement("script"), e.async = !0, t.scriptCharset && (e.charset = t.scriptCharset), e.src = t.url, e.onload = e.onreadystatechange = function (t, n) {
                        (n || !e.readyState || /loaded|complete/.test(e.readyState)) && (e.onload = e.onreadystatechange = null, e.parentNode && e.parentNode.removeChild(e), e = null, n || r(200, "success"))
                    }, n.insertBefore(e, n.firstChild)
                },
                abort: function () {
                    e && e.onload(void 0, !0)
                }
            }
        }
    });
    var tn = [],
        en = /(=)\?(?=&|$)|\?\?/;
    rt.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var t = tn.pop() || rt.expando + "_" + Me++;
            return this[t] = !0, t
        }
    }), rt.ajaxPrefilter("json jsonp", function (e, n, i) {
        var r, o, a, s = e.jsonp !== !1 && (en.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && en.test(e.data) && "data");
        return s || "jsonp" === e.dataTypes[0] ? (r = e.jsonpCallback = rt.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(en, "$1" + r) : e.jsonp !== !1 && (e.url += (Ae.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function () {
            return a || rt.error(r + " was not called"), a[0]
        }, e.dataTypes[0] = "json", o = t[r], t[r] = function () {
            a = arguments
        }, i.always(function () {
            t[r] = o, e[r] && (e.jsonpCallback = n.jsonpCallback, tn.push(r)), a && rt.isFunction(o) && o(a[0]), a = o = void 0
        }), "script") : void 0
    }), rt.parseHTML = function (t, e, n) {
        if (!t || "string" != typeof t) return null;
        "boolean" == typeof e && (n = e, e = !1), e = e || pt;
        var i = dt.exec(t),
            r = !n && [];
        return i ? [e.createElement(i[1])] : (i = rt.buildFragment([t], e, r), r && r.length && rt(r).remove(), rt.merge([], i.childNodes))
    };
    var nn = rt.fn.load;
    rt.fn.load = function (t, e, n) {
        if ("string" != typeof t && nn) return nn.apply(this, arguments);
        var i, r, o, a = this,
            s = t.indexOf(" ");
        return s >= 0 && (i = rt.trim(t.slice(s, t.length)), t = t.slice(0, s)), rt.isFunction(e) ? (n = e, e = void 0) : e && "object" == typeof e && (o = "POST"), a.length > 0 && rt.ajax({
            url: t,
            type: o,
            dataType: "html",
            data: e
        }).done(function (t) {
            r = arguments, a.html(i ? rt("<div>").append(rt.parseHTML(t)).find(i) : t)
        }).complete(n && function (t, e) {
            a.each(n, r || [t.responseText, e, t])
        }), this
    }, rt.expr.filters.animated = function (t) {
        return rt.grep(rt.timers, function (e) {
            return t === e.elem
        }).length
    };
    var rn = t.document.documentElement;
    rt.offset = {
        setOffset: function (t, e, n) {
            var i, r, o, a, s, l, u, c = rt.css(t, "position"),
                d = rt(t),
                h = {};
            "static" === c && (t.style.position = "relative"), s = d.offset(), o = rt.css(t, "top"), l = rt.css(t, "left"), u = ("absolute" === c || "fixed" === c) && rt.inArray("auto", [o, l]) > -1, u ? (i = d.position(), a = i.top, r = i.left) : (a = parseFloat(o) || 0, r = parseFloat(l) || 0), rt.isFunction(e) && (e = e.call(t, n, s)), null != e.top && (h.top = e.top - s.top + a), null != e.left && (h.left = e.left - s.left + r), "using" in e ? e.using.call(t, h) : d.css(h)
        }
    }, rt.fn.extend({
        offset: function (t) {
            if (arguments.length) return void 0 === t ? this : this.each(function (e) {
                rt.offset.setOffset(this, t, e)
            });
            var e, n, i = {
                top: 0,
                left: 0
            },
                r = this[0],
                o = r && r.ownerDocument;
            if (o) return e = o.documentElement, rt.contains(e, r) ? (typeof r.getBoundingClientRect !== kt && (i = r.getBoundingClientRect()), n = V(o), {
                top: i.top + (n.pageYOffset || e.scrollTop) - (e.clientTop || 0),
                left: i.left + (n.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)
            }) : i
        },
        position: function () {
            if (this[0]) {
                var t, e, n = {
                    top: 0,
                    left: 0
                },
                    i = this[0];
                return "fixed" === rt.css(i, "position") ? e = i.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), rt.nodeName(t[0], "html") || (n = t.offset()), n.top += rt.css(t[0], "borderTopWidth", !0), n.left += rt.css(t[0], "borderLeftWidth", !0)), {
                    top: e.top - n.top - rt.css(i, "marginTop", !0),
                    left: e.left - n.left - rt.css(i, "marginLeft", !0)
                }
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var t = this.offsetParent || rn; t && !rt.nodeName(t, "html") && "static" === rt.css(t, "position");) t = t.offsetParent;
                return t || rn
            })
        }
    }), rt.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function (t, e) {
        var n = /Y/.test(e);
        rt.fn[t] = function (i) {
            return Et(this, function (t, i, r) {
                var o = V(t);
                return void 0 === r ? o ? e in o ? o[e] : o.document.documentElement[i] : t[i] : void (o ? o.scrollTo(n ? rt(o).scrollLeft() : r, n ? r : rt(o).scrollTop()) : t[i] = r)
            }, t, i, arguments.length, null)
        }
    }), rt.each(["top", "left"], function (t, e) {
        rt.cssHooks[e] = T(nt.pixelPosition, function (t, n) {
            return n ? (n = ee(t, e), ie.test(n) ? rt(t).position()[e] + "px" : n) : void 0
        })
    }), rt.each({
        Height: "height",
        Width: "width"
    }, function (t, e) {
        rt.each({
            padding: "inner" + t,
            content: e,
            "": "outer" + t
        }, function (n, i) {
            rt.fn[i] = function (i, r) {
                var o = arguments.length && (n || "boolean" != typeof i),
                    a = n || (i === !0 || r === !0 ? "margin" : "border");
                return Et(this, function (e, n, i) {
                    var r;
                    return rt.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === i ? rt.css(e, n, a) : rt.style(e, n, i, a)
                }, e, o ? i : void 0, o, null)
            }
        })
    }), rt.fn.size = function () {
        return this.length
    }, rt.fn.andSelf = rt.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return rt
    });
    var on = t.jQuery,
        an = t.$;
    return rt.noConflict = function (e) {
        return t.$ === rt && (t.$ = an), e && t.jQuery === rt && (t.jQuery = on), rt
    }, typeof e === kt && (t.jQuery = t.$ = rt), rt
}),
    function (t, e) {
        t.rails !== e && t.error("jquery-ujs has already been loaded!");
        var n, i = t(document);
        t.rails = n = {
            linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with], a[data-disable]",
            buttonClickSelector: "button[data-remote]:not(form button), button[data-confirm]:not(form button)",
            inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
            formSubmitSelector: "form",
            formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not([type]), input[type=submit][form], input[type=image][form], button[type=submit][form], button[form]:not([type])",
            disableSelector: "input[data-disable-with]:enabled, button[data-disable-with]:enabled, textarea[data-disable-with]:enabled, input[data-disable]:enabled, button[data-disable]:enabled, textarea[data-disable]:enabled",
            enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled, input[data-disable]:disabled, button[data-disable]:disabled, textarea[data-disable]:disabled",
            requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])",
            fileInputSelector: "input[type=file]",
            linkDisableSelector: "a[data-disable-with], a[data-disable]",
            buttonDisableSelector: "button[data-remote][data-disable-with], button[data-remote][data-disable]",
            CSRFProtection: function (e) {
                var n = t('meta[name="csrf-token"]').attr("content");
                n && e.setRequestHeader("X-CSRF-Token", n)
            },
            refreshCSRFTokens: function () {
                var e = t("meta[name=csrf-token]").attr("content"),
                    n = t("meta[name=csrf-param]").attr("content");
                t('form input[name="' + n + '"]').val(e)
            },
            fire: function (e, n, i) {
                var r = t.Event(n);
                return e.trigger(r, i), r.result !== !1
            },
            confirm: function (t) {
                return confirm(t)
            },
            ajax: function (e) {
                return t.ajax(e)
            },
            href: function (t) {
                return t[0].href
            },
            handleRemote: function (i) {
                var r, o, a, s, l, u;
                if (n.fire(i, "ajax:before")) {
                    if (s = i.data("with-credentials") || null, l = i.data("type") || t.ajaxSettings && t.ajaxSettings.dataType, i.is("form")) {
                        r = i.attr("method"), o = i.attr("action"), a = i.serializeArray();
                        var c = i.data("ujs:submit-button");
                        c && (a.push(c), i.data("ujs:submit-button", null))
                    } else i.is(n.inputChangeSelector) ? (r = i.data("method"), o = i.data("url"), a = i.serialize(), i.data("params") && (a = a + "&" + i.data("params"))) : i.is(n.buttonClickSelector) ? (r = i.data("method") || "get", o = i.data("url"), a = i.serialize(), i.data("params") && (a = a + "&" + i.data("params"))) : (r = i.data("method"), o = n.href(i), a = i.data("params") || null);
                    return u = {
                        type: r || "GET",
                        data: a,
                        dataType: l,
                        beforeSend: function (t, r) {
                            return r.dataType === e && t.setRequestHeader("accept", "*/*;q=0.5, " + r.accepts.script), n.fire(i, "ajax:beforeSend", [t, r]) ? void i.trigger("ajax:send", t) : !1
                        },
                        success: function (t, e, n) {
                            i.trigger("ajax:success", [t, e, n])
                        },
                        complete: function (t, e) {
                            i.trigger("ajax:complete", [t, e])
                        },
                        error: function (t, e, n) {
                            i.trigger("ajax:error", [t, e, n])
                        },
                        crossDomain: n.isCrossDomain(o)
                    }, s && (u.xhrFields = {
                        withCredentials: s
                    }), o && (u.url = o), n.ajax(u)
                }
                return !1
            },
            isCrossDomain: function (t) {
                var e = document.createElement("a");
                e.href = location.href;
                var n = document.createElement("a");
                try {
                    return n.href = t, n.href = n.href, !n.protocol || !n.host || e.protocol + "//" + e.host != n.protocol + "//" + n.host
                } catch (i) {
                    return !0
                }
            },
            handleMethod: function (i) {
                var r = n.href(i),
                    o = i.data("method"),
                    a = i.attr("target"),
                    s = t("meta[name=csrf-token]").attr("content"),
                    l = t("meta[name=csrf-param]").attr("content"),
                    u = t('<form method="post" action="' + r + '"></form>'),
                    c = '<input name="_method" value="' + o + '" type="hidden" />';
                l === e || s === e || n.isCrossDomain(r) || (c += '<input name="' + l + '" value="' + s + '" type="hidden" />'), a && u.attr("target", a), u.hide().append(c).appendTo("body"), u.submit()
            },
            formElements: function (e, n) {
                return e.is("form") ? t(e[0].elements).filter(n) : e.find(n)
            },
            disableFormElements: function (e) {
                n.formElements(e, n.disableSelector).each(function () {
                    n.disableFormElement(t(this))
                })
            },
            disableFormElement: function (t) {
                var n, i;
                n = t.is("button") ? "html" : "val", i = t.data("disable-with"), t.data("ujs:enable-with", t[n]()), i !== e && t[n](i), t.prop("disabled", !0)
            },
            enableFormElements: function (e) {
                n.formElements(e, n.enableSelector).each(function () {
                    n.enableFormElement(t(this))
                })
            },
            enableFormElement: function (t) {
                var e = t.is("button") ? "html" : "val";
                t.data("ujs:enable-with") && t[e](t.data("ujs:enable-with")), t.prop("disabled", !1)
            },
            allowAction: function (t) {
                var e, i = t.data("confirm"),
                    r = !1;
                return i ? (n.fire(t, "confirm") && (r = n.confirm(i), e = n.fire(t, "confirm:complete", [r])), r && e) : !0
            },
            blankInputs: function (e, n, i) {
                var r, o, a = t(),
                    s = n || "input,textarea",
                    l = e.find(s);
                return l.each(function () {
                    if (r = t(this), o = r.is("input[type=checkbox],input[type=radio]") ? r.is(":checked") : r.val(), !o == !i) {
                        if (r.is("input[type=radio]") && l.filter('input[type=radio]:checked[name="' + r.attr("name") + '"]').length) return !0;
                        a = a.add(r)
                    }
                }), a.length ? a : !1
            },
            nonBlankInputs: function (t, e) {
                return n.blankInputs(t, e, !0)
            },
            stopEverything: function (e) {
                return t(e.target).trigger("ujs:everythingStopped"), e.stopImmediatePropagation(), !1
            },
            disableElement: function (t) {
                var i = t.data("disable-with");
                t.data("ujs:enable-with", t.html()), i !== e && t.html(i), t.bind("click.railsDisable", function (t) {
                    return n.stopEverything(t)
                })
            },
            enableElement: function (t) {
                t.data("ujs:enable-with") !== e && (t.html(t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.unbind("click.railsDisable")
            }
        }, n.fire(i, "rails:attachBindings") && (t.ajaxPrefilter(function (t, e, i) {
            t.crossDomain || n.CSRFProtection(i)
        }), i.delegate(n.linkDisableSelector, "ajax:complete", function () {
            n.enableElement(t(this))
        }), i.delegate(n.buttonDisableSelector, "ajax:complete", function () {
            n.enableFormElement(t(this))
        }), i.delegate(n.linkClickSelector, "click.rails", function (i) {
            var r = t(this),
                o = r.data("method"),
                a = r.data("params"),
                s = i.metaKey || i.ctrlKey;
            if (!n.allowAction(r)) return n.stopEverything(i);
            if (!s && r.is(n.linkDisableSelector) && n.disableElement(r), r.data("remote") !== e) {
                if (s && (!o || "GET" === o) && !a) return !0;
                var l = n.handleRemote(r);
                return l === !1 ? n.enableElement(r) : l.error(function () {
                    n.enableElement(r)
                }), !1
            }
            return r.data("method") ? (n.handleMethod(r), !1) : void 0
        }), i.delegate(n.buttonClickSelector, "click.rails", function (e) {
            var i = t(this);
            if (!n.allowAction(i)) return n.stopEverything(e);
            i.is(n.buttonDisableSelector) && n.disableFormElement(i);
            var r = n.handleRemote(i);
            return r === !1 ? n.enableFormElement(i) : r.error(function () {
                n.enableFormElement(i)
            }), !1
        }), i.delegate(n.inputChangeSelector, "change.rails", function (e) {
            var i = t(this);
            return n.allowAction(i) ? (n.handleRemote(i), !1) : n.stopEverything(e)
        }), i.delegate(n.formSubmitSelector, "submit.rails", function (i) {
            var r, o, a = t(this),
                s = a.data("remote") !== e;
            if (!n.allowAction(a)) return n.stopEverything(i);
            if (a.attr("novalidate") == e && (r = n.blankInputs(a, n.requiredInputSelector), r && n.fire(a, "ajax:aborted:required", [r]))) return n.stopEverything(i);
            if (s) {
                if (o = n.nonBlankInputs(a, n.fileInputSelector)) {
                    setTimeout(function () {
                        n.disableFormElements(a)
                    }, 13);
                    var l = n.fire(a, "ajax:aborted:file", [o]);
                    return l || setTimeout(function () {
                        n.enableFormElements(a)
                    }, 13), l
                }
                return n.handleRemote(a), !1
            }
            setTimeout(function () {
                n.disableFormElements(a)
            }, 13)
        }), i.delegate(n.formInputClickSelector, "click.rails", function (e) {
            var i = t(this);
            if (!n.allowAction(i)) return n.stopEverything(e);
            var r = i.attr("name"),
                o = r ? {
                    name: r,
                    value: i.val()
                } : null;
            i.closest("form").data("ujs:submit-button", o)
        }), i.delegate(n.formSubmitSelector, "ajax:send.rails", function (e) {
            this == e.target && n.disableFormElements(t(this))
        }), i.delegate(n.formSubmitSelector, "ajax:complete.rails", function (e) {
            this == e.target && n.enableFormElements(t(this))
        }), t(function () {
            n.refreshCSRFTokens()
        }))
    } (jQuery),
    function (t) {
        var e, n, i, r = t.event,
            o = {
                _: 0
            },
            a = 0;
        e = r.special.throttledresize = {
            setup: function () {
                t(this).on("resize", e.handler)
            },
            teardown: function () {
                t(this).off("resize", e.handler)
            },
            handler: function (s, l) {
                var u = this,
                    c = arguments;
                n = !0, i || (setInterval(function () {
                    a++ , (a > e.threshold && n || l) && (s.type = "throttledresize", r.dispatch.apply(u, c), n = !1, a = 0), a > 9 && (t(o).stop(), i = !1, a = 0)
                }, 30), i = !0)
            },
            threshold: 0
        }
    } (jQuery),
    function () {
        var t = this,
            e = t._,
            n = {},
            i = Array.prototype,
            r = Object.prototype,
            o = Function.prototype,
            a = i.push,
            s = i.slice,
            l = i.concat,
            u = r.toString,
            c = r.hasOwnProperty,
            d = i.forEach,
            h = i.map,
            f = i.reduce,
            p = i.reduceRight,
            m = i.filter,
            g = i.every,
            v = i.some,
            y = i.indexOf,
            _ = i.lastIndexOf,
            w = Array.isArray,
            b = Object.keys,
            x = o.bind,
            k = function (t) {
                return t instanceof k ? t : this instanceof k ? void (this._wrapped = t) : new k(t)
            };
        "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = k), exports._ = k) : t._ = k, k.VERSION = "1.4.4";
        var C = k.each = k.forEach = function (t, e, i) {
            if (null != t)
                if (d && t.forEach === d) t.forEach(e, i);
                else if (t.length === +t.length) {
                    for (var r = 0, o = t.length; o > r; r++)
                        if (e.call(i, t[r], r, t) === n) return
                } else
                    for (var a in t)
                        if (k.has(t, a) && e.call(i, t[a], a, t) === n) return
        };
        k.map = k.collect = function (t, e, n) {
            var i = [];
            return null == t ? i : h && t.map === h ? t.map(e, n) : (C(t, function (t, r, o) {
                i[i.length] = e.call(n, t, r, o)
            }), i)
        };
        var S = "Reduce of empty array with no initial value";
        k.reduce = k.foldl = k.inject = function (t, e, n, i) {
            var r = arguments.length > 2;
            if (null == t && (t = []), f && t.reduce === f) return i && (e = k.bind(e, i)), r ? t.reduce(e, n) : t.reduce(e);
            if (C(t, function (t, o, a) {
                r ? n = e.call(i, n, t, o, a) : (n = t, r = !0)
            }), !r) throw new TypeError(S);
            return n
        }, k.reduceRight = k.foldr = function (t, e, n, i) {
            var r = arguments.length > 2;
            if (null == t && (t = []), p && t.reduceRight === p) return i && (e = k.bind(e, i)), r ? t.reduceRight(e, n) : t.reduceRight(e);
            var o = t.length;
            if (o !== +o) {
                var a = k.keys(t);
                o = a.length
            }
            if (C(t, function (s, l, u) {
                l = a ? a[--o] : --o, r ? n = e.call(i, n, t[l], l, u) : (n = t[l], r = !0)
            }), !r) throw new TypeError(S);
            return n
        }, k.find = k.detect = function (t, e, n) {
            var i;
            return T(t, function (t, r, o) {
                return e.call(n, t, r, o) ? (i = t, !0) : void 0
            }), i
        }, k.filter = k.select = function (t, e, n) {
            var i = [];
            return null == t ? i : m && t.filter === m ? t.filter(e, n) : (C(t, function (t, r, o) {
                e.call(n, t, r, o) && (i[i.length] = t)
            }), i)
        }, k.reject = function (t, e, n) {
            return k.filter(t, function (t, i, r) {
                return !e.call(n, t, i, r)
            }, n)
        }, k.every = k.all = function (t, e, i) {
            e || (e = k.identity);
            var r = !0;
            return null == t ? r : g && t.every === g ? t.every(e, i) : (C(t, function (t, o, a) {
                return (r = r && e.call(i, t, o, a)) ? void 0 : n
            }), !!r)
        };
        var T = k.some = k.any = function (t, e, i) {
            e || (e = k.identity);
            var r = !1;
            return null == t ? r : v && t.some === v ? t.some(e, i) : (C(t, function (t, o, a) {
                return r || (r = e.call(i, t, o, a)) ? n : void 0
            }), !!r)
        };
        k.contains = k.include = function (t, e) {
            return null == t ? !1 : y && t.indexOf === y ? -1 != t.indexOf(e) : T(t, function (t) {
                return t === e
            })
        }, k.invoke = function (t, e) {
            var n = s.call(arguments, 2),
                i = k.isFunction(e);
            return k.map(t, function (t) {
                return (i ? e : t[e]).apply(t, n)
            })
        }, k.pluck = function (t, e) {
            return k.map(t, function (t) {
                return t[e]
            })
        }, k.where = function (t, e, n) {
            return k.isEmpty(e) ? n ? null : [] : k[n ? "find" : "filter"](t, function (t) {
                for (var n in e)
                    if (e[n] !== t[n]) return !1;
                return !0
            })
        }, k.findWhere = function (t, e) {
            return k.where(t, e, !0)
        }, k.max = function (t, e, n) {
            if (!e && k.isArray(t) && t[0] === +t[0] && t.length < 65535) return Math.max.apply(Math, t);
            if (!e && k.isEmpty(t)) return -(1 / 0);
            var i = {
                computed: -(1 / 0),
                value: -(1 / 0)
            };
            return C(t, function (t, r, o) {
                var a = e ? e.call(n, t, r, o) : t;
                a >= i.computed && (i = {
                    value: t,
                    computed: a
                })
            }), i.value
        }, k.min = function (t, e, n) {
            if (!e && k.isArray(t) && t[0] === +t[0] && t.length < 65535) return Math.min.apply(Math, t);
            if (!e && k.isEmpty(t)) return 1 / 0;
            var i = {
                computed: 1 / 0,
                value: 1 / 0
            };
            return C(t, function (t, r, o) {
                var a = e ? e.call(n, t, r, o) : t;
                a < i.computed && (i = {
                    value: t,
                    computed: a
                })
            }), i.value
        }, k.shuffle = function (t) {
            var e, n = 0,
                i = [];
            return C(t, function (t) {
                e = k.random(n++), i[n - 1] = i[e], i[e] = t
            }), i
        };
        var D = function (t) {
            return k.isFunction(t) ? t : function (e) {
                return e[t]
            }
        };
        k.sortBy = function (t, e, n) {
            var i = D(e);
            return k.pluck(k.map(t, function (t, e, r) {
                return {
                    value: t,
                    index: e,
                    criteria: i.call(n, t, e, r)
                }
            }).sort(function (t, e) {
                var n = t.criteria,
                    i = e.criteria;
                if (n !== i) {
                    if (n > i || void 0 === n) return 1;
                    if (i > n || void 0 === i) return -1
                }
                return t.index < e.index ? -1 : 1
            }), "value")
        };
        var $ = function (t, e, n, i) {
            var r = {},
                o = D(e || k.identity);
            return C(t, function (e, a) {
                var s = o.call(n, e, a, t);
                i(r, s, e)
            }), r
        };
        k.groupBy = function (t, e, n) {
            return $(t, e, n, function (t, e, n) {
                (k.has(t, e) ? t[e] : t[e] = []).push(n)
            })
        }, k.countBy = function (t, e, n) {
            return $(t, e, n, function (t, e) {
                k.has(t, e) || (t[e] = 0), t[e]++
            })
        }, k.sortedIndex = function (t, e, n, i) {
            n = null == n ? k.identity : D(n);
            for (var r = n.call(i, e), o = 0, a = t.length; a > o;) {
                var s = o + a >>> 1;
                n.call(i, t[s]) < r ? o = s + 1 : a = s
            }
            return o
        }, k.toArray = function (t) {
            return t ? k.isArray(t) ? s.call(t) : t.length === +t.length ? k.map(t, k.identity) : k.values(t) : []
        }, k.size = function (t) {
            return null == t ? 0 : t.length === +t.length ? t.length : k.keys(t).length
        }, k.first = k.head = k.take = function (t, e, n) {
            return null == t ? void 0 : null == e || n ? t[0] : s.call(t, 0, e)
        }, k.initial = function (t, e, n) {
            return s.call(t, 0, t.length - (null == e || n ? 1 : e))
        }, k.last = function (t, e, n) {
            return null == t ? void 0 : null == e || n ? t[t.length - 1] : s.call(t, Math.max(t.length - e, 0))
        }, k.rest = k.tail = k.drop = function (t, e, n) {
            return s.call(t, null == e || n ? 1 : e)
        }, k.compact = function (t) {
            return k.filter(t, k.identity)
        };
        var E = function (t, e, n) {
            return C(t, function (t) {
                k.isArray(t) ? e ? a.apply(n, t) : E(t, e, n) : n.push(t)
            }), n
        };
        k.flatten = function (t, e) {
            return E(t, e, [])
        }, k.without = function (t) {
            return k.difference(t, s.call(arguments, 1))
        }, k.uniq = k.unique = function (t, e, n, i) {
            k.isFunction(e) && (i = n, n = e, e = !1);
            var r = n ? k.map(t, n, i) : t,
                o = [],
                a = [];
            return C(r, function (n, i) {
                (e ? i && a[a.length - 1] === n : k.contains(a, n)) || (a.push(n), o.push(t[i]))
            }), o
        }, k.union = function () {
            return k.uniq(l.apply(i, arguments))
        }, k.intersection = function (t) {
            var e = s.call(arguments, 1);
            return k.filter(k.uniq(t), function (t) {
                return k.every(e, function (e) {
                    return k.indexOf(e, t) >= 0
                })
            })
        }, k.difference = function (t) {
            var e = l.apply(i, s.call(arguments, 1));
            return k.filter(t, function (t) {
                return !k.contains(e, t)
            })
        }, k.zip = function () {
            for (var t = s.call(arguments), e = k.max(k.pluck(t, "length")), n = new Array(e), i = 0; e > i; i++) n[i] = k.pluck(t, "" + i);
            return n
        }, k.object = function (t, e) {
            if (null == t) return {};
            for (var n = {}, i = 0, r = t.length; r > i; i++) e ? n[t[i]] = e[i] : n[t[i][0]] = t[i][1];
            return n
        }, k.indexOf = function (t, e, n) {
            if (null == t) return -1;
            var i = 0,
                r = t.length;
            if (n) {
                if ("number" != typeof n) return i = k.sortedIndex(t, e), t[i] === e ? i : -1;
                i = 0 > n ? Math.max(0, r + n) : n
            }
            if (y && t.indexOf === y) return t.indexOf(e, n);
            for (; r > i; i++)
                if (t[i] === e) return i;
            return -1
        }, k.lastIndexOf = function (t, e, n) {
            if (null == t) return -1;
            var i = null != n;
            if (_ && t.lastIndexOf === _) return i ? t.lastIndexOf(e, n) : t.lastIndexOf(e);
            for (var r = i ? n : t.length; r--;)
                if (t[r] === e) return r;
            return -1
        }, k.range = function (t, e, n) {
            arguments.length <= 1 && (e = t || 0, t = 0), n = arguments[2] || 1;
            for (var i = Math.max(Math.ceil((e - t) / n), 0), r = 0, o = new Array(i); i > r;) o[r++] = t, t += n;
            return o
        }, k.bind = function (t, e) {
            if (t.bind === x && x) return x.apply(t, s.call(arguments, 1));
            var n = s.call(arguments, 2);
            return function () {
                return t.apply(e, n.concat(s.call(arguments)))
            }
        }, k.partial = function (t) {
            var e = s.call(arguments, 1);
            return function () {
                return t.apply(this, e.concat(s.call(arguments)))
            }
        }, k.bindAll = function (t) {
            var e = s.call(arguments, 1);
            return 0 === e.length && (e = k.functions(t)), C(e, function (e) {
                t[e] = k.bind(t[e], t)
            }), t
        }, k.memoize = function (t, e) {
            var n = {};
            return e || (e = k.identity),
                function () {
                    var i = e.apply(this, arguments);
                    return k.has(n, i) ? n[i] : n[i] = t.apply(this, arguments)
                }
        }, k.delay = function (t, e) {
            var n = s.call(arguments, 2);
            return setTimeout(function () {
                return t.apply(null, n)
            }, e)
        }, k.defer = function (t) {
            return k.delay.apply(k, [t, 1].concat(s.call(arguments, 1)))
        }, k.throttle = function (t, e) {
            var n, i, r, o, a = 0,
                s = function () {
                    a = new Date, r = null, o = t.apply(n, i)
                };
            return function () {
                var l = new Date,
                    u = e - (l - a);
                return n = this, i = arguments, 0 >= u ? (clearTimeout(r), r = null, a = l, o = t.apply(n, i)) : r || (r = setTimeout(s, u)), o
            }
        }, k.debounce = function (t, e, n) {
            var i, r;
            return function () {
                var o = this,
                    a = arguments,
                    s = function () {
                        i = null, n || (r = t.apply(o, a))
                    },
                    l = n && !i;
                return clearTimeout(i), i = setTimeout(s, e), l && (r = t.apply(o, a)), r
            }
        }, k.once = function (t) {
            var e, n = !1;
            return function () {
                return n ? e : (n = !0, e = t.apply(this, arguments), t = null, e)
            }
        }, k.wrap = function (t, e) {
            return function () {
                var n = [t];
                return a.apply(n, arguments), e.apply(this, n)
            }
        }, k.compose = function () {
            var t = arguments;
            return function () {
                for (var e = arguments, n = t.length - 1; n >= 0; n--) e = [t[n].apply(this, e)];
                return e[0]
            }
        }, k.after = function (t, e) {
            return 0 >= t ? e() : function () {
                return --t < 1 ? e.apply(this, arguments) : void 0
            }
        }, k.keys = b || function (t) {
            if (t !== Object(t)) throw new TypeError("Invalid object");
            var e = [];
            for (var n in t) k.has(t, n) && (e[e.length] = n);
            return e
        }, k.values = function (t) {
            var e = [];
            for (var n in t) k.has(t, n) && e.push(t[n]);
            return e
        }, k.pairs = function (t) {
            var e = [];
            for (var n in t) k.has(t, n) && e.push([n, t[n]]);
            return e
        }, k.invert = function (t) {
            var e = {};
            for (var n in t) k.has(t, n) && (e[t[n]] = n);
            return e
        }, k.functions = k.methods = function (t) {
            var e = [];
            for (var n in t) k.isFunction(t[n]) && e.push(n);
            return e.sort()
        }, k.extend = function (t) {
            return C(s.call(arguments, 1), function (e) {
                if (e)
                    for (var n in e) t[n] = e[n]
            }), t
        }, k.pick = function (t) {
            var e = {},
                n = l.apply(i, s.call(arguments, 1));
            return C(n, function (n) {
                n in t && (e[n] = t[n])
            }), e
        }, k.omit = function (t) {
            var e = {},
                n = l.apply(i, s.call(arguments, 1));
            for (var r in t) k.contains(n, r) || (e[r] = t[r]);
            return e
        }, k.defaults = function (t) {
            return C(s.call(arguments, 1), function (e) {
                if (e)
                    for (var n in e) null == t[n] && (t[n] = e[n])
            }), t
        }, k.clone = function (t) {
            return k.isObject(t) ? k.isArray(t) ? t.slice() : k.extend({}, t) : t
        }, k.tap = function (t, e) {
            return e(t), t
        };
        var M = function (t, e, n, i) {
            if (t === e) return 0 !== t || 1 / t == 1 / e;
            if (null == t || null == e) return t === e;
            t instanceof k && (t = t._wrapped), e instanceof k && (e = e._wrapped);
            var r = u.call(t);
            if (r != u.call(e)) return !1;
            switch (r) {
                case "[object String]":
                    return t == String(e);
                case "[object Number]":
                    return t != +t ? e != +e : 0 == t ? 1 / t == 1 / e : t == +e;
                case "[object Date]":
                case "[object Boolean]":
                    return +t == +e;
                case "[object RegExp]":
                    return t.source == e.source && t.global == e.global && t.multiline == e.multiline && t.ignoreCase == e.ignoreCase
            }
            if ("object" != typeof t || "object" != typeof e) return !1;
            for (var o = n.length; o--;)
                if (n[o] == t) return i[o] == e;
            n.push(t), i.push(e);
            var a = 0,
                s = !0;
            if ("[object Array]" == r) {
                if (a = t.length, s = a == e.length)
                    for (; a-- && (s = M(t[a], e[a], n, i)););
            } else {
                var l = t.constructor,
                    c = e.constructor;
                if (l !== c && !(k.isFunction(l) && l instanceof l && k.isFunction(c) && c instanceof c)) return !1;
                for (var d in t)
                    if (k.has(t, d) && (a++ , !(s = k.has(e, d) && M(t[d], e[d], n, i)))) break;
                if (s) {
                    for (d in e)
                        if (k.has(e, d) && !a--) break;
                    s = !a
                }
            }
            return n.pop(), i.pop(), s
        };
        k.isEqual = function (t, e) {
            return M(t, e, [], [])
        }, k.isEmpty = function (t) {
            if (null == t) return !0;
            if (k.isArray(t) || k.isString(t)) return 0 === t.length;
            for (var e in t)
                if (k.has(t, e)) return !1;
            return !0
        }, k.isElement = function (t) {
            return !(!t || 1 !== t.nodeType)
        }, k.isArray = w || function (t) {
            return "[object Array]" == u.call(t)
        }, k.isObject = function (t) {
            return t === Object(t)
        }, C(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function (t) {
            k["is" + t] = function (e) {
                return u.call(e) == "[object " + t + "]"
            }
        }), k.isArguments(arguments) || (k.isArguments = function (t) {
            return !(!t || !k.has(t, "callee"))
        }), "function" != typeof /./ && (k.isFunction = function (t) {
            return "function" == typeof t
        }), k.isFinite = function (t) {
            return isFinite(t) && !isNaN(parseFloat(t))
        }, k.isNaN = function (t) {
            return k.isNumber(t) && t != +t
        }, k.isBoolean = function (t) {
            return t === !0 || t === !1 || "[object Boolean]" == u.call(t)
        }, k.isNull = function (t) {
            return null === t
        }, k.isUndefined = function (t) {
            return void 0 === t
        }, k.has = function (t, e) {
            return c.call(t, e)
        }, k.noConflict = function () {
            return t._ = e, this
        }, k.identity = function (t) {
            return t
        }, k.times = function (t, e, n) {
            for (var i = Array(t), r = 0; t > r; r++) i[r] = e.call(n, r);
            return i
        }, k.random = function (t, e) {
            return null == e && (e = t, t = 0), t + Math.floor(Math.random() * (e - t + 1))
        };
        var A = {
            escape: {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#x27;",
                "/": "&#x2F;"
            }
        };
        A.unescape = k.invert(A.escape);
        var O = {
            escape: new RegExp("[" + k.keys(A.escape).join("") + "]", "g"),
            unescape: new RegExp("(" + k.keys(A.unescape).join("|") + ")", "g")
        };
        k.each(["escape", "unescape"], function (t) {
            k[t] = function (e) {
                return null == e ? "" : ("" + e).replace(O[t], function (e) {
                    return A[t][e]
                })
            }
        }), k.result = function (t, e) {
            if (null == t) return null;
            var n = t[e];
            return k.isFunction(n) ? n.call(t) : n
        }, k.mixin = function (t) {
            C(k.functions(t), function (e) {
                var n = k[e] = t[e];
                k.prototype[e] = function () {
                    var t = [this._wrapped];
                    return a.apply(t, arguments), j.call(this, n.apply(k, t))
                }
            })
        };
        var H = 0;
        k.uniqueId = function (t) {
            var e = ++H + "";
            return t ? t + e : e
        }, k.templateSettings = {
            evaluate: /<%([\s\S]+?)%>/g,
            interpolate: /<%=([\s\S]+?)%>/g,
            escape: /<%-([\s\S]+?)%>/g
        };
        var N = /(.)^/,
            P = {
                "'": "'",
                "\\": "\\",
                "\r": "r",
                "\n": "n",
                "	": "t",
                "\u2028": "u2028",
                "\u2029": "u2029"
            },
            F = /\\|'|\r|\n|\t|\u2028|\u2029/g;
        k.template = function (t, e, n) {
            var i;
            n = k.defaults({}, n, k.templateSettings);
            var r = new RegExp([(n.escape || N).source, (n.interpolate || N).source, (n.evaluate || N).source].join("|") + "|$", "g"),
                o = 0,
                a = "__p+='";
            t.replace(r, function (e, n, i, r, s) {
                return a += t.slice(o, s).replace(F, function (t) {
                    return "\\" + P[t]
                }), n && (a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'"), i && (a += "'+\n((__t=(" + i + "))==null?'':__t)+\n'"), r && (a += "';\n" + r + "\n__p+='"), o = s + e.length, e
            }), a += "';\n", n.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
            try {
                i = new Function(n.variable || "obj", "_", a)
            } catch (s) {
                throw s.source = a, s
            }
            if (e) return i(e, k);
            var l = function (t) {
                return i.call(this, t, k)
            };
            return l.source = "function(" + (n.variable || "obj") + "){\n" + a + "}", l
        }, k.chain = function (t) {
            return k(t).chain()
        };
        var j = function (t) {
            return this._chain ? k(t).chain() : t
        };
        k.mixin(k), C(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (t) {
            var e = i[t];
            k.prototype[t] = function () {
                var n = this._wrapped;
                return e.apply(n, arguments), "shift" != t && "splice" != t || 0 !== n.length || delete n[0], j.call(this, n)
            }
        }), C(["concat", "join", "slice"], function (t) {
            var e = i[t];
            k.prototype[t] = function () {
                return j.call(this, e.apply(this._wrapped, arguments))
            }
        }), k.extend(k.prototype, {
            chain: function () {
                return this._chain = !0, this
            },
            value: function () {
                return this._wrapped
            }
        })
    }.call(this), window.matchMedia = window.matchMedia || function (t, e) {
        "use strict";
        var n, i = t.documentElement,
            r = i.firstElementChild || i.firstChild,
            o = t.createElement("body"),
            a = t.createElement("div");
        return a.id = "mq-test-1", a.style.cssText = "position:absolute;top:-100em", o.style.background = "none", o.appendChild(a),
            function (t) {
                return a.innerHTML = '&shy;<style media="' + t + '"> #mq-test-1 { width: 42px; }</style>', i.insertBefore(o, r), n = 42 === a.offsetWidth, i.removeChild(o), {
                    matches: n,
                    media: t
                }
            }
    } (document),
    function (t) {
        "use strict";

        function e(t, e, n) {
            return t.addEventListener ? t.addEventListener(e, n, !1) : t.attachEvent ? t.attachEvent("on" + e, n) : void 0
        }

        function n(t, e) {
            var n, i;
            for (n = 0, i = t.length; i > n; n++)
                if (t[n] === e) return !0;
            return !1
        }

        function i(t, e) {
            var n;
            t.createTextRange ? (n = t.createTextRange(), n.move("character", e), n.select()) : t.selectionStart && (t.focus(), t.setSelectionRange(e, e))
        }

        function r(t, e) {
            try {
                return t.type = e, !0
            } catch (n) {
                return !1
            }
        }
        t.Placeholders = {
            Utils: {
                addEventListener: e,
                inArray: n,
                moveCaret: i,
                changeType: r
            }
        }
    } (this),
    function (t) {
        "use strict";

        function e() { }

        function n(t) {
            var e;
            return t.value === t.getAttribute(H) && "true" === t.getAttribute(N) ? (t.setAttribute(N, "false"), t.value = "", t.className = t.className.replace(O, ""), e = t.getAttribute(P), e && (t.type = e), !0) : !1
        }

        function i(t) {
            var e, n = t.getAttribute(H);
            return "" === t.value && n ? (t.setAttribute(N, "true"), t.value = n, t.className += " " + A, e = t.getAttribute(P), e ? t.type = "text" : "password" === t.type && q.changeType(t, "text") && t.setAttribute(P, "password"), !0) : !1
        }

        function r(t, e) {
            var n, i, r, o, a;
            if (t && t.getAttribute(H)) e(t);
            else
                for (n = t ? t.getElementsByTagName("input") : p, i = t ? t.getElementsByTagName("textarea") : m, a = 0, o = n.length + i.length; o > a; a++) r = a < n.length ? n[a] : i[a - n.length], e(r)
        }

        function o(t) {
            r(t, n)
        }

        function a(t) {
            r(t, i)
        }

        function s(t) {
            return function () {
                g && t.value === t.getAttribute(H) && "true" === t.getAttribute(N) ? q.moveCaret(t, 0) : n(t)
            }
        }

        function l(t) {
            return function () {
                i(t)
            }
        }

        function u(t) {
            return function (e) {
                return y = t.value, "true" === t.getAttribute(N) && y === t.getAttribute(H) && q.inArray(E, e.keyCode) ? (e.preventDefault && e.preventDefault(), !1) : void 0
            }
        }

        function c(t) {
            return function () {
                var e;
                "true" === t.getAttribute(N) && t.value !== y && (t.className = t.className.replace(O, ""), t.value = t.value.replace(t.getAttribute(H), ""), t.setAttribute(N, !1), e = t.getAttribute(P), e && (t.type = e)), "" === t.value && (t.blur(), q.moveCaret(t, 0))
            }
        }

        function d(t) {
            return function () {
                t === document.activeElement && t.value === t.getAttribute(H) && "true" === t.getAttribute(N) && q.moveCaret(t, 0)
            }
        }

        function h(t) {
            return function () {
                o(t)
            }
        }

        function f(t) {
            return $(t).closest("template").length ? !1 : (t.form && (k = t.form, k.getAttribute(F) || (q.addEventListener(k, "submit", h(k)), k.setAttribute(F, "true"))), q.addEventListener(t, "focus", s(t)), q.addEventListener(t, "blur", l(t)), g && (q.addEventListener(t, "keydown", u(t)), q.addEventListener(t, "keyup", c(t)), q.addEventListener(t, "click", d(t))), t.setAttribute(j, "true"), t.setAttribute(H, b), void i(t))
        }
        var p, m, g, v, y, _, w, b, x, k, C, S, T, D = ["text", "search", "url", "tel", "email", "password", "number", "textarea"],
            E = [27, 33, 34, 35, 36, 37, 38, 39, 40, 8, 46],
            M = "#ccc",
            A = "placeholdersjs",
            O = new RegExp("(?:^|\\s)" + A + "(?!\\S)"),
            H = "data-placeholder-value",
            N = "data-placeholder-active",
            P = "data-placeholder-type",
            F = "data-placeholder-submit",
            j = "data-placeholder-bound",
            L = "data-placeholder-focus",
            I = "data-placeholder-live",
            Y = document.createElement("input"),
            R = document.getElementsByTagName("head")[0],
            W = document.documentElement,
            z = t.Placeholders,
            q = z.Utils;
        if (z.nativeSupport = void 0 !== Y.placeholder, !z.nativeSupport) {
            for (p = document.getElementsByTagName("input"), m = document.getElementsByTagName("textarea"), g = "false" === W.getAttribute(L), v = "false" !== W.getAttribute(I), _ = document.createElement("style"), _.type = "text/css", w = document.createTextNode("." + A + " { color:" + M + "; }"), _.styleSheet ? _.styleSheet.cssText = w.nodeValue : _.appendChild(w), R.insertBefore(_, R.firstChild), T = 0, S = p.length + m.length; S > T; T++) C = T < p.length ? p[T] : m[T - p.length], b = C.attributes.placeholder, b && (b = b.nodeValue, b && q.inArray(D, C.type) && f(C));
            x = setInterval(function () {
                for (T = 0, S = p.length + m.length; S > T; T++) C = T < p.length ? p[T] : m[T - p.length], b = C.attributes.placeholder, b && (b = b.nodeValue, b && q.inArray(D, C.type) && (C.getAttribute(j) || f(C), (b !== C.getAttribute(H) || "password" === C.type && !C.getAttribute(P)) && ("password" === C.type && !C.getAttribute(P) && q.changeType(C, "text") && C.setAttribute(P, "password"), C.value === C.getAttribute(H) && (C.value = b), C.setAttribute(H, b))));
                v || clearInterval(x)
            }, 100)
        }
        z.disable = z.nativeSupport ? e : o, z.enable = z.nativeSupport ? e : a
    } (this),

    function (t) {
        "object" == typeof module && "object" == typeof module.exports ? t(require("jquery")) : "function" == typeof define && define.amd ? define([], t(window.jQuery)) : t(window.jQuery)
    } (function (t) {
        return t ? (t.Unslider = function (e, n) {
            var i = this;
            return i._ = "unslider", i.defaults = {
                autoplay: !1,
                delay: 3e3,
                speed: 750,
                easing: "swing",
                keys: {
                    prev: 37,
                    next: 39
                },
                nav: !0,
                arrows: {
                    prev: '<a class="' + i._ + '-arrow prev">Prev</a>',
                    next: '<a class="' + i._ + '-arrow next">Next</a>'
                },
                animation: "horizontal",
                selectors: {
                    container: "ul:first",
                    slides: "li"
                },
                animateHeight: !1,
                activeClass: i._ + "-active",
                swipe: !0,
                swipeThreshold: .2
            }, i.$context = e, i.options = {}, i.$parent = null, i.$container = null, i.$slides = null, i.$nav = null, i.$arrows = [], i.total = 0, i.current = 0, i.prefix = i._ + "-", i.eventSuffix = "." + i.prefix + ~~(2e3 * Math.random()), i.interval = null, i.init = function (e) {
                return i.options = t.extend({}, i.defaults, e), i.$container = i.$context.find(i.options.selectors.container).addClass(i.prefix + "wrap"), i.$slides = i.$container.children(i.options.selectors.slides), i.setup(), t.each(["nav", "arrows", "keys", "infinite"], function (e, n) {
                    i.options[n] && i["init" + t._ucfirst(n)]()
                }), jQuery.event.special.swipe && i.options.swipe && i.initSwipe(), i.options.autoplay && i.start(), i.calculateSlides(), i.$context.trigger(i._ + ".ready"), i.animate(i.options.index || i.current, "init")
            }, i.setup = function () {
                i.$context.addClass(i.prefix + i.options.animation).wrap('<div class="' + i._ + '" />'), i.$parent = i.$context.parent("." + i._);
                var t = i.$context.css("position");
                "static" === t && i.$context.css("position", "relative"), i.$context.css("overflow", "hidden")
            }, i.calculateSlides = function () {
                if (i.total = i.$slides.length, "fade" !== i.options.animation) {
                    var t = "width";
                    "vertical" === i.options.animation && (t = "height"), i.$container.css(t, 100 * i.total + "%").addClass(i.prefix + "carousel"), i.$slides.css(t, 100 / i.total + "%")
                }
            }, i.start = function () {
                return i.interval = setTimeout(function () {
                    i.next()
                }, i.options.delay), i
            }, i.stop = function () {
                return clearTimeout(i.interval), i
            }, i.initNav = function () {
                var e = t('<nav class="' + i.prefix + 'nav"><ol /></nav>');
                i.$slides.each(function (n) {
                    var r = this.getAttribute("data-nav") || n + 1;
                    t.isFunction(i.options.nav) && (r = i.options.nav.call(i.$slides.eq(n), n, r)), e.children("ol").append('<li data-slide="' + n + '">' + r + "</li>")
                }), i.$nav = e.insertAfter(i.$context), i.$nav.find("li").on("click" + i.eventSuffix, function () {
                    var e = t(this).addClass(i.options.activeClass);
                    e.siblings().removeClass(i.options.activeClass), i.animate(e.attr("data-slide"))
                })
            }, i.initArrows = function () {
                i.options.arrows === !0 && (i.options.arrows = i.defaults.arrows), t.each(i.options.arrows, function (e, n) {
                    i.$arrows.push(t(n).insertAfter(i.$context).on("click" + i.eventSuffix, i[e]))
                })
            }, i.initKeys = function () {
                i.options.keys === !0 && (i.options.keys = i.defaults.keys), t(document).on("keyup" + i.eventSuffix, function (e) {
                    t.each(i.options.keys, function (n, r) {
                        e.which === r && t.isFunction(i[n]) && i[n].call(i)
                    })
                })
            }, i.initSwipe = function () {
                var t = i.$slides.width();
                "fade" !== i.options.animation && i.$container.on({
                    movestart: function (t) {
                        return t.distX > t.distY && t.distX < -t.distY || t.distX < t.distY && t.distX > -t.distY ? !!t.preventDefault() : void i.$container.css("position", "relative")
                    },
                    move: function (e) {
                        i.$container.css("left", -(100 * i.current) + 100 * e.distX / t + "%")
                    },
                    moveend: function (e) {
                        Math.abs(e.distX) / t > i.options.swipeThreshold ? i[e.distX < 0 ? "next" : "prev"]() : i.$container.animate({
                            left: -(100 * i.current) + "%"
                        }, i.options.speed / 2)
                    }
                })
            }, i.initInfinite = function () {
                var e = ["first", "last"];
                t.each(e, function (t, n) {
                    i.$slides.push.apply(i.$slides, i.$slides.filter(':not(".' + i._ + '-clone")')[n]().clone().addClass(i._ + "-clone")["insert" + (0 === t ? "After" : "Before")](i.$slides[e[~~!t]]()))
                })
            }, i.destroyArrows = function () {
                t.each(i.$arrows, function (t, e) {
                    e.remove()
                })
            }, i.destroySwipe = function () {
                i.$container.off("movestart move moveend")
            }, i.destroyKeys = function () {
                t(document).off("keyup" + i.eventSuffix)
            }, i.setIndex = function (t) {
                return 0 > t && (t = i.total - 1), i.current = Math.min(Math.max(0, t), i.total - 1), i.options.nav && i.$nav.find('[data-slide="' + i.current + '"]')._active(i.options.activeClass), i.$slides.eq(i.current)._active(i.options.activeClass), i
            }, i.animate = function (e, n) {
                if ("first" === e && (e = 0), "last" === e && (e = i.total), isNaN(e)) return i;
                i.options.autoplay && i.stop().start(), i.setIndex(e), i.$context.trigger(i._ + ".change", [e, i.$slides.eq(e)]);
                var r = "animate" + t._ucfirst(i.options.animation);
                return t.isFunction(i[r]) && i[r](i.current, n), i
            }, i.next = function () {
                var t = i.current + 1;
                return t >= i.total && (t = 0), i.animate(t, "next")
            }, i.prev = function () {
                return i.animate(i.current - 1, "prev")
            }, i.animateHorizontal = function (t) {
                var e = "left";
                return "rtl" === i.$context.attr("dir") && (e = "right"), i.options.infinite && i.$container.css("margin-" + e, "-100%"), i.slide(e, t)
            }, i.animateVertical = function (t) {
                return i.options.animateHeight = !0, i.options.infinite && i.$container.css("margin-top", -i.$slides.outerHeight()), i.slide("top", t)
            }, i.slide = function (t, e) {
                if (i.animateHeight(e), i.options.infinite) {
                    var n;
                    e === i.total - 1 && (n = i.total - 3, e = -1), e === i.total - 2 && (n = 0, e = i.total - 2), "number" == typeof n && (i.setIndex(n), i.$context.on(i._ + ".moved", function () {
                        i.current === n && i.$container.css(t, -(100 * n) + "%").off(i._ + ".moved")
                    }))
                }
                var r = {};
                return r[t] = -(100 * e) + "%", i._move(i.$container, r)
            }, i.animateFade = function (t) {
                i.animateHeight(t);
                var e = i.$slides.eq(t).addClass(i.options.activeClass);
                i._move(e.siblings().removeClass(i.options.activeClass), {
                    opacity: 0
                }), i._move(e, {
                    opacity: 1
                }, !1)
            }, i.animateHeight = function (t) {
                i.options.animateHeight && i._move(i.$context, {
                    height: i.$slides.eq(t).outerHeight()
                }, !1)
            }, i._move = function (t, e, n, r) {
                return n !== !1 && (n = function () {
                    i.$context.trigger(i._ + ".moved")
                }), t._move(e, r || i.options.speed, i.options.easing, n)
            }, i.init(n)
        }, t.fn._active = function (t) {
            return this.addClass(t).siblings().removeClass(t)
        }, t._ucfirst = function (t) {
            return (t + "").toLowerCase().replace(/^./, function (t) {
                return t.toUpperCase()
            })
        }, t.fn._move = function () {
            return this.stop(!0, !0), t.fn[t.fn.velocity ? "velocity" : "animate"].apply(this, arguments)
        }, void (t.fn.unslider = function (e) {
            return this.each(function () {
                var n = t(this);
                if ("string" == typeof e && n.data("unslider")) {
                    e = e.split(":");
                    var i = n.data("unslider")[e[0]];
                    if (t.isFunction(i)) return i.apply(n, e[1] ? e[1].split(",") : null);
                }
                return n.data("unslider", new t.Unslider(n, e))
            })
        })) : console.warn("Unslider needs jQuery")
    }),
    function () {
        null == this.Heroku && (this.Heroku = {}), null == Heroku.Templates && (Heroku.Templates = {})
    }.call(this),
    function (t) {
        var e = t.fn.ready;
        t.fn.ready = function (n) {
            e(void 0 === this.context ? n : this.selector ? t.proxy(function () {
                t(this.selector, this.context).each(n)
            }, this) : t.proxy(function () {
                t(this).each(n)
            }, this))
        }
    } (jQuery),
    function (t, e) {
        function n(e, n) {
            var r, o, a, s = e.nodeName.toLowerCase();
            return "area" === s ? (r = e.parentNode, o = r.name, e.href && o && "map" === r.nodeName.toLowerCase() ? (a = t("img[usemap=#" + o + "]")[0], !!a && i(a)) : !1) : (/input|select|textarea|button|object/.test(s) ? !e.disabled : "a" === s ? e.href || n : n) && i(e)
        }

        function i(e) {
            return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function () {
                return "hidden" === t.css(this, "visibility")
            }).length
        }
        var r = 0,
            o = /^ui-id-\d+$/;
        t.ui = t.ui || {}, t.extend(t.ui, {
            version: "1.10.3",
            keyCode: {
                BACKSPACE: 8,
                COMMA: 188,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                LEFT: 37,
                NUMPAD_ADD: 107,
                NUMPAD_DECIMAL: 110,
                NUMPAD_DIVIDE: 111,
                NUMPAD_ENTER: 108,
                NUMPAD_MULTIPLY: 106,
                NUMPAD_SUBTRACT: 109,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SPACE: 32,
                TAB: 9,
                UP: 38
            }
        }), t.fn.extend({
            focus: function (e) {
                return function (n, i) {
                    return "number" == typeof n ? this.each(function () {
                        var e = this;
                        setTimeout(function () {
                            t(e).focus(), i && i.call(e)
                        }, n)
                    }) : e.apply(this, arguments)
                }
            } (t.fn.focus),
            scrollParent: function () {
                var e;
                return e = t.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
                    return /(relative|absolute|fixed)/.test(t.css(this, "position")) && /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
                }).eq(0) : this.parents().filter(function () {
                    return /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
                }).eq(0), /fixed/.test(this.css("position")) || !e.length ? t(document) : e
            },
            zIndex: function (n) {
                if (n !== e) return this.css("zIndex", n);
                if (this.length)
                    for (var i, r, o = t(this[0]); o.length && o[0] !== document;) {
                        if (i = o.css("position"), ("absolute" === i || "relative" === i || "fixed" === i) && (r = parseInt(o.css("zIndex"), 10), !isNaN(r) && 0 !== r)) return r;
                        o = o.parent()
                    }
                return 0
            },
            uniqueId: function () {
                return this.each(function () {
                    this.id || (this.id = "ui-id-" + ++r)
                })
            },
            removeUniqueId: function () {
                return this.each(function () {
                    o.test(this.id) && t(this).removeAttr("id")
                })
            }
        }), t.extend(t.expr[":"], {
            data: t.expr.createPseudo ? t.expr.createPseudo(function (e) {
                return function (n) {
                    return !!t.data(n, e)
                }
            }) : function (e, n, i) {
                return !!t.data(e, i[3])
            },
            focusable: function (e) {
                return n(e, !isNaN(t.attr(e, "tabindex")))
            },
            tabbable: function (e) {
                var i = t.attr(e, "tabindex"),
                    r = isNaN(i);
                return (r || i >= 0) && n(e, !r)
            }
        }), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function (n, i) {
            function r(e, n, i, r) {
                return t.each(o, function () {
                    n -= parseFloat(t.css(e, "padding" + this)) || 0, i && (n -= parseFloat(t.css(e, "border" + this + "Width")) || 0), r && (n -= parseFloat(t.css(e, "margin" + this)) || 0)
                }), n
            }
            var o = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
                a = i.toLowerCase(),
                s = {
                    innerWidth: t.fn.innerWidth,
                    innerHeight: t.fn.innerHeight,
                    outerWidth: t.fn.outerWidth,
                    outerHeight: t.fn.outerHeight
                };
            t.fn["inner" + i] = function (n) {
                return n === e ? s["inner" + i].call(this) : this.each(function () {
                    t(this).css(a, r(this, n) + "px")
                })
            }, t.fn["outer" + i] = function (e, n) {
                return "number" != typeof e ? s["outer" + i].call(this, e) : this.each(function () {
                    t(this).css(a, r(this, e, !0, n) + "px")
                })
            }
        }), t.fn.addBack || (t.fn.addBack = function (t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function (e) {
            return function (n) {
                return arguments.length ? e.call(this, t.camelCase(n)) : e.call(this)
            }
        } (t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), t.support.selectstart = "onselectstart" in document.createElement("div"), t.fn.extend({
            disableSelection: function () {
                return this.bind((t.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (t) {
                    t.preventDefault()
                })
            },
            enableSelection: function () {
                return this.unbind(".ui-disableSelection")
            }
        }), t.extend(t.ui, {
            plugin: {
                add: function (e, n, i) {
                    var r, o = t.ui[e].prototype;
                    for (r in i) o.plugins[r] = o.plugins[r] || [], o.plugins[r].push([n, i[r]])
                },
                call: function (t, e, n) {
                    var i, r = t.plugins[e];
                    if (r && t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)
                        for (i = 0; i < r.length; i++) t.options[r[i][0]] && r[i][1].apply(t.element, n)
                }
            },
            hasScroll: function (e, n) {
                if ("hidden" === t(e).css("overflow")) return !1;
                var i = n && "left" === n ? "scrollLeft" : "scrollTop",
                    r = !1;
                return e[i] > 0 ? !0 : (e[i] = 1, r = e[i] > 0, e[i] = 0, r)
            }
        })
    } (jQuery),
    function (t, e) {
        var n = 0,
            i = Array.prototype.slice,
            r = t.cleanData;
        t.cleanData = function (e) {
            for (var n, i = 0; null != (n = e[i]); i++) try {
                t(n).triggerHandler("remove")
            } catch (o) { }
            r(e)
        }, t.widget = function (e, n, i) {
            var r, o, a, s, l = {},
                u = e.split(".")[0];
            e = e.split(".")[1], r = u + "-" + e, i || (i = n, n = t.Widget), t.expr[":"][r.toLowerCase()] = function (e) {
                return !!t.data(e, r)
            }, t[u] = t[u] || {}, o = t[u][e], a = t[u][e] = function (t, e) {
                return this._createWidget ? void (arguments.length && this._createWidget(t, e)) : new a(t, e)
            }, t.extend(a, o, {
                version: i.version,
                _proto: t.extend({}, i),
                _childConstructors: []
            }), s = new n, s.options = t.widget.extend({}, s.options), t.each(i, function (e, i) {
                return t.isFunction(i) ? void (l[e] = function () {
                    var t = function () {
                        return n.prototype[e].apply(this, arguments)
                    },
                        r = function (t) {
                            return n.prototype[e].apply(this, t)
                        };
                    return function () {
                        var e, n = this._super,
                            o = this._superApply;
                        return this._super = t, this._superApply = r, e = i.apply(this, arguments), this._super = n, this._superApply = o, e
                    }
                } ()) : void (l[e] = i)
            }), a.prototype = t.widget.extend(s, {
                widgetEventPrefix: o ? s.widgetEventPrefix : e
            }, l, {
                    constructor: a,
                    namespace: u,
                    widgetName: e,
                    widgetFullName: r
                }), o ? (t.each(o._childConstructors, function (e, n) {
                    var i = n.prototype;
                    t.widget(i.namespace + "." + i.widgetName, a, n._proto)
                }), delete o._childConstructors) : n._childConstructors.push(a), t.widget.bridge(e, a)
        }, t.widget.extend = function (n) {
            for (var r, o, a = i.call(arguments, 1), s = 0, l = a.length; l > s; s++)
                for (r in a[s]) o = a[s][r], a[s].hasOwnProperty(r) && o !== e && (t.isPlainObject(o) ? n[r] = t.isPlainObject(n[r]) ? t.widget.extend({}, n[r], o) : t.widget.extend({}, o) : n[r] = o);
            return n
        }, t.widget.bridge = function (n, r) {
            var o = r.prototype.widgetFullName || n;
            t.fn[n] = function (a) {
                var s = "string" == typeof a,
                    l = i.call(arguments, 1),
                    u = this;
                return a = !s && l.length ? t.widget.extend.apply(null, [a].concat(l)) : a, s ? this.each(function () {
                    var i, r = t.data(this, o);
                    return r ? t.isFunction(r[a]) && "_" !== a.charAt(0) ? (i = r[a].apply(r, l), i !== r && i !== e ? (u = i && i.jquery ? u.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + a + "' for " + n + " widget instance") : t.error("cannot call methods on " + n + " prior to initialization; attempted to call method '" + a + "'")
                }) : this.each(function () {
                    var e = t.data(this, o);
                    e ? e.option(a || {})._init() : t.data(this, o, new r(a, this))
                }), u
            }
        }, t.Widget = function () { }, t.Widget._childConstructors = [], t.Widget.prototype = {
            widgetName: "widget",
            widgetEventPrefix: "",
            defaultElement: "<div>",
            options: {
                disabled: !1,
                create: null
            },
            _createWidget: function (e, i) {
                i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = n++ , this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, {
                    remove: function (t) {
                        t.target === i && this.destroy()
                    }
                }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
            },
            _getCreateOptions: t.noop,
            _getCreateEventData: t.noop,
            _create: t.noop,
            _init: t.noop,
            destroy: function () {
                this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
            },
            _destroy: t.noop,
            widget: function () {
                return this.element
            },
            option: function (n, i) {
                var r, o, a, s = n;
                if (0 === arguments.length) return t.widget.extend({}, this.options);
                if ("string" == typeof n)
                    if (s = {}, r = n.split("."), n = r.shift(), r.length) {
                        for (o = s[n] = t.widget.extend({}, this.options[n]), a = 0; a < r.length - 1; a++) o[r[a]] = o[r[a]] || {}, o = o[r[a]];
                        if (n = r.pop(), i === e) return o[n] === e ? null : o[n];
                        o[n] = i
                    } else {
                        if (i === e) return this.options[n] === e ? null : this.options[n];
                        s[n] = i
                    }
                return this._setOptions(s), this
            },
            _setOptions: function (t) {
                var e;
                for (e in t) this._setOption(e, t[e]);
                return this
            },
            _setOption: function (t, e) {
                return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
            },
            enable: function () {
                return this._setOption("disabled", !1)
            },
            disable: function () {
                return this._setOption("disabled", !0)
            },
            _on: function (e, n, i) {
                var r, o = this;
                "boolean" != typeof e && (i = n, n = e, e = !1), i ? (n = r = t(n), this.bindings = this.bindings.add(n)) : (i = n, n = this.element, r = this.widget()), t.each(i, function (i, a) {
                    function s() {
                        return e || o.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? o[a] : a).apply(o, arguments) : void 0
                    }
                    "string" != typeof a && (s.guid = a.guid = a.guid || s.guid || t.guid++);
                    var l = i.match(/^(\w+)\s*(.*)$/),
                        u = l[1] + o.eventNamespace,
                        c = l[2];
                    c ? r.delegate(c, u, s) : n.bind(u, s)
                })
            },
            _off: function (t, e) {
                e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
            },
            _delay: function (t, e) {
                function n() {
                    return ("string" == typeof t ? i[t] : t).apply(i, arguments)
                }
                var i = this;
                return setTimeout(n, e || 0)
            },
            _hoverable: function (e) {
                this.hoverable = this.hoverable.add(e), this._on(e, {
                    mouseenter: function (e) {
                        t(e.currentTarget).addClass("ui-state-hover")
                    },
                    mouseleave: function (e) {
                        t(e.currentTarget).removeClass("ui-state-hover")
                    }
                })
            },
            _focusable: function (e) {
                this.focusable = this.focusable.add(e), this._on(e, {
                    focusin: function (e) {
                        t(e.currentTarget).addClass("ui-state-focus")
                    },
                    focusout: function (e) {
                        t(e.currentTarget).removeClass("ui-state-focus")
                    }
                })
            },
            _trigger: function (e, n, i) {
                var r, o, a = this.options[e];
                if (i = i || {}, n = t.Event(n), n.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), n.target = this.element[0], o = n.originalEvent)
                    for (r in o) r in n || (n[r] = o[r]);
                return this.element.trigger(n, i), !(t.isFunction(a) && a.apply(this.element[0], [n].concat(i)) === !1 || n.isDefaultPrevented())
            }
        }, t.each({
            show: "fadeIn",
            hide: "fadeOut"
        }, function (e, n) {
            t.Widget.prototype["_" + e] = function (i, r, o) {
                "string" == typeof r && (r = {
                    effect: r
                });
                var a, s = r ? r === !0 || "number" == typeof r ? n : r.effect || n : e;
                r = r || {}, "number" == typeof r && (r = {
                    duration: r
                }), a = !t.isEmptyObject(r), r.complete = o, r.delay && i.delay(r.delay), a && t.effects && t.effects.effect[s] ? i[e](r) : s !== e && i[s] ? i[s](r.duration, r.easing, o) : i.queue(function (n) {
                    t(this)[e](), o && o.call(i[0]), n()
                })
            }
        })
    } (jQuery),
    function (t, e) {
        var n = !1;
        t(document).mouseup(function () {
            n = !1
        }), t.widget("ui.mouse", {
            version: "1.10.3",
            options: {
                cancel: "input,textarea,button,select,option",
                distance: 1,
                delay: 0
            },
            _mouseInit: function () {
                var e = this;
                this.element.bind("mousedown." + this.widgetName, function (t) {
                    return e._mouseDown(t)
                }).bind("click." + this.widgetName, function (n) {
                    return !0 === t.data(n.target, e.widgetName + ".preventClickEvent") ? (t.removeData(n.target, e.widgetName + ".preventClickEvent"), n.stopImmediatePropagation(), !1) : void 0
                }), this.started = !1
            },
            _mouseDestroy: function () {
                this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
            },
            _mouseDown: function (e) {
                if (!n) {
                    this._mouseStarted && this._mouseUp(e), this._mouseDownEvent = e;
                    var i = this,
                        r = 1 === e.which,
                        o = "string" == typeof this.options.cancel && e.target.nodeName ? t(e.target).closest(this.options.cancel).length : !1;
                    return r && !o && this._mouseCapture(e) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
                        i.mouseDelayMet = !0
                    }, this.options.delay)), this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(e) !== !1, !this._mouseStarted) ? (e.preventDefault(), !0) : (!0 === t.data(e.target, this.widgetName + ".preventClickEvent") && t.removeData(e.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (t) {
                        return i._mouseMove(t)
                    }, this._mouseUpDelegate = function (t) {
                        return i._mouseUp(t)
                    }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), e.preventDefault(), n = !0, !0)) : !0
                }
            },
            _mouseMove: function (e) {
                return t.ui.ie && (!document.documentMode || document.documentMode < 9) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
            },
            _mouseUp: function (e) {
                return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
            },
            _mouseDistanceMet: function (t) {
                return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
            },
            _mouseDelayMet: function () {
                return this.mouseDelayMet
            },
            _mouseStart: function () { },
            _mouseDrag: function () { },
            _mouseStop: function () { },
            _mouseCapture: function () {
                return !0
            }
        })
    } (jQuery),
    function (t, e) {
        var n = 5;
        t.widget("ui.slider", t.ui.mouse, {
            version: "1.10.3",
            widgetEventPrefix: "slide",
            options: {
                animate: !1,
                distance: 0,
                max: 100,
                min: 0,
                orientation: "horizontal",
                range: !1,
                step: 1,
                value: 0,
                values: null,
                change: null,
                slide: null,
                start: null,
                stop: null
            },
            _create: function () {
                this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1
            },
            _refresh: function () {
                this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue()
            },
            _createHandles: function () {
                var e, n, i = this.options,
                    r = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),
                    o = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",
                    a = [];
                for (n = i.values && i.values.length || 1, r.length > n && (r.slice(n).remove(), r = r.slice(0, n)), e = r.length; n > e; e++) a.push(o);
                this.handles = r.add(t(a.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function (e) {
                    t(this).data("ui-slider-handle-index", e)
                })
            },
            _createRange: function () {
                var e = this.options,
                    n = "";
                e.range ? (e.range === !0 && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [e.values[0], e.values[0]] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                    left: "",
                    bottom: ""
                }) : (this.range = t("<div></div>").appendTo(this.element), n = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(n + ("min" === e.range || "max" === e.range ? " ui-slider-range-" + e.range : ""))) : this.range = t([])
            },
            _setupEvents: function () {
                var t = this.handles.add(this.range).filter("a");
                this._off(t), this._on(t, this._handleEvents), this._hoverable(t), this._focusable(t)
            },
            _destroy: function () {
                this.handles.remove(), this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy()
            },
            _mouseCapture: function (e) {
                var n, i, r, o, a, s, l, u, c = this,
                    d = this.options;
                return d.disabled ? !1 : (this.elementSize = {
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight()
                }, this.elementOffset = this.element.offset(), n = {
                    x: e.pageX,
                    y: e.pageY
                }, i = this._normValueFromMouse(n), r = this._valueMax() - this._valueMin() + 1, this.handles.each(function (e) {
                    var n = Math.abs(i - c.values(e));
                    (r > n || r === n && (e === c._lastChangedValue || c.values(e) === d.min)) && (r = n, o = t(this), a = e)
                }), s = this._start(e, a), s === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = a, o.addClass("ui-state-active").focus(), l = o.offset(), u = !t(e.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = u ? {
                    left: 0,
                    top: 0
                } : {
                        left: e.pageX - l.left - o.width() / 2,
                        top: e.pageY - l.top - o.height() / 2 - (parseInt(o.css("borderTopWidth"), 10) || 0) - (parseInt(o.css("borderBottomWidth"), 10) || 0) + (parseInt(o.css("marginTop"), 10) || 0)
                    }, this.handles.hasClass("ui-state-hover") || this._slide(e, a, i), this._animateOff = !0, !0))
            },
            _mouseStart: function () {
                return !0
            },
            _mouseDrag: function (t) {
                var e = {
                    x: t.pageX,
                    y: t.pageY
                },
                    n = this._normValueFromMouse(e);
                return this._slide(t, this._handleIndex, n), !1
            },
            _mouseStop: function (t) {
                return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1
            },
            _detectOrientation: function () {
                this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
            },
            _normValueFromMouse: function (t) {
                var e, n, i, r, o;
                return "horizontal" === this.orientation ? (e = this.elementSize.width, n = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, n = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), i = n / e, i > 1 && (i = 1), 0 > i && (i = 0), "vertical" === this.orientation && (i = 1 - i), r = this._valueMax() - this._valueMin(), o = this._valueMin() + i * r, this._trimAlignValue(o)
            },
            _start: function (t, e) {
                var n = {
                    handle: this.handles[e],
                    value: this.value()
                };
                return this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._trigger("start", t, n)
            },
            _slide: function (t, e, n) {
                var i, r, o;
                this.options.values && this.options.values.length ? (i = this.values(e ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === e && n > i || 1 === e && i > n) && (n = i), n !== this.values(e) && (r = this.values(), r[e] = n, o = this._trigger("slide", t, {
                    handle: this.handles[e],
                    value: n,
                    values: r
                }), i = this.values(e ? 0 : 1), o !== !1 && this.values(e, n, !0))) : n !== this.value() && (o = this._trigger("slide", t, {
                    handle: this.handles[e],
                    value: n
                }), o !== !1 && this.value(n))
            },
            _stop: function (t, e) {
                var n = {
                    handle: this.handles[e],
                    value: this.value()
                };
                this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._trigger("stop", t, n)
            },
            _change: function (t, e) {
                if (!this._keySliding && !this._mouseSliding) {
                    var n = {
                        handle: this.handles[e],
                        value: this.value()
                    };
                    this.options.values && this.options.values.length && (n.value = this.values(e), n.values = this.values()), this._lastChangedValue = e, this._trigger("change", t, n)
                }
            },
            value: function (t) {
                return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), void this._change(null, 0)) : this._value()
            },
            values: function (e, n) {
                var i, r, o;
                if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(n), this._refreshValue(), void this._change(null, e);
                if (!arguments.length) return this._values();
                if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value();
                for (i = this.options.values, r = arguments[0], o = 0; o < i.length; o += 1) i[o] = this._trimAlignValue(r[o]), this._change(null, o);
                this._refreshValue()
            },
            _setOption: function (e, n) {
                var i, r = 0;
                switch ("range" === e && this.options.range === !0 && ("min" === n ? (this.options.value = this._values(0), this.options.values = null) : "max" === n && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), t.isArray(this.options.values) && (r = this.options.values.length), t.Widget.prototype._setOption.apply(this, arguments), e) {
                    case "orientation":
                        this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue();
                        break;
                    case "value":
                        this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
                        break;
                    case "values":
                        for (this._animateOff = !0, this._refreshValue(), i = 0; r > i; i += 1) this._change(null, i);
                        this._animateOff = !1;
                        break;
                    case "min":
                    case "max":
                        this._animateOff = !0, this._refreshValue(), this._animateOff = !1;
                        break;
                    case "range":
                        this._animateOff = !0, this._refresh(), this._animateOff = !1
                }
            },
            _value: function () {
                var t = this.options.value;
                return t = this._trimAlignValue(t)
            },
            _values: function (t) {
                var e, n, i;
                if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);
                if (this.options.values && this.options.values.length) {
                    for (n = this.options.values.slice(), i = 0; i < n.length; i += 1) n[i] = this._trimAlignValue(n[i]);
                    return n
                }
                return []
            },
            _trimAlignValue: function (t) {
                if (t <= this._valueMin()) return this._valueMin();
                if (t >= this._valueMax()) return this._valueMax();
                var e = this.options.step > 0 ? this.options.step : 1,
                    n = (t - this._valueMin()) % e,
                    i = t - n;
                return 2 * Math.abs(n) >= e && (i += n > 0 ? e : -e), parseFloat(i.toFixed(5))
            },
            _valueMin: function () {
                return this.options.min
            },
            _valueMax: function () {
                return this.options.max
            },
            _refreshValue: function () {
                var e, n, i, r, o, a = this.options.range,
                    s = this.options,
                    l = this,
                    u = this._animateOff ? !1 : s.animate,
                    c = {};
                this.options.values && this.options.values.length ? this.handles.each(function (i) {
                    n = (l.values(i) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100, c["horizontal" === l.orientation ? "left" : "bottom"] = n + "%", t(this).stop(1, 1)[u ? "animate" : "css"](c, s.animate), l.options.range === !0 && ("horizontal" === l.orientation ? (0 === i && l.range.stop(1, 1)[u ? "animate" : "css"]({
                        left: n + "%"
                    }, s.animate), 1 === i && l.range[u ? "animate" : "css"]({
                        width: n - e + "%"
                    }, {
                            queue: !1,
                            duration: s.animate
                        })) : (0 === i && l.range.stop(1, 1)[u ? "animate" : "css"]({
                            bottom: n + "%"
                        }, s.animate), 1 === i && l.range[u ? "animate" : "css"]({
                            height: n - e + "%"
                        }, {
                                queue: !1,
                                duration: s.animate
                            }))), e = n
                }) : (i = this.value(), r = this._valueMin(), o = this._valueMax(), n = o !== r ? (i - r) / (o - r) * 100 : 0, c["horizontal" === this.orientation ? "left" : "bottom"] = n + "%", this.handle.stop(1, 1)[u ? "animate" : "css"](c, s.animate), "min" === a && "horizontal" === this.orientation && this.range.stop(1, 1)[u ? "animate" : "css"]({
                    width: n + "%"
                }, s.animate), "max" === a && "horizontal" === this.orientation && this.range[u ? "animate" : "css"]({
                    width: 100 - n + "%"
                }, {
                        queue: !1,
                        duration: s.animate
                    }), "min" === a && "vertical" === this.orientation && this.range.stop(1, 1)[u ? "animate" : "css"]({
                        height: n + "%"
                    }, s.animate), "max" === a && "vertical" === this.orientation && this.range[u ? "animate" : "css"]({
                        height: 100 - n + "%"
                    }, {
                            queue: !1,
                            duration: s.animate
                        }))
            },
            _handleEvents: {
                keydown: function (e) {
                    var i, r, o, a, s = t(e.target).data("ui-slider-handle-index");
                    switch (e.keyCode) {
                        case t.ui.keyCode.HOME:
                        case t.ui.keyCode.END:
                        case t.ui.keyCode.PAGE_UP:
                        case t.ui.keyCode.PAGE_DOWN:
                        case t.ui.keyCode.UP:
                        case t.ui.keyCode.RIGHT:
                        case t.ui.keyCode.DOWN:
                        case t.ui.keyCode.LEFT:
                            if (e.preventDefault(), !this._keySliding && (this._keySliding = !0, t(e.target).addClass("ui-state-active"), i = this._start(e, s), i === !1)) return
                    }
                    switch (a = this.options.step, r = o = this.options.values && this.options.values.length ? this.values(s) : this.value(), e.keyCode) {
                        case t.ui.keyCode.HOME:
                            o = this._valueMin();
                            break;
                        case t.ui.keyCode.END:
                            o = this._valueMax();
                            break;
                        case t.ui.keyCode.PAGE_UP:
                            o = this._trimAlignValue(r + (this._valueMax() - this._valueMin()) / n);
                            break;
                        case t.ui.keyCode.PAGE_DOWN:
                            o = this._trimAlignValue(r - (this._valueMax() - this._valueMin()) / n);
                            break;
                        case t.ui.keyCode.UP:
                        case t.ui.keyCode.RIGHT:
                            if (r === this._valueMax()) return;
                            o = this._trimAlignValue(r + a);
                            break;
                        case t.ui.keyCode.DOWN:
                        case t.ui.keyCode.LEFT:
                            if (r === this._valueMin()) return;
                            o = this._trimAlignValue(r - a)
                    }
                    this._slide(e, s, o)
                },
                click: function (t) {
                    t.preventDefault()
                },
                keyup: function (e) {
                    var n = t(e.target).data("ui-slider-handle-index");
                    this._keySliding && (this._keySliding = !1, this._stop(e, n), this._change(e, n), t(e.target).removeClass("ui-state-active"))
                }
            }
        })
    } (jQuery),
    function (t, e) {
        var n = 0,
            i = {},
            r = {};
        i.height = i.paddingTop = i.paddingBottom = i.borderTopWidth = i.borderBottomWidth = "hide", r.height = r.paddingTop = r.paddingBottom = r.borderTopWidth = r.borderBottomWidth = "show", t.widget("ui.accordion", {
            version: "1.10.3",
            options: {
                active: 0,
                animate: {},
                collapsible: !1,
                event: "click",
                header: "> li > :first-child,> :not(li):even",
                heightStyle: "auto",
                icons: {
                    activeHeader: "ui-icon-triangle-1-s",
                    header: "ui-icon-triangle-1-e"
                },
                activate: null,
                beforeActivate: null
            },
            _create: function () {
                var e = this.options;
                this.prevShow = this.prevHide = t(), this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist"), e.collapsible || e.active !== !1 && null != e.active || (e.active = 0), this._processPanels(), e.active < 0 && (e.active += this.headers.length), this._refresh()
            },
            _getCreateEventData: function () {
                return {
                    header: this.active,
                    panel: this.active.length ? this.active.next() : t(),
                    content: this.active.length ? this.active.next() : t()
                }
            },
            _createIcons: function () {
                var e = this.options.icons;
                e && (t("<span>").addClass("ui-accordion-header-icon ui-icon " + e.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(e.header).addClass(e.activeHeader), this.headers.addClass("ui-accordion-icons"))
            },
            _destroyIcons: function () {
                this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
            },
            _destroy: function () {
                var t;
                this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function () {
                    /^ui-accordion/.test(this.id) && this.removeAttribute("id")
                }), this._destroyIcons(), t = this.headers.next().css("display", "").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function () {
                    /^ui-accordion/.test(this.id) && this.removeAttribute("id")
                }), "content" !== this.options.heightStyle && t.css("height", "")
            },
            _setOption: function (t, e) {
                return "active" === t ? void this._activate(e) : ("event" === t && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || this.options.active !== !1 || this._activate(0), "icons" === t && (this._destroyIcons(), e && this._createIcons()), void ("disabled" === t && this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!e)))
            },
            _keydown: function (e) {
                if (!e.altKey && !e.ctrlKey) {
                    var n = t.ui.keyCode,
                        i = this.headers.length,
                        r = this.headers.index(e.target),
                        o = !1;
                    switch (e.keyCode) {
                        case n.RIGHT:
                        case n.DOWN:
                            o = this.headers[(r + 1) % i];
                            break;
                        case n.LEFT:
                        case n.UP:
                            o = this.headers[(r - 1 + i) % i];
                            break;
                        case n.SPACE:
                        case n.ENTER:
                            this._eventHandler(e);
                            break;
                        case n.HOME:
                            o = this.headers[0];
                            break;
                        case n.END:
                            o = this.headers[i - 1]
                    }
                    o && (t(e.target).attr("tabIndex", -1), t(o).attr("tabIndex", 0), o.focus(), e.preventDefault())
                }
            },
            _panelKeyDown: function (e) {
                e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().focus()
            },
            refresh: function () {
                var e = this.options;
                this._processPanels(), e.active === !1 && e.collapsible === !0 || !this.headers.length ? (e.active = !1, this.active = t()) : e.active === !1 ? this._activate(0) : this.active.length && !t.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (e.active = !1, this.active = t()) : this._activate(Math.max(0, e.active - 1)) : e.active = this.headers.index(this.active), this._destroyIcons(), this._refresh()
            },
            _processPanels: function () {
                this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"), this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()
            },
            _refresh: function () {
                var e, i = this.options,
                    r = i.heightStyle,
                    o = this.element.parent(),
                    a = this.accordionId = "ui-accordion-" + (this.element.attr("id") || ++n);
                this.active = this._findActive(i.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"), this.active.next().addClass("ui-accordion-content-active").show(), this.headers.attr("role", "tab").each(function (e) {
                    var n = t(this),
                        i = n.attr("id"),
                        r = n.next(),
                        o = r.attr("id");
                    i || (i = a + "-header-" + e, n.attr("id", i)), o || (o = a + "-panel-" + e, r.attr("id", o)), n.attr("aria-controls", o), r.attr("aria-labelledby", i)
                }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
                    "aria-selected": "false",
                    tabIndex: -1
                }).next().attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }).hide(), this.active.length ? this.active.attr({
                    "aria-selected": "true",
                    tabIndex: 0
                }).next().attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(i.event), "fill" === r ? (e = o.height(), this.element.siblings(":visible").each(function () {
                    var n = t(this),
                        i = n.css("position");
                    "absolute" !== i && "fixed" !== i && (e -= n.outerHeight(!0))
                }), this.headers.each(function () {
                    e -= t(this).outerHeight(!0)
                }), this.headers.next().each(function () {
                    t(this).height(Math.max(0, e - t(this).innerHeight() + t(this).height()))
                }).css("overflow", "auto")) : "auto" === r && (e = 0, this.headers.next().each(function () {
                    e = Math.max(e, t(this).css("height", "").height())
                }).height(e))
            },
            _activate: function (e) {
                var n = this._findActive(e)[0];
                n !== this.active[0] && (n = n || this.active[0], this._eventHandler({
                    target: n,
                    currentTarget: n,
                    preventDefault: t.noop
                }))
            },
            _findActive: function (e) {
                return "number" == typeof e ? this.headers.eq(e) : t()
            },
            _setupEvents: function (e) {
                var n = {
                    keydown: "_keydown"
                };
                e && t.each(e.split(" "), function (t, e) {
                    n[e] = "_eventHandler"
                }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, n), this._on(this.headers.next(), {
                    keydown: "_panelKeyDown"
                }), this._hoverable(this.headers), this._focusable(this.headers)
            },
            _eventHandler: function (e) {
                var n = this.options,
                    i = this.active,
                    r = t(e.currentTarget),
                    o = r[0] === i[0],
                    a = o && n.collapsible,
                    s = a ? t() : r.next(),
                    l = i.next(),
                    u = {
                        oldHeader: i,
                        oldPanel: l,
                        newHeader: a ? t() : r,
                        newPanel: s
                    };
                e.preventDefault(), o && !n.collapsible || this._trigger("beforeActivate", e, u) === !1 || (n.active = a ? !1 : this.headers.index(r), this.active = o ? t() : r, this._toggle(u), i.removeClass("ui-accordion-header-active ui-state-active"), n.icons && i.children(".ui-accordion-header-icon").removeClass(n.icons.activeHeader).addClass(n.icons.header), o || (r.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), n.icons && r.children(".ui-accordion-header-icon").removeClass(n.icons.header).addClass(n.icons.activeHeader), r.next().addClass("ui-accordion-content-active")))
            },
            _toggle: function (e) {
                var n = e.newPanel,
                    i = this.prevShow.length ? this.prevShow : e.oldPanel;
                this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = n, this.prevHide = i, this.options.animate ? this._animate(n, i, e) : (i.hide(), n.show(), this._toggleComplete(e)), i.attr({
                    "aria-expanded": "false",
                    "aria-hidden": "true"
                }), i.prev().attr("aria-selected", "false"), n.length && i.length ? i.prev().attr("tabIndex", -1) : n.length && this.headers.filter(function () {
                    return 0 === t(this).attr("tabIndex")
                }).attr("tabIndex", -1), n.attr({
                    "aria-expanded": "true",
                    "aria-hidden": "false"
                }).prev().attr({
                    "aria-selected": "true",
                    tabIndex: 0
                })
            },
            _animate: function (t, e, n) {
                var o, a, s, l = this,
                    u = 0,
                    c = t.length && (!e.length || t.index() < e.index()),
                    d = this.options.animate || {},
                    h = c && d.down || d,
                    f = function () {
                        l._toggleComplete(n)
                    };
                return "number" == typeof h && (s = h), "string" == typeof h && (a = h), a = a || h.easing || d.easing, s = s || h.duration || d.duration, e.length ? t.length ? (o = t.show().outerHeight(), e.animate(i, {
                    duration: s,
                    easing: a,
                    step: function (t, e) {
                        e.now = Math.round(t)
                    }
                }), void t.hide().animate(r, {
                    duration: s,
                    easing: a,
                    complete: f,
                    step: function (t, n) {
                        n.now = Math.round(t), "height" !== n.prop ? u += n.now : "content" !== l.options.heightStyle && (n.now = Math.round(o - e.outerHeight() - u), u = 0)
                    }
                })) : e.animate(i, s, a, f) : t.animate(r, s, a, f)
            },
            _toggleComplete: function (t) {
                var e = t.oldPanel;
                e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t)
            }
        })
    } (jQuery),
    function t(e, n, i) {
        function r(a, s) {
            if (!n[a]) {
                if (!e[a]) {
                    var l = "function" == typeof require && require;
                    if (!s && l) return l(a, !0);
                    if (o) return o(a, !0);
                    var u = new Error("Cannot find module '" + a + "'");
                    throw u.code = "MODULE_NOT_FOUND", u
                }
                var c = n[a] = {
                    exports: {}
                };
                e[a][0].call(c.exports, function (t) {
                    var n = e[a][1][t];
                    return r(n ? n : t)
                }, c, c.exports, t, e, n, i)
            }
            return n[a].exports
        }
        for (var o = "function" == typeof require && require, a = 0; a < i.length; a++) r(i[a]);
        return r
    } ({
        1: [function (t, e, n) {
            ! function (n, i) {
                "use strict";
                var r = n.document,
                    o = t("./src/utils/get-by-class"),
                    a = t("./src/utils/extend"),
                    s = t("./src/utils/index-of"),
                    l = t("./src/utils/events"),
                    u = t("./src/utils/to-string"),
                    c = t("./src/utils/natural-sort"),
                    d = t("./src/utils/classes"),
                    h = t("./src/utils/get-attribute"),
                    f = t("./src/utils/to-array"),
                    p = function (e, n, m) {
                        var g, v = this,
                            y = t("./src/item")(v),
                            _ = t("./src/add-async")(v);
                        g = {
                            start: function () {
                                v.listClass = "list",
                                    v.searchClass = "search", v.sortClass = "sort", v.page = 1e4, v.i = 1, v.items = [], v.visibleItems = [], v.matchingItems = [], v.searched = !1, v.filtered = !1, v.searchColumns = i, v.handlers = {
                                        updated: []
                                    }, v.plugins = {}, v.valueNames = [], v.utils = {
                                        getByClass: o,
                                        extend: a,
                                        indexOf: s,
                                        events: l,
                                        toString: u,
                                        naturalSort: c,
                                        classes: d,
                                        getAttribute: h,
                                        toArray: f
                                    }, v.utils.extend(v, n), v.listContainer = "string" == typeof e ? r.getElementById(e) : e, v.listContainer && (v.list = o(v.listContainer, v.listClass, !0), v.parse = t("./src/parse")(v), v.templater = t("./src/templater")(v), v.search = t("./src/search")(v), v.filter = t("./src/filter")(v), v.sort = t("./src/sort")(v), this.handlers(), this.items(), v.update(), this.plugins())
                            },
                            handlers: function () {
                                for (var t in v.handlers) v[t] && v.on(t, v[t])
                            },
                            items: function () {
                                v.parse(v.list), m !== i && v.add(m)
                            },
                            plugins: function () {
                                for (var t = 0; t < v.plugins.length; t++) {
                                    var e = v.plugins[t];
                                    v[e.name] = e, e.init(v, p)
                                }
                            }
                        }, this.reIndex = function () {
                            v.items = [], v.visibleItems = [], v.matchingItems = [], v.searched = !1, v.filtered = !1, v.parse(v.list)
                        }, this.toJSON = function () {
                            for (var t = [], e = 0, n = v.items.length; n > e; e++) t.push(v.items[e].values());
                            return t
                        }, this.add = function (t, e) {
                            if (0 !== t.length) {
                                if (e) return void _(t, e);
                                var n = [],
                                    r = !1;
                                t[0] === i && (t = [t]);
                                for (var o = 0, a = t.length; a > o; o++) {
                                    var s = null;
                                    r = v.items.length > v.page ? !0 : !1, s = new y(t[o], i, r), v.items.push(s), n.push(s)
                                }
                                return v.update(), n
                            }
                        }, this.show = function (t, e) {
                            return this.i = t, this.page = e, v.update(), v
                        }, this.remove = function (t, e, n) {
                            for (var i = 0, r = 0, o = v.items.length; o > r; r++) v.items[r].values()[t] == e && (v.templater.remove(v.items[r], n), v.items.splice(r, 1), o-- , r-- , i++);
                            return v.update(), i
                        }, this.get = function (t, e) {
                            for (var n = [], i = 0, r = v.items.length; r > i; i++) {
                                var o = v.items[i];
                                o.values()[t] == e && n.push(o)
                            }
                            return n
                        }, this.size = function () {
                            return v.items.length
                        }, this.clear = function () {
                            return v.templater.clear(), v.items = [], v
                        }, this.on = function (t, e) {
                            return v.handlers[t].push(e), v
                        }, this.off = function (t, e) {
                            var n = v.handlers[t],
                                i = s(n, e);
                            return i > -1 && n.splice(i, 1), v
                        }, this.trigger = function (t) {
                            for (var e = v.handlers[t].length; e--;) v.handlers[t][e](v);
                            return v
                        }, this.reset = {
                            filter: function () {
                                for (var t = v.items, e = t.length; e--;) t[e].filtered = !1;
                                return v
                            },
                            search: function () {
                                for (var t = v.items, e = t.length; e--;) t[e].found = !1;
                                return v
                            }
                        }, this.update = function () {
                            var t = v.items,
                                e = t.length;
                            v.visibleItems = [], v.matchingItems = [], v.templater.clear();
                            for (var n = 0; e > n; n++) t[n].matching() && v.matchingItems.length + 1 >= v.i && v.visibleItems.length < v.page ? (t[n].show(), v.visibleItems.push(t[n]), v.matchingItems.push(t[n])) : t[n].matching() ? (v.matchingItems.push(t[n]), t[n].hide()) : t[n].hide();
                            return v.trigger("updated"), v
                        }, g.start()
                    };
                "function" == typeof define && define.amd && define(function () {
                    return p
                }), e.exports = p, n.List = p
            } (window)
        }, {
            "./src/add-async": 2,
            "./src/filter": 3,
            "./src/item": 4,
            "./src/parse": 5,
            "./src/search": 6,
            "./src/sort": 7,
            "./src/templater": 8,
            "./src/utils/classes": 9,
            "./src/utils/events": 10,
            "./src/utils/extend": 11,
            "./src/utils/get-attribute": 12,
            "./src/utils/get-by-class": 13,
            "./src/utils/index-of": 14,
            "./src/utils/natural-sort": 15,
            "./src/utils/to-array": 16,
            "./src/utils/to-string": 17
        }],
        2: [function (t, e, n) {
            e.exports = function (t) {
                var e = function (n, i, r) {
                    var o = n.splice(0, 50);
                    r = r || [], r = r.concat(t.add(o)), n.length > 0 ? setTimeout(function () {
                        e(n, i, r)
                    }, 1) : (t.update(), i(r))
                };
                return e
            }
        }, {}],
        3: [function (t, e, n) {
            e.exports = function (t) {
                return t.handlers.filterStart = t.handlers.filterStart || [], t.handlers.filterComplete = t.handlers.filterComplete || [],
                    function (e) {
                        if (t.trigger("filterStart"), t.i = 1, t.reset.filter(), void 0 === e) t.filtered = !1;
                        else {
                            t.filtered = !0;
                            for (var n = t.items, i = 0, r = n.length; r > i; i++) {
                                var o = n[i];
                                e(o) ? o.filtered = !0 : o.filtered = !1
                            }
                        }
                        return t.update(), t.trigger("filterComplete"), t.visibleItems
                    }
            }
        }, {}],
        4: [function (t, e, n) {
            e.exports = function (t) {
                return function (e, n, i) {
                    var r = this;
                    this._values = {}, this.found = !1, this.filtered = !1;
                    var o = function (e, n, i) {
                        if (void 0 === n) i ? r.values(e, i) : r.values(e);
                        else {
                            r.elm = n;
                            var o = t.templater.get(r, e);
                            r.values(o)
                        }
                    };
                    this.values = function (e, n) {
                        if (void 0 === e) return r._values;
                        for (var i in e) r._values[i] = e[i];
                        n !== !0 && t.templater.set(r, r.values())
                    }, this.show = function () {
                        t.templater.show(r)
                    }, this.hide = function () {
                        t.templater.hide(r)
                    }, this.matching = function () {
                        return t.filtered && t.searched && r.found && r.filtered || t.filtered && !t.searched && r.filtered || !t.filtered && t.searched && r.found || !t.filtered && !t.searched
                    }, this.visible = function () {
                        return r.elm && r.elm.parentNode == t.list ? !0 : !1
                    }, o(e, n, i)
                }
            }
        }, {}],
        5: [function (t, e, n) {
            e.exports = function (e) {
                var n = t("./item")(e),
                    i = function (t) {
                        for (var e = t.childNodes, n = [], i = 0, r = e.length; r > i; i++) void 0 === e[i].data && n.push(e[i]);
                        return n
                    },
                    r = function (t, i) {
                        for (var r = 0, o = t.length; o > r; r++) e.items.push(new n(i, t[r]))
                    },
                    o = function (t, n) {
                        var i = t.splice(0, 50);
                        r(i, n), t.length > 0 ? setTimeout(function () {
                            o(t, n)
                        }, 1) : (e.update(), e.trigger("parseComplete"))
                    };
                return e.handlers.parseComplete = e.handlers.parseComplete || [],
                    function () {
                        var t = i(e.list),
                            n = e.valueNames;
                        e.indexAsync ? o(t, n) : r(t, n)
                    }
            }
        }, {
            "./item": 4
        }],
        6: [function (t, e, n) {
            e.exports = function (t) {
                var e, n, i, r, o = {
                    resetList: function () {
                        t.i = 1, t.templater.clear(), r = void 0
                    },
                    setOptions: function (t) {
                        2 == t.length && t[1] instanceof Array ? n = t[1] : 2 == t.length && "function" == typeof t[1] ? r = t[1] : 3 == t.length && (n = t[1], r = t[2])
                    },
                    setColumns: function () {
                        0 !== t.items.length && void 0 === n && (n = void 0 === t.searchColumns ? o.toArray(t.items[0].values()) : t.searchColumns)
                    },
                    setSearchString: function (e) {
                        e = t.utils.toString(e).toLowerCase(), e = e.replace(/[-[\]{}()*+?.,\\^$|#]/g, "\\$&"), i = e
                    },
                    toArray: function (t) {
                        var e = [];
                        for (var n in t) e.push(n);
                        return e
                    }
                },
                    a = {
                        list: function () {
                            for (var e = 0, n = t.items.length; n > e; e++) a.item(t.items[e])
                        },
                        item: function (t) {
                            t.found = !1;
                            for (var e = 0, i = n.length; i > e; e++)
                                if (a.values(t.values(), n[e])) return void (t.found = !0)
                        },
                        values: function (n, r) {
                            return n.hasOwnProperty(r) && (e = t.utils.toString(n[r]).toLowerCase(), "" !== i && e.search(i) > -1) ? !0 : !1
                        },
                        reset: function () {
                            t.reset.search(), t.searched = !1
                        }
                    },
                    s = function (e) {
                        return t.trigger("searchStart"), o.resetList(), o.setSearchString(e), o.setOptions(arguments), o.setColumns(), "" === i ? a.reset() : (t.searched = !0, r ? r(i, n) : a.list()), t.update(), t.trigger("searchComplete"), t.visibleItems
                    };
                return t.handlers.searchStart = t.handlers.searchStart || [], t.handlers.searchComplete = t.handlers.searchComplete || [], t.utils.events.bind(t.utils.getByClass(t.listContainer, t.searchClass), "keyup", function (e) {
                    var n = e.target || e.srcElement,
                        i = "" === n.value && !t.searched;
                    i || s(n.value)
                }), t.utils.events.bind(t.utils.getByClass(t.listContainer, t.searchClass), "input", function (t) {
                    var e = t.target || t.srcElement;
                    "" === e.value && s("")
                }), s
            }
        }, {}],
        7: [function (t, e, n) {
            e.exports = function (t) {
                t.sortFunction = t.sortFunction || function (e, n, i) {
                    return i.desc = "desc" == i.order ? !0 : !1, t.utils.naturalSort(e.values()[i.valueName], n.values()[i.valueName], i)
                };
                var e = {
                    els: void 0,
                    clear: function () {
                        for (var n = 0, i = e.els.length; i > n; n++) t.utils.classes(e.els[n]).remove("asc"), t.utils.classes(e.els[n]).remove("desc")
                    },
                    getOrder: function (e) {
                        var n = t.utils.getAttribute(e, "data-order");
                        return "asc" == n || "desc" == n ? n : t.utils.classes(e).has("desc") ? "asc" : t.utils.classes(e).has("asc") ? "desc" : "asc"
                    },
                    getInSensitive: function (e, n) {
                        var i = t.utils.getAttribute(e, "data-insensitive");
                        "false" === i ? n.insensitive = !1 : n.insensitive = !0
                    },
                    setOrder: function (n) {
                        for (var i = 0, r = e.els.length; r > i; i++) {
                            var o = e.els[i];
                            if (t.utils.getAttribute(o, "data-sort") === n.valueName) {
                                var a = t.utils.getAttribute(o, "data-order");
                                "asc" == a || "desc" == a ? a == n.order && t.utils.classes(o).add(n.order) : t.utils.classes(o).add(n.order)
                            }
                        }
                    }
                },
                    n = function () {
                        t.trigger("sortStart");
                        var n = {},
                            i = arguments[0].currentTarget || arguments[0].srcElement || void 0;
                        i ? (n.valueName = t.utils.getAttribute(i, "data-sort"), e.getInSensitive(i, n), n.order = e.getOrder(i)) : (n = arguments[1] || n, n.valueName = arguments[0], n.order = n.order || "asc", n.insensitive = "undefined" == typeof n.insensitive ? !0 : n.insensitive), e.clear(), e.setOrder(n), n.sortFunction = n.sortFunction || t.sortFunction, t.items.sort(function (t, e) {
                            var i = "desc" === n.order ? -1 : 1;
                            return n.sortFunction(t, e, n) * i
                        }), t.update(), t.trigger("sortComplete")
                    };
                return t.handlers.sortStart = t.handlers.sortStart || [], t.handlers.sortComplete = t.handlers.sortComplete || [], e.els = t.utils.getByClass(t.listContainer, t.sortClass), t.utils.events.bind(e.els, "click", n), t.on("searchStart", e.clear), t.on("filterStart", e.clear), n
            }
        }, {}],
        8: [function (t, e, n) {
            var i = function (t) {
                var e, n = this,
                    i = function () {
                        e = n.getItemSource(t.item), e = n.clearSourceItem(e, t.valueNames)
                    };
                this.clearSourceItem = function (e, n) {
                    for (var i = 0, r = n.length; r > i; i++) {
                        var o;
                        if (n[i].data)
                            for (var a = 0, s = n[i].data.length; s > a; a++) e.setAttribute("data-" + n[i].data[a], "");
                        else n[i].attr && n[i].name ? (o = t.utils.getByClass(e, n[i].name, !0), o && o.setAttribute(n[i].attr, "")) : (o = t.utils.getByClass(e, n[i], !0), o && (o.innerHTML = ""));
                        o = void 0
                    }
                    return e
                }, this.getItemSource = function (e) {
                    if (void 0 === e) {
                        for (var n = t.list.childNodes, i = 0, r = n.length; r > i; i++)
                            if (void 0 === n[i].data) return n[i].cloneNode(!0)
                    } else {
                        if (/^tr[\s>]/.exec(e)) {
                            var o = document.createElement("table");
                            return o.innerHTML = e, o.firstChild
                        }
                        if (-1 !== e.indexOf("<")) {
                            var a = document.createElement("div");
                            return a.innerHTML = e, a.firstChild
                        }
                        var s = document.getElementById(t.item);
                        if (s) return s
                    }
                    throw new Error("The list need to have at list one item on init otherwise you'll have to add a template.")
                }, this.get = function (e, i) {
                    n.create(e);
                    for (var r = {}, o = 0, a = i.length; a > o; o++) {
                        var s;
                        if (i[o].data)
                            for (var l = 0, u = i[o].data.length; u > l; l++) r[i[o].data[l]] = t.utils.getAttribute(e.elm, "data-" + i[o].data[l]);
                        else i[o].attr && i[o].name ? (s = t.utils.getByClass(e.elm, i[o].name, !0), r[i[o].name] = s ? t.utils.getAttribute(s, i[o].attr) : "") : (s = t.utils.getByClass(e.elm, i[o], !0), r[i[o]] = s ? s.innerHTML : "");
                        s = void 0
                    }
                    return r
                }, this.set = function (e, i) {
                    var r = function (e) {
                        for (var n = 0, i = t.valueNames.length; i > n; n++)
                            if (t.valueNames[n].data) {
                                for (var r = t.valueNames[n].data, o = 0, a = r.length; a > o; o++)
                                    if (r[o] === e) return {
                                        data: e
                                    }
                            } else {
                                if (t.valueNames[n].attr && t.valueNames[n].name && t.valueNames[n].name == e) return t.valueNames[n];
                                if (t.valueNames[n] === e) return e
                            }
                    },
                        o = function (n, i) {
                            var o, a = r(n);
                            a && (a.data ? e.elm.setAttribute("data-" + a.data, i) : a.attr && a.name ? (o = t.utils.getByClass(e.elm, a.name, !0), o && o.setAttribute(a.attr, i)) : (o = t.utils.getByClass(e.elm, a, !0), o && (o.innerHTML = i)), o = void 0)
                        };
                    if (!n.create(e))
                        for (var a in i) i.hasOwnProperty(a) && o(a, i[a])
                }, this.create = function (t) {
                    if (void 0 !== t.elm) return !1;
                    var i = e.cloneNode(!0);
                    return i.removeAttribute("id"), t.elm = i, n.set(t, t.values()), !0
                }, this.remove = function (e) {
                    e.elm.parentNode === t.list && t.list.removeChild(e.elm)
                }, this.show = function (e) {
                    n.create(e), t.list.appendChild(e.elm)
                }, this.hide = function (e) {
                    void 0 !== e.elm && e.elm.parentNode === t.list && t.list.removeChild(e.elm)
                }, this.clear = function () {
                    if (t.list.hasChildNodes())
                        for (; t.list.childNodes.length >= 1;) t.list.removeChild(t.list.firstChild)
                }, i()
            };
            e.exports = function (t) {
                return new i(t)
            }
        }, {}],
        9: [function (t, e, n) {
            function i(t) {
                if (!t || !t.nodeType) throw new Error("A DOM element reference is required");
                this.el = t, this.list = t.classList
            }
            var r = t("./index-of"),
                o = /\s+/,
                a = Object.prototype.toString;
            e.exports = function (t) {
                return new i(t)
            }, i.prototype.add = function (t) {
                if (this.list) return this.list.add(t), this;
                var e = this.array(),
                    n = r(e, t);
                return ~n || e.push(t), this.el.className = e.join(" "), this
            }, i.prototype.remove = function (t) {
                if ("[object RegExp]" == a.call(t)) return this.removeMatching(t);
                if (this.list) return this.list.remove(t), this;
                var e = this.array(),
                    n = r(e, t);
                return ~n && e.splice(n, 1), this.el.className = e.join(" "), this
            }, i.prototype.removeMatching = function (t) {
                for (var e = this.array(), n = 0; n < e.length; n++) t.test(e[n]) && this.remove(e[n]);
                return this
            }, i.prototype.toggle = function (t, e) {
                return this.list ? ("undefined" != typeof e ? e !== this.list.toggle(t, e) && this.list.toggle(t) : this.list.toggle(t), this) : ("undefined" != typeof e ? e ? this.add(t) : this.remove(t) : this.has(t) ? this.remove(t) : this.add(t), this)
            }, i.prototype.array = function () {
                var t = this.el.getAttribute("class") || "",
                    e = t.replace(/^\s+|\s+$/g, ""),
                    n = e.split(o);
                return "" === n[0] && n.shift(), n
            }, i.prototype.has = i.prototype.contains = function (t) {
                return this.list ? this.list.contains(t) : !!~r(this.array(), t)
            }
        }, {
            "./index-of": 14
        }],
        10: [function (t, e, n) {
            var i = window.addEventListener ? "addEventListener" : "attachEvent",
                r = window.removeEventListener ? "removeEventListener" : "detachEvent",
                o = "addEventListener" !== i ? "on" : "",
                a = t("./to-array");
            n.bind = function (t, e, n, r) {
                t = a(t);
                for (var s = 0; s < t.length; s++) t[s][i](o + e, n, r || !1)
            }, n.unbind = function (t, e, n, i) {
                t = a(t);
                for (var s = 0; s < t.length; s++) t[s][r](o + e, n, i || !1)
            }
        }, {
            "./to-array": 16
        }],
        11: [function (t, e, n) {
            e.exports = function (t) {
                for (var e, n = Array.prototype.slice.call(arguments, 1), i = 0; e = n[i]; i++)
                    if (e)
                        for (var r in e) t[r] = e[r];
                return t
            }
        }, {}],
        12: [function (t, e, n) {
            e.exports = function (t, e) {
                var n = t.getAttribute && t.getAttribute(e) || null;
                if (!n)
                    for (var i = t.attributes, r = i.length, o = 0; r > o; o++) void 0 !== e[o] && e[o].nodeName === e && (n = e[o].nodeValue);
                return n
            }
        }, {}],
        13: [function (t, e, n) {
            e.exports = function () {
                return document.getElementsByClassName ? function (t, e, n) {
                    return n ? t.getElementsByClassName(e)[0] : t.getElementsByClassName(e)
                } : document.querySelector ? function (t, e, n) {
                    return e = "." + e, n ? t.querySelector(e) : t.querySelectorAll(e)
                } : function (t, e, n) {
                    var i = [],
                        r = "*";
                    null === t && (t = document);
                    for (var o = t.getElementsByTagName(r), a = o.length, s = new RegExp("(^|\\s)" + e + "(\\s|$)"), l = 0, u = 0; a > l; l++)
                        if (s.test(o[l].className)) {
                            if (n) return o[l];
                            i[u] = o[l], u++
                        }
                    return i
                }
            } ()
        }, {}],
        14: [function (t, e, n) {
            var i = [].indexOf;
            e.exports = function (t, e) {
                if (i) return t.indexOf(e);
                for (var n = 0; n < t.length; ++n)
                    if (t[n] === e) return n;
                return -1
            }
        }, {}],
        15: [function (t, e, n) {
            e.exports = function (t, e, n) {
                var i, r, o = /(^([+\-]?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?(?=\D|\s|$))|^0x[\da-fA-F]+$|\d+)/g,
                    a = /^\s+|\s+$/g,
                    s = /\s+/g,
                    l = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
                    u = /^0x[0-9a-f]+$/i,
                    c = /^0/,
                    d = n || {},
                    h = function (t) {
                        return (d.insensitive && ("" + t).toLowerCase() || "" + t).replace(a, "")
                    },
                    f = h(t),
                    p = h(e),
                    m = f.replace(o, "\x00$1\x00").replace(/\0$/, "").replace(/^\0/, "").split("\x00"),
                    g = p.replace(o, "\x00$1\x00").replace(/\0$/, "").replace(/^\0/, "").split("\x00"),
                    v = parseInt(f.match(u), 16) || 1 !== m.length && Date.parse(f),
                    y = parseInt(p.match(u), 16) || v && p.match(l) && Date.parse(p) || null,
                    _ = function (t, e) {
                        return (!t.match(c) || 1 == e) && parseFloat(t) || t.replace(s, " ").replace(a, "") || 0
                    };
                if (y) {
                    if (y > v) return -1;
                    if (v > y) return 1
                }
                for (var w = 0, b = m.length, x = g.length, k = Math.max(b, x); k > w; w++) {
                    if (i = _(m[w] || "", b), r = _(g[w] || "", x), isNaN(i) !== isNaN(r)) return isNaN(i) ? 1 : -1;
                    if (/[^\x00-\x80]/.test(i + r) && i.localeCompare) {
                        var C = i.localeCompare(r);
                        return C / Math.abs(C)
                    }
                    if (r > i) return -1;
                    if (i > r) return 1
                }
                return 0
            }
        }, {}],
        16: [function (t, e, n) {
            function i(t) {
                return "[object Array]" === Object.prototype.toString.call(t)
            }
            e.exports = function (t) {
                if ("undefined" == typeof t) return [];
                if (null === t) return [null];
                if (t === window) return [window];
                if ("string" == typeof t) return [t];
                if (i(t)) return t;
                if ("number" != typeof t.length) return [t];
                if ("function" == typeof t && t instanceof Function) return [t];
                for (var e = [], n = 0; n < t.length; n++)(Object.prototype.hasOwnProperty.call(t, n) || n in t) && e.push(t[n]);
                return e.length ? e : []
            }
        }, {}],
        17: [function (t, e, n) {
            e.exports = function (t) {
                return t = void 0 === t ? "" : t, t = null === t ? "" : t, t = t.toString()
            }
        }, {}]
    }, {}, [1]);
var fixto = function (t, e, n) {
    function i() {
        this._vendor = null
    }

    function r() {
        var t = !1,
            e = n.createElement("div"),
            i = n.createElement("div");
        e.appendChild(i), e.style[f] = "translate(0)", e.style.marginTop = "10px", e.style.visibility = "hidden", i.style.position = "fixed", i.style.top = 0, n.body.appendChild(e);
        var r = i.getBoundingClientRect();
        return r.top > 0 && (t = !0), n.body.removeChild(e), t
    }

    function o(e, n, i) {
        this.child = e, this._$child = t(e), this.parent = n, this.options = {
            className: "fixto-fixed",
            top: 0
        }, this._setOptions(i)
    }

    function a(t, e, n) {
        o.call(this, t, e, n), this._replacer = new u.MimicNode(t), this._ghostNode = this._replacer.replacer, this._saveStyles(), this._saveViewportHeight(), this._proxied_onscroll = this._bind(this._onscroll, this), this._proxied_onresize = this._bind(this._onresize, this), this.start()
    }

    function s(t, e, n) {
        o.call(this, t, e, n), this.start()
    }
    var l = function () {
        var t = {
            getAll: function (t) {
                return n.defaultView.getComputedStyle(t)
            },
            get: function (t, e) {
                return this.getAll(t)[e]
            },
            toFloat: function (t) {
                return parseFloat(t, 10) || 0
            },
            getFloat: function (t, e) {
                return this.toFloat(this.get(t, e))
            },
            _getAllCurrentStyle: function (t) {
                return t.currentStyle
            }
        };
        return n.documentElement.currentStyle && (t.getAll = t._getAllCurrentStyle), t
    } (),
        u = function () {
            function e(t) {
                this.element = t, this.replacer = n.createElement("div"), this.replacer.style.visibility = "hidden", this.hide(), t.parentNode.insertBefore(this.replacer, t)
            }
            e.prototype = {
                replace: function () {
                    var t = this.replacer.style,
                        e = l.getAll(this.element);
                    t.width = this._width(), t.height = this._height(), t.marginTop = e.marginTop, t.marginBottom = e.marginBottom, t.marginLeft = e.marginLeft, t.marginRight = e.marginRight, t.cssFloat = e.cssFloat, t.styleFloat = e.styleFloat, t.position = e.position, t.top = e.top, t.right = e.right, t.bottom = e.bottom, t.left = e.left, t.display = e.display
                },
                hide: function () {
                    this.replacer.style.display = "none"
                },
                _width: function () {
                    return this.element.getBoundingClientRect().width + "px"
                },
                _widthOffset: function () {
                    return this.element.offsetWidth + "px"
                },
                _height: function () {
                    return this.element.getBoundingClientRect().height + "px"
                },
                _heightOffset: function () {
                    return this.element.offsetHeight + "px"
                },
                destroy: function () {
                    t(this.replacer).remove();
                    for (var e in this) this.hasOwnProperty(e) && (this[e] = null)
                }
            };
            var i = n.documentElement.getBoundingClientRect();
            return i.width || (e.prototype._width = e.prototype._widthOffset, e.prototype._height = e.prototype._heightOffset), {
                MimicNode: e,
                computedStyle: l
            }
        } ();
    i.prototype = {
        _vendors: {
            webkit: {
                cssPrefix: "-webkit-",
                jsPrefix: "Webkit"
            },
            moz: {
                cssPrefix: "-moz-",
                jsPrefix: "Moz"
            },
            ms: {
                cssPrefix: "-ms-",
                jsPrefix: "ms"
            },
            opera: {
                cssPrefix: "-o-",
                jsPrefix: "O"
            }
        },
        _prefixJsProperty: function (t, e) {
            return t.jsPrefix + e[0].toUpperCase() + e.substr(1)
        },
        _prefixValue: function (t, e) {
            return t.cssPrefix + e
        },
        _valueSupported: function (t, e, n) {
            try {
                return n.style[t] = e, n.style[t] === e
            } catch (i) {
                return !1
            }
        },
        propertySupported: function (t) {
            return void 0 !== n.documentElement.style[t]
        },
        getJsProperty: function (t) {
            if (this.propertySupported(t)) return t;
            if (this._vendor) return this._prefixJsProperty(this._vendor, t);
            var e;
            for (var n in this._vendors)
                if (e = this._prefixJsProperty(this._vendors[n], t), this.propertySupported(e)) return this._vendor = this._vendors[n], e;
            return null
        },
        getCssValue: function (t, e) {
            var i = n.createElement("div"),
                r = this.getJsProperty(t);
            if (this._valueSupported(r, e, i)) return e;
            var o;
            if (this._vendor && (o = this._prefixValue(this._vendor, e), this._valueSupported(r, o, i))) return o;
            for (var a in this._vendors)
                if (o = this._prefixValue(this._vendors[a], e), this._valueSupported(r, o, i)) return this._vendor = this._vendors[a], o;
            return null
        }
    };
    var c, d, h = new i,
        f = h.getJsProperty("transform"),
        p = h.getCssValue("position", "sticky"),
        m = h.getCssValue("position", "fixed"),
        g = "Microsoft Internet Explorer" === navigator.appName;
    g && (d = parseFloat(navigator.appVersion.split("MSIE")[1])), o.prototype = {
        _mindtop: function () {
            var t = 0;
            if (this._$mind)
                for (var e, n, i = 0, r = this._$mind.length; r > i; i++)
                    if (e = this._$mind[i], n = e.getBoundingClientRect(), n.height) t += n.height;
                    else {
                        var o = l.getAll(e);
                        t += e.offsetHeight + l.toFloat(o.marginTop) + l.toFloat(o.marginBottom)
                    }
            return t
        },
        stop: function () {
            this._stop(), this._running = !1
        },
        start: function () {
            this._running || (this._start(), this._running = !0)
        },
        destroy: function () {
            this.stop(), this._destroy(), this._$child.removeData("fixto-instance");
            for (var t in this) this.hasOwnProperty(t) && (this[t] = null)
        },
        _setOptions: function (e) {
            t.extend(this.options, e), this.options.mind && (this._$mind = t(this.options.mind)), this.options.zIndex && (this.child.style.zIndex = this.options.zIndex)
        },
        setOptions: function (t) {
            this._setOptions(t), this.refresh()
        },
        _stop: function () { },
        _start: function () { },
        _destroy: function () { },
        refresh: function () { }
    }, a.prototype = new o, t.extend(a.prototype, {
        _bind: function (t, e) {
            return function () {
                return t.call(e)
            }
        },
        _toresize: 8 === d ? n.documentElement : e,
        _onscroll: function () {
            if (this._scrollTop = n.documentElement.scrollTop || n.body.scrollTop, this._parentBottom = this.parent.offsetHeight + this._fullOffset("offsetTop", this.parent), this.options.mindBottomPadding !== !1 && (this._parentBottom -= l.getFloat(this.parent, "paddingBottom")), this.fixed) {
                if (this._scrollTop > this._parentBottom || this._scrollTop < this._fullOffset("offsetTop", this._ghostNode) - this.options.top - this._mindtop()) return void this._unfix();
                this._adjust()
            } else {
                var t = l.getAll(this.child);
                this._scrollTop < this._parentBottom && this._scrollTop > this._fullOffset("offsetTop", this.child) - this.options.top - this._mindtop() && this._viewportHeight > this.child.offsetHeight + l.toFloat(t.marginTop) + l.toFloat(t.marginBottom) && (this._fix(), this._adjust())
            }
        },
        _adjust: function () {
            var t = 0,
                e = this._mindtop(),
                n = 0,
                i = l.getAll(this.child),
                r = null;
            c && (r = this._getContext(), r && (t = Math.abs(r.getBoundingClientRect().top))), n = this._parentBottom - this._scrollTop - (this.child.offsetHeight + l.toFloat(i.marginBottom) + e + this.options.top), n > 0 && (n = 0), this.child.style.top = n + e + t + this.options.top - l.toFloat(i.marginTop) + "px"
        },
        _fullOffset: function (t, e, n) {
            for (var i = e[t], r = e.offsetParent; null !== r && r !== n;) i += r[t], r = r.offsetParent;
            return i
        },
        _getContext: function () {
            for (var t, e, i = this.child, r = null; !r;) {
                if (t = i.parentNode, t === n.documentElement) return null;
                if (e = l.getAll(t), "none" !== e[f]) {
                    r = t;
                    break
                }
                i = t
            }
            return r
        },
        _fix: function () {
            var t = this.child,
                e = t.style,
                i = l.getAll(t),
                r = t.getBoundingClientRect().left,
                o = i.width;
            if (this._saveStyles(), n.documentElement.currentStyle && (o = t.offsetWidth - (l.toFloat(i.paddingLeft) + l.toFloat(i.paddingRight) + l.toFloat(i.borderLeftWidth) + l.toFloat(i.borderRightWidth)) + "px"), c) {
                var a = this._getContext();
                a && (r = t.getBoundingClientRect().left - a.getBoundingClientRect().left)
            }
            this._replacer.replace(), e.left = r - l.toFloat(i.marginLeft) + "px", e.width = o, e.position = "fixed", e.top = this._mindtop() + this.options.top - l.toFloat(i.marginTop) + "px", this._$child.addClass(this.options.className), this.fixed = !0
        },
        _unfix: function () {
            var t = this.child.style;
            this._replacer.hide(), t.position = this._childOriginalPosition, t.top = this._childOriginalTop, t.width = this._childOriginalWidth, t.left = this._childOriginalLeft, this._$child.removeClass(this.options.className), this.fixed = !1
        },
        _saveStyles: function () {
            var t = this.child.style;
            this._childOriginalPosition = t.position, this._childOriginalTop = t.top, this._childOriginalWidth = t.width, this._childOriginalLeft = t.left
        },
        _onresize: function () {
            this.refresh()
        },
        _saveViewportHeight: function () {
            this._viewportHeight = e.innerHeight || n.documentElement.clientHeight
        },
        _stop: function () {
            this._unfix(), t(e).unbind("scroll", this._proxied_onscroll), t(this._toresize).unbind("resize", this._proxied_onresize)
        },
        _start: function () {
            this._onscroll(), t(e).bind("scroll", this._proxied_onscroll), t(this._toresize).bind("resize", this._proxied_onresize)
        },
        _destroy: function () {
            this._replacer.destroy()
        },
        refresh: function () {
            this._saveViewportHeight(), this._unfix(), this._onscroll()
        }
    }), s.prototype = new o, t.extend(s.prototype, {
        _start: function () {
            var t = l.getAll(this.child);
            this._childOriginalPosition = t.position, this._childOriginalTop = t.top, this.child.style.position = p, this.refresh()
        },
        _stop: function () {
            this.child.style.position = this._childOriginalPosition, this.child.style.top = this._childOriginalTop
        },
        refresh: function () {
            this.child.style.top = this._mindtop() + this.options.top + "px"
        }
    });
    var v = function (t, e, n) {
        return p && !n || p && n && n.useNativeSticky !== !1 ? new s(t, e, n) : m ? (void 0 === c && (c = r()), new a(t, e, n)) : "Neither fixed nor sticky positioning supported"
    };
    return 8 > d && (v = function () {
        return "not supported"
    }), t.fn.fixTo = function (e, n) {
        var i = t(e),
            r = 0;
        return this.each(function () {
            var o = t(this).data("fixto-instance");
            if (o) {
                var a = e;
                o[a].call(o, n)
            } else t(this).data("fixto-instance", v(this, i[r], n));
            r++
        })
    }, {
            FixToContainer: a,
            fixTo: v,
            computedStyle: l,
            mimicNode: u
        }
} (window.jQuery, window, document);
! function (t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : t("object" == typeof exports ? require("jquery") : jQuery)
} (function (t) {
    "use strict";

    function e() {
        var t = document.createElement("input");
        return t.setAttribute("type", "range"), "text" !== t.type
    }

    function n(t, e) {
        var n = Array.prototype.slice.call(arguments, 2);
        return setTimeout(function () {
            return t.apply(null, n)
        }, e)
    }

    function i(t, e) {
        return e = e || 100,
            function () {
                if (!t.debouncing) {
                    var n = Array.prototype.slice.apply(arguments);
                    t.lastReturnVal = t.apply(window, n), t.debouncing = !0
                }
                return clearTimeout(t.debounceTimeout), t.debounceTimeout = setTimeout(function () {
                    t.debouncing = !1
                }, e), t.lastReturnVal
            }
    }

    function r(t) {
        return 0 !== t.offsetWidth || 0 !== t.offsetHeight ? !1 : !0
    }

    function o(t) {
        for (var e = [], n = t.parentNode; r(n);) e.push(n), n = n.parentNode;
        return e
    }

    function a(t, e) {
        var n = o(t),
            i = n.length,
            r = [],
            a = t[e];
        if (i) {
            for (var s = 0; i > s; s++) r[s] = n[s].style.display, n[s].style.display = "block", n[s].style.height = "0", n[s].style.overflow = "hidden", n[s].style.visibility = "hidden";
            a = t[e];
            for (var l = 0; i > l; l++) n[l].style.display = r[l], n[l].style.height = "", n[l].style.overflow = "", n[l].style.visibility = ""
        }
        return a
    }

    function s(e, r) {
        if (this.$window = t(window), this.$document = t(document), this.$element = t(e), this.options = t.extend({}, h, r), this._defaults = h, this._name = l, this.startEvent = this.options.startEvent.join("." + l + " ") + "." + l, this.moveEvent = this.options.moveEvent.join("." + l + " ") + "." + l, this.endEvent = this.options.endEvent.join("." + l + " ") + "." + l, this.polyfill = this.options.polyfill, this.onInit = this.options.onInit, this.onSlide = this.options.onSlide, this.onSlideEnd = this.options.onSlideEnd, this.polyfill && d) return !1;
        this.identifier = "js-" + l + "-" + c++ , this.min = parseFloat(this.$element[0].getAttribute("min") || 0), this.max = parseFloat(this.$element[0].getAttribute("max") || 100), this.value = parseFloat(this.$element[0].value || this.min + (this.max - this.min) / 2), this.step = parseFloat(this.$element[0].getAttribute("step") || 1), this.toFixed = (this.step + "").replace(".", "").length - 1, this.$fill = t('<div class="' + this.options.fillClass + '" />'), this.$handle = t('<div class="' + this.options.handleClass + '" />'), this.$range = t('<div class="' + this.options.rangeClass + '" id="' + this.identifier + '" />').insertAfter(this.$element).prepend(this.$fill, this.$handle), this.$element.css({
            position: "absolute",
            width: "1px",
            height: "1px",
            overflow: "hidden",
            opacity: "0"
        }), this.handleDown = t.proxy(this.handleDown, this), this.handleMove = t.proxy(this.handleMove, this), this.handleEnd = t.proxy(this.handleEnd, this), this.init();
        var o = this;
        this.$window.on("resize." + l, i(function () {
            n(function () {
                o.update()
            }, 300)
        }, 20)), this.$document.on(this.startEvent, "#" + this.identifier + ":not(." + this.options.disabledClass + ")", this.handleDown), this.$element.on("change." + l, function (t, e) {
            if (!e || e.origin !== l) {
                var n = t.target.value,
                    i = o.getPositionFromValue(n);
                o.setPosition(i)
            }
        })
    }
    var l = "rangeslider",
        u = [],
        c = 0,
        d = e(),
        h = {
            polyfill: !0,
            rangeClass: "rangeslider",
            disabledClass: "rangeslider--disabled",
            fillClass: "rangeslider__fill",
            handleClass: "rangeslider__handle",
            startEvent: ["mousedown", "touchstart", "pointerdown"],
            moveEvent: ["mousemove", "touchmove", "pointermove"],
            endEvent: ["mouseup", "touchend", "pointerup"]
        };
    s.prototype.init = function () {
        this.onInit && "function" == typeof this.onInit && this.onInit(), this.update()
    }, s.prototype.update = function () {
        this.handleWidth = a(this.$handle[0], "offsetWidth"), this.rangeWidth = a(this.$range[0], "offsetWidth"), this.maxHandleX = this.rangeWidth - this.handleWidth, this.grabX = this.handleWidth / 2, this.position = this.getPositionFromValue(this.value), this.$element[0].disabled ? this.$range.addClass(this.options.disabledClass) : this.$range.removeClass(this.options.disabledClass), this.setPosition(this.position)
    }, s.prototype.handleDown = function (t) {
        if (t.preventDefault(), this.$document.on(this.moveEvent, this.handleMove), this.$document.on(this.endEvent, this.handleEnd), !((" " + t.target.className + " ").replace(/[\n\t]/g, " ").indexOf(this.options.handleClass) > -1)) {
            var e = this.getRelativePosition(t),
                n = this.$range[0].getBoundingClientRect().left,
                i = this.getPositionFromNode(this.$handle[0]) - n;
            this.setPosition(e - this.grabX), e >= i && e < i + this.handleWidth && (this.grabX = e - i)
        }
    }, s.prototype.handleMove = function (t) {
        t.preventDefault();
        var e = this.getRelativePosition(t);
        this.setPosition(e - this.grabX)
    }, s.prototype.handleEnd = function (t) {
        t.preventDefault(), this.$document.off(this.moveEvent, this.handleMove), this.$document.off(this.endEvent, this.handleEnd), this.onSlideEnd && "function" == typeof this.onSlideEnd && this.onSlideEnd(this.position, this.value)
    }, s.prototype.cap = function (t, e, n) {
        return e > t ? e : t > n ? n : t
    }, s.prototype.setPosition = function (t) {
        var e, n;
        e = this.getValueFromPosition(this.cap(t, 0, this.maxHandleX)), n = this.getPositionFromValue(e), this.$fill[0].style.width = n + this.grabX + "px", this.$handle[0].style.left = n + "px", this.setValue(e), this.position = n, this.value = e, this.onSlide && "function" == typeof this.onSlide && this.onSlide(n, e)
    }, s.prototype.getPositionFromNode = function (t) {
        for (var e = 0; null !== t;) e += t.offsetLeft, t = t.offsetParent;
        return e
    }, s.prototype.getRelativePosition = function (t) {
        var e = this.$range[0].getBoundingClientRect().left,
            n = 0;
        return "undefined" != typeof t.pageX ? n = t.pageX : "undefined" != typeof t.originalEvent.clientX ? n = t.originalEvent.clientX : t.originalEvent.touches && t.originalEvent.touches[0] && "undefined" != typeof t.originalEvent.touches[0].clientX ? n = t.originalEvent.touches[0].clientX : t.currentPoint && "undefined" != typeof t.currentPoint.x && (n = t.currentPoint.x), n - e
    }, s.prototype.getPositionFromValue = function (t) {
        var e, n;
        return e = (t - this.min) / (this.max - this.min), n = e * this.maxHandleX
    }, s.prototype.getValueFromPosition = function (t) {
        var e, n;
        return e = t / (this.maxHandleX || 1), n = this.step * Math.round(e * (this.max - this.min) / this.step) + this.min, Number(n.toFixed(this.toFixed))
    }, s.prototype.setValue = function (t) {
        t !== this.value && this.$element.val(t).trigger("change", {
            origin: l
        })
    }, s.prototype.destroy = function () {
        this.$document.off(this.startEvent, "#" + this.identifier, this.handleDown), this.$element.off("." + l).removeAttr("style").removeData("plugin_" + l), this.$range && this.$range.length && this.$range[0].parentNode.removeChild(this.$range[0]), u.splice(u.indexOf(this.$element[0]), 1), u.length || this.$window.off("." + l)
    }, t.fn[l] = function (e) {
        return this.each(function () {
            var n = t(this),
                i = n.data("plugin_" + l);
            i || (n.data("plugin_" + l, i = new s(this, e)), u.push(this)), "string" == typeof e && i[e]()
        })
    }
}),
    function (t) {
        var e = t(window);
        t.fn.visible = function (t, n, i) {
            if (!(this.length < 1)) {
                var r = this.length > 1 ? this.eq(0) : this,
                    o = r.get(0),
                    a = e.width(),
                    s = e.height(),
                    i = i ? i : "both",
                    l = n === !0 ? o.offsetWidth * o.offsetHeight : !0;
                if ("function" == typeof o.getBoundingClientRect) {
                    var u = o.getBoundingClientRect(),
                        c = u.top >= 0 && u.top < s,
                        d = u.bottom > 0 && u.bottom <= s,
                        h = u.left >= 0 && u.left < a,
                        f = u.right > 0 && u.right <= a,
                        p = t ? c || d : c && d,
                        m = t ? h || f : h && f;
                    if ("both" === i) return l && p && m;
                    if ("vertical" === i) return l && p;
                    if ("horizontal" === i) return l && m
                } else {
                    var g = e.scrollTop(),
                        v = g + s,
                        y = e.scrollLeft(),
                        _ = y + a,
                        w = r.offset(),
                        b = w.top,
                        x = b + r.height(),
                        k = w.left,
                        C = k + r.width(),
                        S = t === !0 ? x : b,
                        T = t === !0 ? b : x,
                        D = t === !0 ? C : k,
                        $ = t === !0 ? k : C;
                    if ("both" === i) return !!l && v >= T && S >= g && _ >= $ && D >= y;
                    if ("vertical" === i) return !!l && v >= T && S >= g;
                    if ("horizontal" === i) return !!l && _ >= $ && D >= y
                }
            }
        }
    } (jQuery),
    function (t, e, n) {
        function i(e, n) {
            this.element = t(e), this.options = t.extend({}, o, n), this._defaults = o, this._name = r, this.init()
        }
        var r = "recital",
            o = {
                tocClass: ".recital-toc",
                currentBookmark: -1,
                isPlaying: !1
            };
        i.prototype.init = function () {
            this.player = $f(this.element.find("iframe")[0]), this.toc = this.element.find(this.options.tocClass + " a");
            var n = this;
            this.element.on("show", function () {
                n.isPlaying = !0, n.player.api("play")
            }), this.element.on("hide", function () {
                n.isPlaying = !1, n.player.api("pause")
            }), t(e).on("scroll", function () {
                var t = n.element.find("iframe").visible();
                if ((t || n.isPlaying) && (!t || !n.isPlaying)) return !t && n.isPlaying ? (n.isPlaying = !1, void n.player.api("pause")) : void (t && !n.isPlaying && (n.isPlaying = !0, n.player.api("play")))
            }), this.player.addEvent("ready", function () {
                n.player.addEvent("playProgress", n.onPlayProgress.bind(n)), n.player.addEvent("finish", n.clearCurrent.bind(n)), n.toc.on("click", n.onClickBookmark.bind(n))
            }), -1 === this.options.currentBookmark ? this.setCurrent(this.toc[0]) : this.setCurrent(this.toc[this.options.currentBookmark])
        }, i.prototype.onClickBookmark = function (e) {
            var n = t(e.target).closest("a").data("seek-to");
            this.player.api("seekTo", n), this.setCurrent(e.target), e.preventDefault()
        }, i.prototype.onPlayProgress = function (e, n) {
            var i = this.options.currentBookmark,
                r = this.toc.map(function (e, n) {
                    return +t(n).data("seek-to")
                }).toArray().reduce(function (n, i, r) {
                    return t.isNumeric(i) ? n = e.seconds < i ? n : r : n
                }, -1);
            i !== r && -1 !== r && (i = r, this.setCurrent(this.toc[i]))
        }, i.prototype.setCurrent = function (e) {
            t(e).parents("li").hasClass("recital-current") || (this.clearCurrent(), t(e).parents("li").addClass("recital-current"))
        }, i.prototype.clearCurrent = function () {
            this.toc.each(function (e, n) {
                t(n).parents("li").hasClass("recital-current") && t(n).parents("li").removeClass("recital-current")
            })
        }, t.fn[r] = function (e) {
            return this.each(function () {
                t.data(this, "plugin_" + r) || t.data(this, "plugin_" + r, new i(this, e))
            })
        }
    } (jQuery, window, document);
var Froogaloop = function () {
    function t(e) {
        return new t.fn.init(e)
    }

    function e(t, e, n) {
        if (!n.contentWindow.postMessage) return !1;
        var i = JSON.stringify({
            method: t,
            value: e
        });
        n.contentWindow.postMessage(i, u);
    }

    function n(t) {
        var e, n;
        try {
            e = JSON.parse(t.data), n = e.event || e.method
        } catch (i) { }
        if ("ready" != n || l || (l = !0), !/^https?:\/\/player.vimeo.com/.test(t.origin)) return !1;
        "*" === u && (u = t.origin);
        var o = e.value,
            a = e.data,
            s = "" === s ? null : e.player_id,
            c = r(n, s),
            d = [];
        return c ? (void 0 !== o && d.push(o), a && d.push(a), s && d.push(s), d.length > 0 ? c.apply(null, d) : c.call()) : !1
    }

    function i(t, e, n) {
        n ? (s[n] || (s[n] = {}), s[n][t] = e) : s[t] = e
    }

    function r(t, e) {
        return e ? s[e] ? s[e][t] : void 0 : s[t]
    }

    function o(t, e) {
        if (e && s[e]) {
            if (!s[e][t]) return !1;
            s[e][t] = null
        } else {
            if (!s[t]) return !1;
            s[t] = null
        }
        return !0
    }

    function a(t) {
        return !!(t && t.constructor && t.call && t.apply)
    }
    var s = {},
        l = !1,
        u = (Array.prototype.slice, "*");
    return t.fn = t.prototype = {
        element: null,
        init: function (t) {
            return "string" == typeof t && (t = document.getElementById(t)), this.element = t, this
        },
        api: function (t, n) {
            if (!this.element || !t) return !1;
            var r = this,
                o = r.element,
                s = "" !== o.id ? o.id : null,
                l = a(n) ? null : n,
                u = a(n) ? n : null;
            return u && i(t, u, s), e(t, l, o), r
        },
        addEvent: function (t, n) {
            if (!this.element) return !1;
            var r = this,
                o = r.element,
                a = "" !== o.id ? o.id : null;
            return i(t, n, a), "ready" != t ? e("addEventListener", t, o) : "ready" == t && l && n.call(null, a), r
        },
        removeEvent: function (t) {
            if (!this.element) return !1;
            var n = this,
                i = n.element,
                r = "" !== i.id ? i.id : null,
                a = o(t, r);
            "ready" != t && a && e("removeEventListener", t, i)
        }
    }, t.fn.init.prototype = t.fn, window.addEventListener ? window.addEventListener("message", n, !1) : window.attachEvent("onmessage", n), window.Froogaloop = window.$f = t
} ();
! function (t) {
    function e(e, n, i, r) {
        var o = e.text(),
            a = o.split(n),
            s = "";
        a.length && (t(a).each(function (t, e) {
            s += '<span class="' + i + (t + 1) + '" aria-hidden="true">' + e + "</span>" + r
        }), e.attr("aria-label", o).empty().append(s))
    }
    var n = {
        init: function () {
            return this.each(function () {
                e(t(this), "", "char", "")
            })
        },
        words: function () {
            return this.each(function () {
                e(t(this), " ", "word", " ")
            })
        },
        lines: function () {
            return this.each(function () {
                var n = "eefec303079ad17405c889e092e105b0";
                e(t(this).children("br").replaceWith(n).end(), n, "line", "")
            })
        }
    };
    t.fn.lettering = function (e) {
        return e && n[e] ? n[e].apply(this, [].slice.call(arguments, 1)) : "letters" !== e && e ? (t.error("Method " + e + " does not exist on jQuery.lettering"), this) : n.init.apply(this, [].slice.call(arguments, 0))
    }
} (jQuery),
    function (t) {
        "use strict";

        function e(e) {
            return /In/.test(e) || t.inArray(e, t.fn.textillate.defaults.inEffects) >= 0
        }

        function n(e) {
            return /Out/.test(e) || t.inArray(e, t.fn.textillate.defaults.outEffects) >= 0
        }

        function i(t) {
            return "true" !== t && "false" !== t ? t : "true" === t
        }

        function r(e) {
            var n = e.attributes || [],
                r = {};
            return n.length ? (t.each(n, function (t, e) {
                var n = e.nodeName.replace(/delayscale/, "delayScale");
                /^data-in-*/.test(n) ? (r["in"] = r["in"] || {}, r["in"][n.replace(/data-in-/, "")] = i(e.nodeValue)) : /^data-out-*/.test(n) ? (r.out = r.out || {}, r.out[n.replace(/data-out-/, "")] = i(e.nodeValue)) : /^data-*/.test(n) && (r[n.replace(/data-/, "")] = i(e.nodeValue))
            }), r) : r
        }

        function o(t) {
            for (var e, n, i = t.length; i; e = parseInt(Math.random() * i), n = t[--i], t[i] = t[e], t[e] = n);
            return t
        }

        function a(t, e, n) {
            t.addClass("animated " + e).css("visibility", "visible").show(), t.one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
                t.removeClass("animated " + e), n && n()
            })
        }

        function s(i, r, s) {
            var l = i.length;
            return l ? (r.shuffle && (i = o(i)), r.reverse && (i = i.toArray().reverse()), void t.each(i, function (i, o) {
                function u() {
                    e(r.effect) ? c.css("visibility", "visible") : n(r.effect) && c.css("visibility", "hidden"), l -= 1, !l && s && s()
                }
                var c = t(o),
                    d = r.sync ? r.delay : r.delay * i * r.delayScale;
                c.text() ? setTimeout(function () {
                    a(c, r.effect, u)
                }, d) : u()
            })) : void (s && s())
        }
        var l = function (i, o) {
            var a = this,
                l = t(i);
            a.init = function () {
                a.$texts = l.find(o.selector), a.$texts.length || (a.$texts = t('<ul class="texts">' + l.html() + "</ul>"), l.html(a.$texts)), a.$texts.hide(), a.$current = t("<span>").html(a.$texts.find(":first-child").html()).prependTo(l), e(o["in"].effect) ? a.$current.css("visibility", "hidden") : n(o.out.effect) && a.$current.css("visibility", "visible"), a.setOptions(o), a.timeoutRun = null, setTimeout(function () {
                    a.options.autoStart && a.start()
                }, a.options.initialDelay)
            }, a.setOptions = function (t) {
                a.options = t
            }, a.triggerEvent = function (e) {
                var n = t.Event(e + ".tlt");
                return l.trigger(n, a), n
            }, a["in"] = function (i, o) {
                i = i || 0;
                var l, u = a.$texts.find(":nth-child(" + ((i || 0) + 1) + ")"),
                    c = t.extend(!0, {}, a.options, u.length ? r(u[0]) : {});
                u.addClass("current"), a.triggerEvent("inAnimationBegin"), a.$current.html(u.html()).lettering("words"), "char" == a.options.type && a.$current.find('[class^="word"]').css({
                    display: "inline-block",
                    "-webkit-transform": "translate3d(0,0,0)",
                    "-moz-transform": "translate3d(0,0,0)",
                    "-o-transform": "translate3d(0,0,0)",
                    transform: "translate3d(0,0,0)"
                }).each(function () {
                    t(this).lettering()
                }), l = a.$current.find('[class^="' + a.options.type + '"]').css("display", "inline-block"), e(c["in"].effect) ? l.css("visibility", "hidden") : n(c["in"].effect) && l.css("visibility", "visible"), a.currentIndex = i, s(l, c["in"], function () {
                    a.triggerEvent("inAnimationEnd"), c["in"].callback && c["in"].callback(), o && o(a)
                })
            }, a.out = function (e) {
                var n = a.$texts.find(":nth-child(" + ((a.currentIndex || 0) + 1) + ")"),
                    i = a.$current.find('[class^="' + a.options.type + '"]'),
                    o = t.extend(!0, {}, a.options, n.length ? r(n[0]) : {});
                a.triggerEvent("outAnimationBegin"), s(i, o.out, function () {
                    n.removeClass("current"), a.triggerEvent("outAnimationEnd"), o.out.callback && o.out.callback(), e && e(a)
                })
            }, a.start = function (t) {
                setTimeout(function () {
                    a.triggerEvent("start"),
                        function e(t) {
                            a["in"](t, function () {
                                var n = a.$texts.children().length;
                                t += 1, !a.options.loop && t >= n ? (a.options.callback && a.options.callback(), a.triggerEvent("end")) : (t %= n, a.timeoutRun = setTimeout(function () {
                                    a.out(function () {
                                        e(t)
                                    })
                                }, a.options.minDisplayTime))
                            })
                        } (t || 0)
                }, a.options.initialDelay)
            }, a.stop = function () {
                a.timeoutRun && (clearInterval(a.timeoutRun), a.timeoutRun = null)
            }, a.init()
        };
        t.fn.textillate = function (e, n) {
            return this.each(function () {
                var i = t(this),
                    o = i.data("textillate"),
                    a = t.extend(!0, {}, t.fn.textillate.defaults, r(this), "object" == typeof e && e);
                o ? "string" == typeof e ? o[e].apply(o, [].concat(n)) : o.setOptions.call(o, a) : i.data("textillate", o = new l(this, a))
            })
        }, t.fn.textillate.defaults = {
            selector: ".texts",
            loop: !1,
            minDisplayTime: 2e3,
            initialDelay: 0,
            "in": {
                effect: "fadeIn",
                delayScale: 1.5,
                delay: 50,
                sync: !1,
                reverse: !1,
                shuffle: !1,
                callback: function () { }
            },
            out: {
                effect: "fadeOut",
                delayScale: 1.5,
                delay: 50,
                sync: !1,
                reverse: !1,
                shuffle: !1,
                callback: function () { }
            },
            autoStart: !0,
            inEffects: [],
            outEffects: ["fadeOut"],
            callback: function () { },
            type: "char"
        }
    } (jQuery),
    function () {
        "use strict";

        function t(i) {
            if (!i) throw new Error("No options passed to Waypoint constructor");
            if (!i.element) throw new Error("No element option passed to Waypoint constructor");
            if (!i.handler) throw new Error("No handler option passed to Waypoint constructor");
            this.key = "waypoint-" + e, this.options = t.Adapter.extend({}, t.defaults, i), this.element = this.options.element, this.adapter = new t.Adapter(this.element), this.callback = i.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = t.Group.findOrCreate({
                name: this.options.group,
                axis: this.axis
            }), this.context = t.Context.findOrCreateByElement(this.options.context), t.offsetAliases[this.options.offset] && (this.options.offset = t.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), n[this.key] = this, e += 1
        }
        var e = 0,
            n = {};
        t.prototype.queueTrigger = function (t) {
            this.group.queueTrigger(this, t)
        }, t.prototype.trigger = function (t) {
            this.enabled && this.callback && this.callback.apply(this, t)
        }, t.prototype.destroy = function () {
            this.context.remove(this), this.group.remove(this), delete n[this.key]
        }, t.prototype.disable = function () {
            return this.enabled = !1, this
        }, t.prototype.enable = function () {
            return this.context.refresh(), this.enabled = !0, this
        }, t.prototype.next = function () {
            return this.group.next(this)
        }, t.prototype.previous = function () {
            return this.group.previous(this)
        }, t.invokeAll = function (t) {
            var e = [];
            for (var i in n) e.push(n[i]);
            for (var r = 0, o = e.length; o > r; r++) e[r][t]()
        }, t.destroyAll = function () {
            t.invokeAll("destroy")
        }, t.disableAll = function () {
            t.invokeAll("disable")
        }, t.enableAll = function () {
            t.invokeAll("enable")
        }, t.refreshAll = function () {
            t.Context.refreshAll()
        }, t.viewportHeight = function () {
            return window.innerHeight || document.documentElement.clientHeight
        }, t.viewportWidth = function () {
            return document.documentElement.clientWidth
        }, t.adapters = [], t.defaults = {
            context: window,
            continuous: !0,
            enabled: !0,
            group: "default",
            horizontal: !1,
            offset: 0
        }, t.offsetAliases = {
            "bottom-in-view": function () {
                return this.context.innerHeight() - this.adapter.outerHeight()
            },
            "right-in-view": function () {
                return this.context.innerWidth() - this.adapter.outerWidth()
            }
        }, window.Waypoint = t
    } (),
    function () {
        "use strict";

        function t(t) {
            window.setTimeout(t, 1e3 / 60)
        }

        function e(t) {
            this.element = t, this.Adapter = r.Adapter, this.adapter = new this.Adapter(t), this.key = "waypoint-context-" + n, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
                x: this.adapter.scrollLeft(),
                y: this.adapter.scrollTop()
            }, this.waypoints = {
                vertical: {},
                horizontal: {}
            }, t.waypointContextKey = this.key, i[t.waypointContextKey] = this, n += 1, this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
        }
        var n = 0,
            i = {},
            r = window.Waypoint,
            o = window.onload;
        e.prototype.add = function (t) {
            var e = t.options.horizontal ? "horizontal" : "vertical";
            this.waypoints[e][t.key] = t, this.refresh()
        }, e.prototype.checkEmpty = function () {
            var t = this.Adapter.isEmptyObject(this.waypoints.horizontal),
                e = this.Adapter.isEmptyObject(this.waypoints.vertical);
            t && e && (this.adapter.off(".waypoints"), delete i[this.key])
        }, e.prototype.createThrottledResizeHandler = function () {
            function t() {
                e.handleResize(), e.didResize = !1
            }
            var e = this;
            this.adapter.on("resize.waypoints", function () {
                e.didResize || (e.didResize = !0, r.requestAnimationFrame(t))
            })
        }, e.prototype.createThrottledScrollHandler = function () {
            function t() {
                e.handleScroll(), e.didScroll = !1
            }
            var e = this;
            this.adapter.on("scroll.waypoints", function () {
                (!e.didScroll || r.isTouch) && (e.didScroll = !0, r.requestAnimationFrame(t))
            })
        }, e.prototype.handleResize = function () {
            r.Context.refreshAll()
        }, e.prototype.handleScroll = function () {
            var t = {},
                e = {
                    horizontal: {
                        newScroll: this.adapter.scrollLeft(),
                        oldScroll: this.oldScroll.x,
                        forward: "right",
                        backward: "left"
                    },
                    vertical: {
                        newScroll: this.adapter.scrollTop(),
                        oldScroll: this.oldScroll.y,
                        forward: "down",
                        backward: "up"
                    }
                };
            for (var n in e) {
                var i = e[n],
                    r = i.newScroll > i.oldScroll,
                    o = r ? i.forward : i.backward;
                for (var a in this.waypoints[n]) {
                    var s = this.waypoints[n][a],
                        l = i.oldScroll < s.triggerPoint,
                        u = i.newScroll >= s.triggerPoint,
                        c = l && u,
                        d = !l && !u;
                    (c || d) && (s.queueTrigger(o), t[s.group.id] = s.group)
                }
            }
            for (var h in t) t[h].flushTriggers();
            this.oldScroll = {
                x: e.horizontal.newScroll,
                y: e.vertical.newScroll
            }
        }, e.prototype.innerHeight = function () {
            return this.element == this.element.window ? r.viewportHeight() : this.adapter.innerHeight()
        }, e.prototype.remove = function (t) {
            delete this.waypoints[t.axis][t.key], this.checkEmpty()
        }, e.prototype.innerWidth = function () {
            return this.element == this.element.window ? r.viewportWidth() : this.adapter.innerWidth()
        }, e.prototype.destroy = function () {
            var t = [];
            for (var e in this.waypoints)
                for (var n in this.waypoints[e]) t.push(this.waypoints[e][n]);
            for (var i = 0, r = t.length; r > i; i++) t[i].destroy()
        }, e.prototype.refresh = function () {
            var t, e = this.element == this.element.window,
                n = e ? void 0 : this.adapter.offset(),
                i = {};
            this.handleScroll(), t = {
                horizontal: {
                    contextOffset: e ? 0 : n.left,
                    contextScroll: e ? 0 : this.oldScroll.x,
                    contextDimension: this.innerWidth(),
                    oldScroll: this.oldScroll.x,
                    forward: "right",
                    backward: "left",
                    offsetProp: "left"
                },
                vertical: {
                    contextOffset: e ? 0 : n.top,
                    contextScroll: e ? 0 : this.oldScroll.y,
                    contextDimension: this.innerHeight(),
                    oldScroll: this.oldScroll.y,
                    forward: "down",
                    backward: "up",
                    offsetProp: "top"
                }
            };
            for (var o in t) {
                var a = t[o];
                for (var s in this.waypoints[o]) {
                    var l, u, c, d, h, f = this.waypoints[o][s],
                        p = f.options.offset,
                        m = f.triggerPoint,
                        g = 0,
                        v = null == m;
                    f.element !== f.element.window && (g = f.adapter.offset()[a.offsetProp]), "function" == typeof p ? p = p.apply(f) : "string" == typeof p && (p = parseFloat(p), f.options.offset.indexOf("%") > -1 && (p = Math.ceil(a.contextDimension * p / 100))), l = a.contextScroll - a.contextOffset, f.triggerPoint = g + l - p, u = m < a.oldScroll, c = f.triggerPoint >= a.oldScroll, d = u && c, h = !u && !c, !v && d ? (f.queueTrigger(a.backward), i[f.group.id] = f.group) : !v && h ? (f.queueTrigger(a.forward), i[f.group.id] = f.group) : v && a.oldScroll >= f.triggerPoint && (f.queueTrigger(a.forward), i[f.group.id] = f.group)
                }
            }
            return r.requestAnimationFrame(function () {
                for (var t in i) i[t].flushTriggers()
            }), this
        }, e.findOrCreateByElement = function (t) {
            return e.findByElement(t) || new e(t)
        }, e.refreshAll = function () {
            for (var t in i) i[t].refresh()
        }, e.findByElement = function (t) {
            return i[t.waypointContextKey]
        }, window.onload = function () {
            o && o(), e.refreshAll()
        }, r.requestAnimationFrame = function (e) {
            var n = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || t;
            n.call(window, e)
        }, r.Context = e
    } (),
    function () {
        "use strict";

        function t(t, e) {
            return t.triggerPoint - e.triggerPoint
        }

        function e(t, e) {
            return e.triggerPoint - t.triggerPoint
        }

        function n(t) {
            this.name = t.name, this.axis = t.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), i[this.axis][this.name] = this
        }
        var i = {
            vertical: {},
            horizontal: {}
        },
            r = window.Waypoint;
        n.prototype.add = function (t) {
            this.waypoints.push(t)
        }, n.prototype.clearTriggerQueues = function () {
            this.triggerQueues = {
                up: [],
                down: [],
                left: [],
                right: []
            }
        }, n.prototype.flushTriggers = function () {
            for (var n in this.triggerQueues) {
                var i = this.triggerQueues[n],
                    r = "up" === n || "left" === n;
                i.sort(r ? e : t);
                for (var o = 0, a = i.length; a > o; o += 1) {
                    var s = i[o];
                    (s.options.continuous || o === i.length - 1) && s.trigger([n])
                }
            }
            this.clearTriggerQueues()
        }, n.prototype.next = function (e) {
            this.waypoints.sort(t);
            var n = r.Adapter.inArray(e, this.waypoints),
                i = n === this.waypoints.length - 1;
            return i ? null : this.waypoints[n + 1]
        }, n.prototype.previous = function (e) {
            this.waypoints.sort(t);
            var n = r.Adapter.inArray(e, this.waypoints);
            return n ? this.waypoints[n - 1] : null
        }, n.prototype.queueTrigger = function (t, e) {
            this.triggerQueues[e].push(t)
        }, n.prototype.remove = function (t) {
            var e = r.Adapter.inArray(t, this.waypoints);
            e > -1 && this.waypoints.splice(e, 1)
        }, n.prototype.first = function () {
            return this.waypoints[0]
        }, n.prototype.last = function () {
            return this.waypoints[this.waypoints.length - 1]
        }, n.findOrCreate = function (t) {
            return i[t.axis][t.name] || new n(t)
        }, r.Group = n
    } (),
    function () {
        "use strict";

        function t(t) {
            this.$element = e(t)
        }
        var e = window.jQuery,
            n = window.Waypoint;
        e.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function (e, n) {
            t.prototype[n] = function () {
                var t = Array.prototype.slice.call(arguments);
                return this.$element[n].apply(this.$element, t)
            }
        }), e.each(["extend", "inArray", "isEmptyObject"], function (n, i) {
            t[i] = e[i]
        }), n.adapters.push({
            name: "jquery",
            Adapter: t
        }), n.Adapter = t
    } (),
    function () {
        "use strict";

        function t(t) {
            return function () {
                var n = [],
                    i = arguments[0];
                return t.isFunction(arguments[0]) && (i = t.extend({}, arguments[1]), i.handler = arguments[0]), this.each(function () {
                    var r = t.extend({}, i, {
                        element: this
                    });
                    "string" == typeof r.context && (r.context = t(this).closest(r.context)[0]), n.push(new e(r))
                }), n
            }
        }
        var e = window.Waypoint;
        window.jQuery && (window.jQuery.fn.waypoint = t(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = t(window.Zepto))
    } (),
    function () {
        this.JST || (this.JST = {}), this.JST["templates/event"] = function (obj) {
            var __p = [];
            with (obj || {}) {
                __p.push('<a href="', attributes.redirect_url || "/events/" + attributes.start_year + "/" + attributes.slug, '" class="event-link ', attributes.is_past ? "event-past" : "event-upcoming", '">\n<div class="event-wrapper">\n  '), attributes.featured_image_url && __p.push('\n    <div class="featured-image">\n      <img src="', attributes.featured_image_url, '" alt="', attributes.title, '" />\n    </div>\n  '), __p.push('\n  <div class="featured-content">\n    <div class="event-details">\n      ', attributes.display_date, "\n      "), attributes.is_past ? __p.push('\n        <span class="day day-detail">', attributes.start_year, "</span>\n      ") : __p.push("\n        ", attributes.display_time, "\n      "), __p.push('\n    </div>\n\n    <div class="event-description">\n      <div class="event-head">\n        <div class="event-type-icon">\n          <img src="', attributes.icon_url, '" alt="', attributes.event_type_name, '"/>\n        </div>\n        <div class="event-title">\n          <h4>', attributes.title, '</h4>\n        </div>\n        <ul class="filter-tag-list">\n          '), attributes.heroku_speaker && __p.push('\n            <li class="heroku">Heroku presenter</li>\n          '), __p.push("\n          "), attributes.heroku_sponsored && __p.push('\n            <li class="heroku">Heroku sponsored</li>\n          '), __p.push("\n          ");
                for (var i = 0; i < attributes.tag_names.length; i++) __p.push("\n            <li>", attributes.tag_names[i], "</li>\n          ");
                __p.push('\n        </ul>\n      </div><!--/event-head-->\n      <div class="description-text">\n        <p>', attributes.short_description || attributes.description_html_safe, "</p>\n      </div>\n    </div>\n  </div><!--/featured-content-->\n</div><!--/event-wrapper-->\n</a>\n")
            }
            return __p.join("")
        }
    }.call(this),
    function () {
        this.JST || (this.JST = {}), this.JST["templates/no_events"] = function (obj) {
            var __p = [];
            with (obj || {}) __p.push('<div class="event-results-empty">\n  <div class="panel text-center">\n    <h3>No results found</h3>\n    <p>Try using the event filters to widen your search or <a href=\'/events\'>clear the current filters</a>.</p>\n  </div>\n</div>\n');
            return __p.join("")
        }
    }.call(this),
    function (t, e) {
        "function" == typeof define && define.amd ? define([], e) : "undefined" != typeof module && module.exports ? module.exports = e() : t.lscache = e()
    } (this, function () {
        function t() {
            var t = "__lscachetest__",
                n = t;
            if (void 0 !== f) return f;
            try {
                s(t, n), l(t), f = !0
            } catch (i) {
                f = e(i) ? !0 : !1
            }
            return f
        }

        function e(t) {
            return t && "QUOTA_EXCEEDED_ERR" === t.name || "NS_ERROR_DOM_QUOTA_REACHED" === t.name || "QuotaExceededError" === t.name ? !0 : !1
        }

        function n() {
            return void 0 === p && (p = null != window.JSON), p
        }

        function i(t) {
            return t.replace(/[[\]{}()*+?.\\^$|]/g, "\\$&")
        }

        function r(t) {
            return t + g
        }

        function o() {
            return Math.floor((new Date).getTime() / y)
        }

        function a(t) {
            return localStorage.getItem(m + w + t)
        }

        function s(t, e) {
            localStorage.removeItem(m + w + t), localStorage.setItem(m + w + t, e)
        }

        function l(t) {
            localStorage.removeItem(m + w + t)
        }

        function u(t) {
            for (var e = new RegExp("^" + m + i(w) + "(.*)"), n = localStorage.length - 1; n >= 0; --n) {
                var o = localStorage.key(n);
                o = o && o.match(e), o = o && o[1], o && o.indexOf(g) < 0 && t(o, r(o))
            }
        }

        function c(t) {
            var e = r(t);
            l(t), l(e)
        }

        function d(t) {
            var e = r(t),
                n = a(e);
            if (n) {
                var i = parseInt(n, v);
                if (o() >= i) return l(t), l(e), !0
            }
        }

        function h(t, e) {
            b && "console" in window && "function" == typeof window.console.warn && (window.console.warn("lscache - " + t), e && window.console.warn("lscache - The error was: " + e.message))
        }
        var f, p, m = "lscache-",
            g = "-cacheexpiration",
            v = 10,
            y = 6e4,
            _ = Math.floor(864e13 / y),
            w = "",
            b = !1,
            x = {
                set: function (i, d, f) {
                    if (t()) {
                        if ("string" != typeof d) {
                            if (!n()) return;
                            try {
                                d = JSON.stringify(d)
                            } catch (p) {
                                return
                            }
                        }
                        try {
                            s(i, d)
                        } catch (p) {
                            if (!e(p)) return void h("Could not add item with key '" + i + "'", p);
                            var m, g = [];
                            u(function (t, e) {
                                var n = a(e);
                                n = n ? parseInt(n, v) : _, g.push({
                                    key: t,
                                    size: (a(t) || "").length,
                                    expiration: n
                                })
                            }), g.sort(function (t, e) {
                                return e.expiration - t.expiration
                            });
                            for (var y = (d || "").length; g.length && y > 0;) m = g.pop(), h("Cache is full, removing item with key '" + i + "'"), c(m.key), y -= m.size;
                            try {
                                s(i, d)
                            } catch (p) {
                                return void h("Could not add item with key '" + i + "', perhaps it's too big?", p)
                            }
                        }
                        f ? s(r(i), (o() + f).toString(v)) : l(r(i))
                    }
                },
                get: function (e) {
                    if (!t()) return null;
                    if (d(e)) return null;
                    var i = a(e);
                    if (!i || !n()) return i;
                    try {
                        return JSON.parse(i)
                    } catch (r) {
                        return i
                    }
                },
                remove: function (e) {
                    t() && c(e)
                },
                supported: function () {
                    return t()
                },
                flush: function () {
                    t() && u(function (t) {
                        c(t)
                    })
                },
                flushExpired: function () {
                    t() && u(function (t) {
                        d(t)
                    })
                },
                setBucket: function (t) {
                    w = t
                },
                resetBucket: function () {
                    w = ""
                },
                enableWarnings: function (t) {
                    b = t
                }
            };
        return x
    });

var _createClass = function () {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }
    return function (e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
    }
} (),
    DELAY = 4e3;
Heroku.ToolTipCarousel = function () {
    function t(e) {
        var n = this;
        _classCallCheck(this, t), this.$el = $(e), this.lastItem = this.$el.length - 1, this.currentIndex = this.lastItem, this.$el.hover(function (t) {
            return n.pauseCarousel(t)
        }), this.startCarousel()
    }
    return _createClass(t, [{
        key: "startCarousel",
        value: function () {
            var t = this;
            this.changeActive(), this.interval = setInterval(function () {
                t.changeActive()
            }, DELAY)
        }
    }, {
        key: "changeActive",
        value: function () {
            var t = this.$el[this.currentIndex];
            this.setTheIndex(), $(t).removeClass("active"), $(this.$el[this.currentIndex]).addClass("active")
        }
    }, {
        key: "pauseCarousel",
        value: function (t) {
            $(this.$el[this.currentIndex]).removeClass("active"), this.currentIndex = $(t.currentTarget).data("index"), clearInterval(this.interval)
        }
    }, {
        key: "setTheIndex",
        value: function () {
            this.currentIndex === this.lastItem ? this.currentIndex = 0 : this.currentIndex++
        }
    }]), t
} ();
var _createClass = function () {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }
    return function (e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
    }
} ();
Heroku.Carousel = function () {
    function t(e) {
        var n = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1];
        _classCallCheck(this, t), this.$selector = $(e), this.options = {
            autoplay: null != n.autoplay ? n.autoplay : n.autoplay = !0,
            delay: n.delay || 5e3,
            fluid: null != n.fluid ? n.fluid : n.fluid = !0,
            dots: null != n.dots ? n.dots : n.dots = !0,
            arrows: null != n.arrows ? n.arrows : n.arrows = !1
        }, this.init(), this.$selector.on("unslider.change", function (t, e, n) {
            var i = $(t.currentTarget).find("li")[e - 1];
            $(i).attr("aria-hidden", !0), $(n).attr("aria-hidden", !1)
        })
    }
    return _createClass(t, [{
        key: "init",
        value: function () {
            this.$selector.removeClass("hide"), this.$selector.unslider(this.options), this.$selector.height("initial"), this.$selector.next("nav").find("ol").attr("aria-controls", "initial")
        }
    }]), t
} (),

    function () {
        $("body.js-pages-show-contact").ready(function () {
            var t = window.location.hash.match(/#sales/i);
            if (t) return window.location = "/form/contact-sales" + window.location.search;
            var e = new google.maps.LatLng(37.7724664, -122.4025232),
                n = function () {
                    var t = {
                        zoom: 16,
                        center: e,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: !1
                    },
                        n = new google.maps.Map(document.getElementById("heroku-map"), t),
                        i = "<h2>Heroku, Inc.\u200e</h2><div><p>650 7th Street<br/>San Francisco, CA 94103<br/>heroku.com\u200e</div>",
                        r = new google.maps.InfoWindow({
                            content: i
                        }),
                        o = new google.maps.Marker({
                            position: e,
                            map: n,
                            title: "Heroku Rocks!",
                            animation: google.maps.Animation.DROP
                        });
                    google.maps.event.addListener(o, "click", function () {
                        r.open(n, o)
                    })
                };
            google.maps.event.addDomListener(window, "load", n)
        })
    }.call(void 0), Heroku.iFrameLoaded = function (t, e, n) {
        var i = $.Deferred(),
            r = $("" + t).attr("src", e);
        return r.load(i.resolve), r.appendTo(n), i.promise()
    },
    function () {
        $("body.js-customers-index").ready(function () {
            var t = {
                valueNames: ["customer-name", "customer-desc"]
            },
                e = new List("customers-search", t);
            e.on("updated", function () {
                e.matchingItems.length || $("#customers-search .list").append('<li class="empty">No customers found.</li>')
            })
        })
    }.call(void 0);
var _createClass = function () {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }
    return function (e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
    }
} ();


var _createClass = function () {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }
    return function (e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
    }
} ();
Heroku.AgreeBeforeSubmit = function () {
    function t(e, n) {
        var i = this;
        _classCallCheck(this, t), this.$form = $(e), this.form = this.$form.get(0), this.url = n, this.$form.find(":submit").on("click", function (t) {
            (!i.form.checkValidity || i.form.checkValidity()) && i.handleAgreeTerms(t)
        })
    }
    return _createClass(t, [{
        key: "handleAgreeTerms",
        value: function (t) {
            var e = this;
            t.preventDefault(), new Modal("#terms").show(), $(document).on("click", "#agree-to-terms", function (t) {
                t.preventDefault(), e.$form.attr("action", e.url), e.$form.submit()
            })
        }
    }]), t
} (),
    function () {
        $("body.js-forms-show-500startups").ready(function () {
            new Heroku.AgreeBeforeSubmit("form.js-agree-before-submit", "/form_submissions/five_hundred_startups_form")
        })
    }.call(void 0),
    function () {
        var t = function () {
            var t = arguments.length <= 0 || void 0 === arguments[0] ? fieldDetails : arguments[0],
                e = t.$target,
                n = (t.$hiddenField, t.fieldName),
                i = t.inputType;
            if ("checkbox" === i) {
                var r = n.includes("_other"),
                    o = e.is(":checked");
                return r && o
            }
            return "Other" === e.val()
        },
            e = function (e) {
                var n = $(e.currentTarget),
                    i = n.attr("name"),
                    r = n.attr("type"),
                    o = "checkbox" === r ? $(".js_" + i + "_text") : $(".js_" + i + "_other_text"),
                    a = {
                        $target: n,
                        $hiddenField: o,
                        fieldName: i,
                        inputType: r
                    };
                t(a) ? (o.attr("required", !0), o.removeClass("hide")) : (o.attr("required", !1), o.addClass("hide"))
            };
        $("input[type=radio], input[type=checkbox]").on("change", e)
    }.call(void 0),
    function () {
        $("body.js-home-index").ready(function () {

            var t, e;
            return window.Heroku.carouselScrubber = new Heroku.Carousel("#scrubber-slide .slides", {
                speed: 750
            }), t = $("#scrubber-mobile-container"), e = $("#scrubber-slide .slides", t), t.waypoint({
                handler: function (t) {
                    return "down" === t ? e.data("unslider").next() : void 0
                },
                offset: "10%"
            }), t.waypoint({
                handler: function (t) {
                    return "up" === t && e.data("unslider").prev(), e.data("unslider").start()
                },
                offset: "-10%"
            })
        } (),
            function () {
                var t;
                return t = $("section.news .news-twitter a[data-timestamp]"), $.each(t, function (t, e) {
                    var n, i;
                    return i = $(e).attr("data-timestamp"), n = moment(i), n.isBefore(moment().subtract(24, "hours")) ? $(e).text(n.local().format("hh:mm A - DD MMM YYYY")) : $(e).text(moment(i).fromNow())
                })
            } (),
            function () {
                var t, e, n, i, r, o, a, s, l, u, c, d, h, f, p, m, g, v, y, w, b, x, k, C, S, T, D;
                return w = $(".scrubber-stage"), y = $(".scrubber-middle"), l = $("#scrubber-handle"), r = $(".scrubber-before"), t = $(".scrubber-after"), u = l.outerWidth(), T = y.outerWidth(), c = y.outerHeight(), D = y.offset(), C = D.top, h = D.left, o = C + y.outerHeight(), m = h + T, d = !1, v = !1, S = window.innerHeight, n = S / 2 + 134, i = C - n, e = i + 750, f = 0, a = 0, p = 0, s = 0, w.hover(function () {
                    return d = !0
                }, function () {
                    return d = !1
                }), $(document).mousemove(_.throttle(function (t) {
                    return f = a, p = s, a = t.pageX, s = t.pageY, b() && f !== a && $(window).scrollTop() > C / 2 ? (k(t), v = !0) : void 0
                }, 33)), x = function (t) {
                    return Math.min(Math.max(t - h, -u), T)
                }, b = function () {
                    var t, e;
                    return e = $(window).scrollTop(), t = e + window.innerHeight, C > e && t > C || o > e && t > o
                }, g = function (e, n, i) {
                    var o;
                    return o = x(e), n ? (l.stop().animate({
                        left: o
                    }, n, "linear", i), r.stop().animate({
                        width: T - o
                    }, n, "linear"), t.stop().animate({
                        width: o
                    }, n, "linear")) : (l.stop().css({
                        left: o
                    }), r.stop().css({
                        width: T - o
                    }), t.stop().css({
                        width: o
                    }))
                }, k = function () {
                    var t, e, n, i, r;
                    if (!d) return r = $(window).scrollTop(), t = s ? C > s : C > r, e = s ? s > o : r > o, i = l.offset().left, a !== f && Math.abs(a - i) > 10 ? (n = t || e ? Math.abs(t ? s - C : o - s) : 100, g(a, n)) : void 0
                }, $(window).scroll(function (t) {
                    var n;
                    if (!v) return i <= (n = $(window).scrollTop() + 32) && e + 32 >= n && !v ? g(Math.min(h + Math.max(12 + $(window).scrollTop() - i, 12), h + 830)) : void 0
                }), w.mousemove(function (t) {
                    return a !== f ? (g(t.pageX), v = !0) : void 0
                }), $(window).scrollTop() < i ? g(h + 12) : $(window).scrollTop() > e ? g(h + 830) : void 0
            } ()
        )
    }.call(this);
