(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .directive('codeScrub', codeScrub);

    codeScrub.$inject = ['$translatePartialLoader', '$translate', '$filter', 'SocialService'];

    function codeScrub($translatePartialLoader, $translate, $filter, SocialService) {
        var directive = {
            restrict: 'E',
            scope: {
                provider: '@ngProvider'
            },
            templateUrl: 'app/widgets/scrubber/widget.scrub.html',
        };

        return directive;

        /* private helper methods */

        function linkFunc(scope) {

            $translatePartialLoader.addPart('social');
            $translate.refresh();

            scope.label = $filter('capitalize')(scope.provider);
            scope.providerSetting = SocialService.getProviderSetting(scope.provider);
            scope.providerURL = SocialService.getProviderURL(scope.provider);
            scope.csrf = SocialService.getCSRF();
        }

    }
})();
