(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('login', {
            parent: 'app',
            url: '/login',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/components/login/login.html',
                    controller:'LoginFlatController',
                    controllerAs:'vm'
                },
                'footer@': {
                    templateUrl: 'app/layouts/footer/footer.small.html',
                },
                'navbar@':{
                    templateUrl:'app/layouts/navbar/navbar.small.html'
                },
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('login');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
