(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('public', {
            parent: 'containers',
            url: '/codelogs',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/containers/application/application.codelogs.html',
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('home');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
