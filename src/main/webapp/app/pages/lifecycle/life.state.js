(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('lifecycle', {
            parent: 'pages',
            url: '/lifecycle',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/pages/lifecycle/life.html',
                },
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('home');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
