(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
