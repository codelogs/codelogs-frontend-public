function fullScan() {
    loadBackgrounds();
    scrollFuncions();
    newsLetter();
    renderGraphs();
}
/**
 * Transforma los img establecidos en el html a su correspondiente CSS MODE
 */

function loadBackgrounds() {
    "use strict";
    console.log("BACKUPDATES" + $('.background-image-holder').length)
    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")').css('background-position', 'initial').css('opacity', '1');
    });
};


/**
 * Funciones de utilidad
 */
function util() {
    "use strict";
    /**
     * Mapa de funciones utiles de javascript
     * @type {{getURLParameter: getURLParameter, capitaliseFirstLetter: capitaliseFirstLetter, idleSrc: idleSrc, activateIdleSrc: activateIdleSrc, pauseVideo: pauseVideo}}
     */
    var util = {
        getURLParameter: getURLParameter,
        capitaliseFirstLetter: capitaliseFirstLetter,
        idleSrc: idleSrc,
        activateIdleSrc: activateIdleSrc,
        pauseVideo: pauseVideo
    };

    util.requestAnimationFrame = window.requestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame;

    function initDates() {
        var today = new Date();
        var year = today.getFullYear();
        $('.update-year').text(year);
    }

    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
    };


    function capitaliseFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };


    // Set data-src attribute of element from src to be restored later
    function idleSrc(context, selector) {

        selector = (typeof selector !== typeof undefined) ? selector : '';
        var elems = context.is(selector + '[src]') ? context : context.find(selector + '[src]');

        elems.each(function (index, elem) {
            elem = $(elem);
            var currentSrc = elem.attr('src'),
                dataSrc = elem.attr('data-src');

            // If there is no data-src, save current source to it
            if (typeof dataSrc === typeof undefined) {
                elem.attr('data-src', currentSrc);
            }

            // Clear the src attribute
            elem.attr('src', '');

        });
    };

    // Set src attribute of element from its data-src where it was temporarily stored earlier
    function activateIdleSrc(context, selector) {

        selector = (typeof selector !== typeof undefined) ? selector : '';
        var elems = context.is(selector + '[src]') ? context : context.find(selector + '[src]');

        elems.each(function (index, elem) {
            elem = $(elem);
            var dataSrc = elem.attr('data-src');

            // If there is no data-src, save current source to it
            if (typeof dataSrc !== typeof undefined) {
                elem.attr('src', dataSrc);
            }
        });
    };

    function pauseVideo(context) {
        var elems = context.is('video') ? context : context.find('video');

        elems.each(function (index, video) {
            var playingVideo = $(video).get(0);
            playingVideo.pause();
        });
    };

};

/**
 * Funciones scroll
 */
function scrollFuncions() {
    "use strict";

    var scroll = {};
    scroll.listeners = [];
    scroll.y = 0;
    scroll.x = 0;

    function onReady() {
        addEventListener('scroll', function (evt) {
            util.requestAnimationFrame.call(window, scroll.update);
        }, false);
    }
    scroll.update = function () {
        scroll.y = window.pageYOffset;
        scroll.x = window.pageXOffset;
        for (var i = 0, l = scroll.listeners.length; i < l; i++) {
            scroll.listeners[i]();
        }
    };

};

/**
 * Formularios
 */
function formulario() {
    "use strict";



    var forms = {};

    (function init() {
        onReady();
    })();

    function onReady() {

        //////////////// Checkbox Inputs

        $('.input-checkbox').on('click', function () {
            var checkbox = $(this);
            checkbox.toggleClass('checked');

            var input = checkbox.find('input');
            if (input.prop('checked') === false) {
                input.prop('checked', true);
            } else {
                input.prop('checked', false);
            }
            return false;
        });

        //////////////// Radio Buttons

        $('.input-radio').on('click', function () {
            var radio = $(this);
            radio.closest('form').find('.input-radio').removeClass('checked');
            radio.addClass('checked').find('input').prop('checked', true);
            return false;
        });

        //////////////// File Uploads

        $('.input-file .btn').on('click', function () {
            $(this).siblings('input').trigger('click');
            return false;
        });

        //////////////// Handle Form Submit

        $('form.form-email, form[action*="list-manage.com"], form[action*="createsend.com"]').attr('novalidate', true).unbind('submit').on('submit', forms.submit);

        //////////////// Handle Form Submit
        $(document).on('change, input, paste, keyup', '.attempted-submit .field-error', function () {
            $(this).removeClass('field-error');
        });

    };

    forms.submit = function (e) {
        // return false so form submits through jQuery rather than reloading page.
        if (e.preventDefault) e.preventDefault();
        else e.returnValue = false;

        var body = $('body'),
            thisForm = $(e.target).closest('form'),
            formAction = typeof thisForm.attr('action') !== typeof undefined ? thisForm.attr('action') : "",
            submitButton = thisForm.find('button[type="submit"], input[type="submit"]'),
            error = 0,
            originalError = thisForm.attr('original-error'),
            successRedirect, formError, formSuccess, errorText, successText;

        body.find('.form-error, .form-success').remove();
        submitButton.attr('data-text', submitButton.text());
        errorText = thisForm.attr('data-error') ? thisForm.attr('data-error') : "Please fill all fields correctly";
        successText = thisForm.attr('data-success') ? thisForm.attr('data-success') : "Thanks, we'll be in touch shortly";
        body.append('<div class="form-error" style="display: none;">' + errorText + '</div>');
        body.append('<div class="form-success" style="display: none;">' + successText + '</div>');
        formError = body.find('.form-error');
        formSuccess = body.find('.form-success');
        thisForm.addClass('attempted-submit');

        // Do this if the form is intended to be submitted to MailChimp or Campaign Monitor
        if (formAction.indexOf('createsend.com') !== -1 || formAction.indexOf('list-manage.com') !== -1) {

            console.log('Mail list form signup detected.');
            if (typeof originalError !== typeof undefined && originalError !== false) {
                formError.html(originalError);
            }

            // validateFields returns 1 on error;
            if (forms.validateFields(thisForm) !== 1) {

                thisForm.removeClass('attempted-submit');

                // Hide the error if one was shown
                formError.fadeOut(200);
                // Create a new loading spinner in the submit button.
                submitButton.addClass('btn--loading');

                try {
                    $.ajax({
                        url: thisForm.attr('action'),
                        crossDomain: true,
                        data: thisForm.serialize(),
                        method: "GET",
                        cache: false,
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            // Request was a success, what was the response?

                            if (data.result !== "success" && data.Status !== 200) {

                                // Got an error from Mail Chimp or Campaign Monitor

                                // Keep the current error text in a data attribute on the form
                                formError.attr('original-error', formError.text());
                                // Show the error with the returned error text.
                                formError.html(data.msg).stop(true).fadeIn(1000);
                                formSuccess.stop(true).fadeOut(1000);

                                submitButton.removeClass('btn--loading');
                            } else {

                                // Got success from Mail Chimp or Campaign Monitor

                                submitButton.removeClass('btn--loading');

                                successRedirect = thisForm.attr('data-success-redirect');
                                // For some browsers, if empty `successRedirect` is undefined; for others,
                                // `successRedirect` is false.  Check for both.
                                if (typeof successRedirect !== typeof undefined && successRedirect !== false && successRedirect !== "") {
                                    window.location = successRedirect;
                                } else {
                                    forms.resetForm(thisForm);
                                    forms.showFormSuccess(formSuccess, formError, 1000, 5000, 500);
                                }
                            }
                        }
                    });
                } catch (err) {
                    // Keep the current error text in a data attribute on the form
                    formError.attr('original-error', formError.text());
                    // Show the error with the returned error text.
                    formError.html(err.message);
                    forms.showFormError(formSuccess, formError, 1000, 5000, 500);

                    submitButton.removeClass('btn--loading');
                }



            } else {
                // There was a validation error - show the default form error message
                forms.showFormError(formSuccess, formError, 1000, 5000, 500);
            }
        } else {
            // If no MailChimp or Campaign Monitor form was detected then this is treated as an email form instead.
            if (typeof originalError !== typeof undefined && originalError !== false) {
                formError.text(originalError);
            }

            error = forms.validateFields(thisForm);

            if (error === 1) {
                forms.showFormError(formSuccess, formError, 1000, 5000, 500);
            } else {

                thisForm.removeClass('attempted-submit');

                // Hide the error if one was shown
                formError.fadeOut(200);

                // Create a new loading spinner in the submit button.
                submitButton.addClass('btn--loading');

                jQuery.ajax({
                    type: "POST",
                    url: "mail/mail.php",
                    data: thisForm.serialize() + "&url=" + window.location.href,
                    success: function (response) {
                        // Swiftmailer always sends back a number representing number of emails sent.
                        // If this is numeric (not Swift Mailer error text) AND greater than 0 then show success message.

                        submitButton.removeClass('btn--loading');

                        if ($.isNumeric(response)) {
                            if (parseInt(response, 10) > 0) {
                                // For some browsers, if empty 'successRedirect' is undefined; for others,
                                // 'successRedirect' is false.  Check for both.
                                successRedirect = thisForm.attr('data-success-redirect');
                                if (typeof successRedirect !== typeof undefined && successRedirect !== false && successRedirect !== "") {
                                    window.location = successRedirect;
                                }

                                forms.resetForm(thisForm);
                                forms.showFormSuccess(formSuccess, formError, 1000, 5000, 500);
                            }
                        }
                        // If error text was returned, put the text in the .form-error div and show it.
                        else {
                            // Keep the current error text in a data attribute on the form
                            formError.attr('original-error', formError.text());
                            // Show the error with the returned error text.
                            formError.text(response).stop(true).fadeIn(1000);
                            formSuccess.stop(true).fadeOut(1000);
                        }
                    },
                    error: function (errorObject, errorText, errorHTTP) {
                        // Keep the current error text in a data attribute on the form
                        formError.attr('original-error', formError.text());
                        // Show the error with the returned error text.
                        formError.text(errorHTTP).stop(true).fadeIn(1000);
                        formSuccess.stop(true).fadeOut(1000);
                        submitButton.removeClass('btn--loading');
                    }
                });
            }
        }
        return false;
    };

    forms.validateFields = function (form) {
        var body = $(body), name, error, originalErrorMessage;

        $(form).find('.validate-required[type="checkbox"]').each(function () {
            if (!$('[name="' + $(this).attr('name') + '"]:checked').length) {
                error = 1;
                name = $(this).attr('name').replace('[]', '');
                body.find('.form-error').text('Please tick at least one ' + name + ' box.');
            }
        });

        $(form).find('.validate-required, .required, [required]').each(function () {
            if ($(this).val() === '') {
                $(this).addClass('field-error');
                error = 1;
            } else {
                $(this).removeClass('field-error');
            }
        });

        $(form).find('.validate-email, .email, [name*="cm-"][type="email"]').each(function () {
            if (!(/(.+)@(.+){2,}\.(.+){2,}/.test($(this).val()))) {
                $(this).addClass('field-error');
                error = 1;
            } else {
                $(this).removeClass('field-error');
            }
        });

        if (!form.find('.field-error').length) {
            body.find('.form-error').fadeOut(1000);
        } else {

            var firstError = $(form).find('.field-error:first');

            if (firstError.length) {
                $('html, body').stop(true).animate({
                    scrollTop: (firstError.offset().top - 100)
                }, 1200, function () { firstError.focus(); });
            }
        }

        return error;
    };

    forms.showFormSuccess = function (formSuccess, formError, fadeOutError, wait, fadeOutSuccess) {

        formSuccess.stop(true).fadeIn(fadeOutError);

        formError.stop(true).fadeOut(fadeOutError);
        setTimeout(function () {
            formSuccess.stop(true).fadeOut(fadeOutSuccess);
        }, wait);
    };

    forms.showFormError = function (formSuccess, formError, fadeOutSuccess, wait, fadeOutError) {

        formError.stop(true).fadeIn(fadeOutSuccess);

        formSuccess.stop(true).fadeOut(fadeOutSuccess);
        setTimeout(function () {
            formError.stop(true).fadeOut(fadeOutError);
        }, wait);
    };

    // Reset form to empty/default state.
    forms.resetForm = function (form) {
        form = $(form);
        form.get(0).reset();
        form.find('.input-radio, .input-checkbox').removeClass('checked');
    };

};


/**
 * Provider newletters
 */
function newsLetter() {
    "use strict";

    var newsletters = {};

    (function onReady() {
        var form, checkbox, label, id, parent, radio;

        // Treat Campaign Monitor forms
        $('form[action*="createsend.com"]').each(function () {
            form = $(this);

            // Override browser validation and allow us to use our own
            form.attr('novalidate', 'novalidate');

            // Give each text input a placeholder value

            if (!form.is('.form--no-placeholders')) {
                form.find('input:not([checkbox]):not([radio])').each(function () {
                    var $input = $(this);
                    if (typeof $input.attr('placeholder') !== typeof undefined) {
                        if ($input.attr('placeholder') === "") {
                            if ($input.siblings('label').length) {
                                $input.attr('placeholder', $input.siblings('label').first().text());
                                if (form.is('.form--no-labels')) {
                                    $input.siblings('label').first().remove();
                                }
                            }
                        }
                    } else if ($input.siblings('label').length) {
                        $input.attr('placeholder', $input.siblings('label').first().text());
                        if (form.is('.form--no-labels')) {
                            $input.siblings('label').first().remove();
                        }
                    }
                    if ($input.parent().is('p')) {
                        $input.unwrap();
                    }
                });
            } else {
                form.find('input[placeholder]').removeAttr('placeholder');
            }


            // Wrap select elements in template code

            form.find('select').wrap('<div class="input-select"></div>');

            // Wrap radios elements in template code

            form.find('input[type="radio"]').wrap('<div class="input-radio"></div>');

            // Wrap checkbox elements in template code

            form.find('input[type="checkbox"]').each(function () {
                checkbox = $(this);
                id = checkbox.attr('id');
                label = form.find('label[for=' + id + ']');

                checkbox.before('<div class="input-checkbox" data-id="' + id + '"></div>');
                $('.input-checkbox[data-id="' + id + '"]').prepend(checkbox);
                $('.input-checkbox[data-id="' + id + '"]').prepend(label);
                $('.input-checkbox[data-id="' + id + '"]').prepend('<div class="inner"></div>');
            });

            form.find('button[type="submit"]').each(function () {
                var button = $(this);
                button.addClass('btn');
                if (button.parent().is('p')) {
                    button.unwrap();
                }
            });

            form.find('[required]').removeAttr('required').addClass('validate-required');

            form.addClass('form--active');

        });

    })();

};


/**
 * Renderiza los graficos estadisticos
 */
function renderGraphs (){
	  "use strict";
      (function onReady(){
            render();
      })();
	  function render(){
	      jQuery.fn.easyaspie = function () {

				var	size	= parseInt(this.data('size'), 10),
					radius	= size / 2,
					value	= parseInt(this.data('value'), 10);

				// pie all the things!
				if (this.length > 1){
					this.each( function() {
						$(this).easyaspie();
					});
					return this;
				}

				// is you trying to break things?
				if (isNaN(value)) {
					return this;
				}

				// set the size of this
				this.css({
					height: size,
					width: size
				}).addClass('pie-sliced');

				// make value behave
				value = Math.min(Math.max(value, 0), 100);

				// make me some svg
				this.pie = document.createElementNS("http://www.w3.org/2000/svg", "svg");

				// if value is 100 or higher, just use a circle
				if (value >= 100) {
					this.pie.slice = document.createElementNS("http://www.w3.org/2000/svg", "circle");
					this.pie.slice.setAttribute('r', radius);
					this.pie.slice.setAttribute('cx', radius);
					this.pie.slice.setAttribute('cy', radius);

				} else {
					this.pie.slice = document.createElementNS("http://www.w3.org/2000/svg", "path");

					//calculate x,y coordinates of the point on the circle to draw the arc to.
					var x = Math.cos((2 * Math.PI)/(100/value));
					var y = Math.sin((2 * Math.PI)/(100/value));

					//should the arc go the long way round?
					var longArc = (value <= 50) ? 0 : 1;

					//d is a string that describes the path of the slice.
					var d = "M" + radius + "," + radius + " L" + radius + "," + 0 + ", A" + radius + "," + radius + " 0 " + longArc + ",1 " + (radius + y*radius) + "," + (radius - x*radius) + " z";
					this.pie.slice.setAttribute('d', d);
				}

				//add the slice to the pie.
		        jQuery(this.pie.slice).appendTo(this.pie);

				// add the pie to this
				jQuery(this.pie).appendTo(this);

				return this;
			};

			var pieCharts = $('.piechart');

			if(pieCharts.length){
			    pieCharts.easyaspie().addClass('active');
			}

		    $('.barchart').each(function(){
		    	var chart = $(this);
		    	var bar = chart.find('.barchart__progress');
		    	var barWidth = chart.attr('data-value')*1 + '%';
		    	bar.attr('data-value', barWidth);
		    	if(!chart.hasClass('barchart--vertical')){
		    		bar.css('width', barWidth);
		    	}else{
		    		bar.css('height', barWidth);
		    	}

		    });
	  };

};
/**
function _createClass() {
    function t(t, e) {
        for (var n = 0; n < e.length; n++) {
            var i = e[n];
            i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
        }
    }
    return function(e, n, i) {
        return n && t(e.prototype, n), i && t(e, i), e
    }
};
function CodelogsCarousel(){
    function t(e) {
        var n = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1];
          this.$selector = $(e), this.options = {
            autoplay: null != n.autoplay ? n.autoplay : n.autoplay = !0,
            delay: n.delay || 5e3,
            fluid: null != n.fluid ? n.fluid : n.fluid = !0,
            dots: null != n.dots ? n.dots : n.dots = !0,
            arrows: null != n.arrows ? n.arrows : n.arrows = !1
        }, this.init(), this.$selector.on("unslider.change", function(t, e, n) {
            var i = $(t.currentTarget).find("li")[e - 1];
            $(i).attr("aria-hidden", !0), $(n).attr("aria-hidden", !1)
        })
    }
    return _createClass(t, [{
        key: "init",
        value: function() {
            this.$selector.removeClass("hide"), this.$selector.unslider(this.options), this.$selector.height("initial"), this.$selector.next("nav").find("ol").attr("aria-controls", "initial")
        }
    }]), t
}

(function autoinit(){
    scrubber();
    scrubberAnimation();
})();
function scrubber() {
    console.log("ANIMATINO SCR IMG")
    var exports = {

    };


    var t, e;
    return CodelogsCarousel = new CodelogsCarousel("#scrubber-slide .slides", {
        speed: 750
    }), t = $("#scrubber-mobile-container"), e = $("#scrubber-slide .slides", t), t.waypoint({
        handler: function (t) {
            return "down" === t ? e.data("unslider").next() : void 0
        },
        offset: "10%"
    }), t.waypoint({
        handler: function (t) {
            return "up" === t && e.data("unslider").prev(), e.data("unslider").start()
        },
        offset: "-10%"
    })
};

function scrubberAnimation() {
    console.log("ANIMATINO")
    
}; */