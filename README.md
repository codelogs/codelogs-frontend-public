# Estado
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
[![Gitter](https://img.shields.io/gitter/room/nwjs/nw.js.svg)](https://gitter.im/codelogs-fragent/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
[![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://opensource.org/licenses/MIT)
[![coverage](https://gitlab.com/codelogs/codelogs-frontend-public/badges/master/build.svg)](https://gitlab.com/codelogs/codelogs-frontend-public/commits/master)

# Desarrollo de aplicación publica

Aplicación simple contenido público del proyecto. 
Este proyecto es utilizado en conjunto con overlays de maven, para seperar 
el desarrollo del contenido web del servidor.

- gulp : cadenas y minificación de archivos, server local , live reload
- node
- bower
- maven : empaquetar el proyecto como un war, integración con node, gulp y bower.
 
 perfiles :
    -prod-fast // utiliza un ambiente configurado en la maquina (no descarga nada)
    -prod // descarga todo (lento) lo necesario para poner empaquetar la aplicación en un war
    GITLAB CI
    

Utilidad:

Util para que las aplicaciones y componentes se desarrollen de forma independiente
y se integra con el servidor añadiendola solamente como una dependencia. 



### IMPORTANTE

Recuerda solo puede existir un index en el directorio root del servidor aplicación en spring,
puedes renombrar esta al id del artefacto o cualquier otro para no override los recursos. 


Hasta este punto la semilla esta configurada para tomar atributos del
pom.xml para nombrar directorios, problemas conocidos :

Actualiza las constantes de la plantilla generada con el
nombre de tu aplicación en caso de renombrarla
gulpfile.js

```
 return ngConstant({
        name: 'modulePublic',
        constants: {
            VERSION: util.parseVersion(),
            DEBUG_INFO_ENABLED: true,
            SERVER_API:"DEFAULT" // IP O HOST DEL SERVER API
        },
        template: config.constantTemplate,
        stream: true
    })
    
  ```
