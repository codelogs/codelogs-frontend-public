(function() {
    'use strict';

    angular
        .module('codpubliapp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$scope','$state', 'Auth', 'Principal', 'ProfileService', 'LoginService'];

    function NavbarController ($scope,$state, Auth, Principal, ProfileService, LoginService) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.isAuthenticated = Principal.isAuthenticated;
        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;

        ProfileService.getProfileInfo().then(function(response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        init();

        function init() {

            $scope.$on("authenticationSuccess",function (data) {
                console.log("AUTH SUCCESS")
                loadUserInfo();
            });
            loadUserInfo();
        }

        function loadUserInfo() {
            Principal.identity(true).then(
                function(data) {     // On success
                    vm.userinfo =  data;
                },
                function(error) {   // On failure
                    vm.userinfo = {};
                    console.error("error auth")
                }
            );
            return vm.userinfo;
        }

        function login() {
            collapseNavbar();
            LoginService.open();
        }

        function logout() {
            collapseNavbar();
            Auth.logout();
            $state.go('home');
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }
    }
})();
