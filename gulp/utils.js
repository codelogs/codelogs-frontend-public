'use strict';

var fs = require('fs');

module.exports = {
    endsWith : endsWith,
    parseVersion : parseVersion,
    parseArtefactoId:parseArtefactoId,
    isLintFixed : isLintFixed,
    parseName:parseName
};

function endsWith(str, suffix) {
    return str.indexOf('/', str.length - suffix.length) !== -1;
}

var parseString = require('xml2js').parseString;
// return the version number from `pom.xml` file
function parseVersion() {
    var version = null;
    var pomXml = fs.readFileSync('pom.xml', 'utf8');
    parseString(pomXml, function (err, result) {
        if (result.project.version && result.project.version[0]) {
            version = result.project.version[0];
        } else if (result.project.parent && result.project.parent[0] && result.project.parent[0].version && result.project.parent[0].version[0]) {
            version = result.project.parent[0].version[0];
        }
    });
    if (version === null) {
        throw new Error('pom.xml is malformed. No version is defined');
    }
    return version;
}
function parseArtefactoId() {
    var artefactoId = null;
    var pomXml = fs.readFileSync('pom.xml', 'utf8');
    parseString(pomXml, function (err, result) {
        if (result.project.artifactId && result.project.artifactId[0]) {
            artefactoId = result.project.artifactId[0];
        } else if (result.project.parent && result.project.parent[0] && result.project.parent[0].artifactId && result.project.parent[0].artifactId[0]) {
            artefactoId = result.project.parent[0].artifactId[0];
        }
    });
    if (artefactoId === null) {
        throw new Error('pom.xml is malformed. No version is defined');
    }
    return artefactoId;
}

function parseName() {
    var name = null;
    var pomXml = fs.readFileSync('pom.xml', 'utf8');
    parseString(pomXml, function (err, result) {
        if (result.project.artifactId && result.project.name[0]) {
            name = result.project.name[0];
        } else if (name == null) {
            name = result.project.artifactId[0];
        }
    });
    if (name === null) {
        throw new Error('pom.xml is malformed. No version is defined');
    }
    return name;
}
function isLintFixed(file) {
	// Has ESLint fixed the file contents?
	return file.eslint !== null && file.eslint.fixed;
}

